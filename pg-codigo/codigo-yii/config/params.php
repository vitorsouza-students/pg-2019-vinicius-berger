<?php

return [
    'adminEmail' => 'viniciush4@gmail.com',
    'mail' => [
        'class' => 'Swift_SmtpTransport',
        'host' => '', // adicionar aqui o host. Ex: smtp.hostinger.com.br
        'username' => '', // adicionar aqui o username. Ex: nao-responda@viniciush4.com
        'password' => '', // adicionar a senha
        'port' => '465',
        'encryption' => 'ssl',
    ],
    'nao-responda' => 'nao-responda@viniciush4.com',
    'notificacao_erros' => ['viniciush4@gmail.com']
];
