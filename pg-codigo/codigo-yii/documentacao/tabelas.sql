

create table if not exists usuario (
    id int(11) not null auto_increment,
    login varchar(100) not null,
    senha varchar(100) not null,
    primary key (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;


create table if not exists secretario (
    id int(11) not null auto_increment,
    nome varchar(100) not null,
    sobrenome varchar(100) not null,
    email varchar(100) not null,
    telefone varchar(15) default null,
    usuario_id int(11) default null,
    primary key (id),
    foreign key (usuario_id) references usuario (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;


create table if not exists professor (
    id int(11) not null auto_increment,
    nome varchar(100) not null,
    sobrenome varchar(100) not null,
    email varchar(100) not null,
    telefone varchar(15) default null,
    usuario_id int(11) default null,
    primary key (id),
    foreign key (usuario_id) references usuario (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;


create table if not exists parentesco (
    id int(11) not null auto_increment,
    parentesco enum('sanguineo','matrimonial') not null,
    professor_id int(11) default null,
    professor_parente_id int(11) default null,
    primary key (id),
    foreign key (professor_id) references professor (id),
    foreign key (professor_parente_id) references professor (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;


create table if not exists mandato (
    id int(11) not null auto_increment,
    data_inicio date not null,
    data_fim date not null,
    professor_id int(11) not null,
    primary key (id),
    foreign key (professor_id) references professor (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;


create table if not exists afastamento (
    id int(11) not null auto_increment,
    data_solicitacao date not null,
    data_inicio_afastamento date not null,
    data_fim_afastamento date not null,
    data_inicio_evento date not null,
    data_fim_evento date not null,
    nome_evento varchar(100) not null,
    situacao enum('iniciado','aguardando_parecer_relator','liberado','aguardando_decisao_di','aprovado_di','aprovado_ct','aprovado_prppg','arquivado','cancelado','reprovado') not null,
    tipo enum('nacional','internacional') not null,
    onus enum('total','parcial','inexistente') not null,
    motivo varchar(1000) default null,
    professor_solicitante_id int(11) not null,
    professor_relator_id int(11) default null,
    primary key (id),
    foreign key (professor_solicitante_id) references professor (id),
    foreign key (professor_relator_id) references professor (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;


create table if not exists documento (
    id int(11) not null auto_increment,
    titulo varchar(100) not null,
    nome_arquivo varchar(100) not null,
    data_juntada datetime not null,
    afastamento_id int(11) not null,
    primary key (id),
    foreign key (afastamento_id) references afastamento (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;


create table if not exists parecer (
    id int(11) not null auto_increment,
    data_parecer datetime not null,
    tipo_parecer enum('favoravel','desfavoravel') not null,
    motivo varchar(1000) default null,
    afastamento_id int(11) not null,
    professor_id int(11) default null,
    primary key (id),
    foreign key (afastamento_id) references afastamento (id),
    foreign key (professor_id) references professor (id)
) engine=InnoDB  default charset=utf8 collate=utf8_bin;




insert into usuario (
    login,
    senha
) values (
    'viniciush4@gmail.com',
    'e615605b99747ddb8fc2fd1f12ee1800f782930c'
);

insert into professor (
    nome,
    sobrenome,
    email,
    telefone,
    usuario_id
) values (
    'Vinícius',
    'Berger',
    'viniciush4@gmail.com',
    '(27) 99999-8888',
    1
);
