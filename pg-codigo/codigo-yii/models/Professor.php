<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "professor".
 *
 * @property Afastamento[] $meusAfastamentos
 * @property Afastamento[] $afastamentosSouRelator
 * @property Mandato[] $mandatos
 * @property Parecer[] $pareceres
 * @property Parentesco[] $meusParentes
 * @property Parentesco[] $souParente
 */
class Professor extends Pessoa
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'professor';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeusAfastamentos()
    {
        return $this->hasMany(Afastamento::className(), ['professor_solicitante_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAfastamentosSouRelator()
    {
        return $this->hasMany(Afastamento::className(), ['professor_relator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMandatos()
    {
        return $this->hasMany(Mandato::className(), ['professor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPareceres()
    {
        return $this->hasMany(Parecer::className(), ['professor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeusParentes()
    {
        return $this->hasMany(Parentesco::className(), ['professor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSouParente()
    {
        return $this->hasMany(Parentesco::className(), ['professor_parente_id' => 'id']);
    }
}
