<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parecer".
 *
 * @property integer $id
 * @property string $data_parecer
 * @property string $tipo_parecer
 * @property string $motivo
 * @property string $tipo_emissor
 * @property integer $afastamento_id
 * @property integer $professor_id
 *
 * @property Afastamento $afastamento
 * @property Professor $professor
 */
class Parecer extends \yii\db\ActiveRecord
{
	const TIPO_PARECER_FAVORAVEL = 'favoravel';
	const TIPO_PARECER_DESFAVORAVEL = 'desfavoravel';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parecer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_parecer', 'tipo_parecer', 'afastamento_id'], 'required'],
            [['data_parecer'], 'safe'],
            [['tipo_parecer'], 'string'],
            [['afastamento_id', 'professor_id'], 'integer'],
            [['motivo'], 'string', 'max' => 1000],
            [['afastamento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Afastamento::className(), 'targetAttribute' => ['afastamento_id' => 'id']],
            [['professor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_parecer' => 'Data Parecer',
            'tipo_parecer' => 'Tipo Parecer',
            'motivo' => 'Motivo',
            'afastamento_id' => 'Afastamento ID',
            'professor_id' => 'Professor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAfastamento()
    {
        return $this->hasOne(Afastamento::className(), ['id' => 'afastamento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessor()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_id']);
    }

	/**
	 * @return string[]
	 */
    public static function recuperarTiposParecer()
	{
		return [
			self::TIPO_PARECER_FAVORAVEL => 'Favorável',
			self::TIPO_PARECER_DESFAVORAVEL => 'Desfavorável'
		];
	}
}
