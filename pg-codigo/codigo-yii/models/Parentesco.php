<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "parentesco".
 *
 * @property integer $id
 * @property string $parentesco
 * @property integer $professor_id
 * @property integer $professor_parente_id
 *
 * @property Professor $professor
 * @property Professor $professorParente
 */
class Parentesco extends \yii\db\ActiveRecord
{
	const PARENTESCO_SANGUINEO = 'sanguineo';
	const PARENTESCO_MATRIMONIAL = 'matrimonial';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parentesco';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parentesco'], 'required'],
            [['parentesco'], 'string'],
            [['professor_id', 'professor_parente_id'], 'integer'],
            [['professor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_id' => 'id']],
            [['professor_parente_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_parente_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parentesco' => 'Parentesco',
            'professor_id' => 'Professor ID',
            'professor_parente_id' => 'Professor Parente ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessor()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessorParente()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_parente_id']);
    }

}
