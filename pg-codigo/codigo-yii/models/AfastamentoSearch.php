<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Afastamento;

/**
 * AfastamentoSearch represents the model behind the search form about `app\models\Afastamento`.
 */
class AfastamentoSearch extends Afastamento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'professor_solicitante_id', 'professor_relator_id'], 'integer'],
            [['data_solicitacao', 'data_inicio_afastamento', 'data_fim_afastamento', 'data_inicio_evento', 'data_fim_evento', 'nome_evento', 'situacao', 'tipo', 'onus', 'motivo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Afastamento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data_solicitacao' => implode('-', array_reverse( explode('/', $this->data_solicitacao))),
            'data_inicio_afastamento' => $this->data_inicio_afastamento,
            'data_fim_afastamento' => $this->data_fim_afastamento,
            'data_inicio_evento' => $this->data_inicio_evento,
            'data_fim_evento' => $this->data_fim_evento,
            'professor_solicitante_id' => $this->professor_solicitante_id,
            'professor_relator_id' => $this->professor_relator_id,
            'tipo' => $this->tipo
        ]);

        $query->andFilterWhere(['like', 'nome_evento', $this->nome_evento])
            ->andFilterWhere(['like', 'situacao', $this->situacao])
            ->andFilterWhere(['like', 'onus', $this->onus])
            ->andFilterWhere(['like', 'motivo', $this->motivo]);

        return $dataProvider;
    }
}
