<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mandato".
 *
 * @property integer $id
 * @property string $data_inicio
 * @property string $data_fim
 * @property integer $professor_id
 *
 * @property Professor $professor
 */
class Mandato extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mandato';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_inicio', 'data_fim', 'professor_id'], 'required'],
            [['data_inicio', 'data_fim'], 'safe'],
            [['data_inicio', 'data_fim'], 'validarDataInicio'],
            [['data_inicio', 'data_fim'], 'validarDataConflitoComOutrosMandatos'],
            [['professor_id'], 'integer'],
            [['professor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_inicio' => 'Data Inicio',
            'data_fim' => 'Data Fim',
            'professor_id' => 'Professor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessor()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_id']);
    }

	/**
	 * @return mixed|null
	 */
    public static function recuperarChefeDepartamento()
	{
//		$mandato_atual = self::find()->where('mandato.data_fim > CURRENT_DATE and mandato.data_inicio < CURRENT_DATE')->one();
//		if (!empty($mandato_atual)) {
//			return $mandato_atual->professor;
//		}
        $mandatos_atuais = self::find()->where('mandato.data_fim > CURRENT_DATE and mandato.data_inicio < CURRENT_DATE')->limit(2)->all();
        $professores = [];
        foreach ($mandatos_atuais as $mandato) {
            $professores[] = $mandato->professor;
        }
        return $professores;
	}

    /**
     * @param $attribute
     * @throws \Exception
     */
    public function validarDataInicio($attribute)
    {
        $data_inicio = new \DateTime($this->$attribute, new \DateTimeZone('America/Sao_Paulo'));

        $nome_atributo_fim = str_replace('inicio', 'fim', $attribute);
        $data_fim = new \DateTime($this->$nome_atributo_fim, new \DateTimeZone('America/Sao_Paulo'));

        if ($data_inicio > $data_fim) {
            $this->addError($attribute, 'A data de início não pode ser posterior à data de fim');
        }
    }

    /**
     * @param $attribute
     * @throws \Exception
     */
    public function validarDataConflitoComOutrosMandatos($attribute)
    {
        $data = new \DateTime($this->$attribute, new \DateTimeZone('America/Sao_Paulo'));

        $quantidade_mandatos_conflitantes = 0;

        foreach (Mandato::find()->all() as $mandato) {
            $data_inicio = new \DateTime($mandato->data_inicio, new \DateTimeZone('America/Sao_Paulo'));
            $data_fim = new \DateTime($mandato->data_fim, new \DateTimeZone('America/Sao_Paulo'));
            if ($data >= $data_inicio && $data <= $data_fim) {
                $quantidade_mandatos_conflitantes++;
            }
        }

        if ($quantidade_mandatos_conflitantes > 1) {
            $this->addError($attribute, 'Esta data conflita com dois ou mais mandatos cadastrados');
        }
    }
}
