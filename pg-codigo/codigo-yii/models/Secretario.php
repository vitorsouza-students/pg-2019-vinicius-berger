<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "secretario".
 */
class Secretario extends Pessoa
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'secretario';
    }
}
