<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $id
 * @property string $login
 * @property string $senha
 *
 * @property Professor $professor
 * @property Secretario $secretario
 */
class Usuario extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'senha'], 'required'],
			[['login'], 'string', 'max' => 100],
			[['senha'], 'string', 'max' => 100],
			[['login'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
			'login' => 'Login',
            'senha' => 'Senha'
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProfessor()
	{
		return $this->hasOne(Professor::className(), ['usuario_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSecretario()
	{
		return $this->hasOne(Secretario::className(), ['usuario_id' => 'id']);
	}

	/**
	 * @param $id
	 * @return static|null
	 */
	public static function findIdentity($id)
	{
		$usuario = Usuario::find()->where(['id' => $id])->one();

		if ($usuario) {
			return new static($usuario);
		}

		return null;
	}

	/**
	 * @param $token
	 * @param null $type
	 * @return |null
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return null;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return |null
	 */
	public function getAuthKey()
	{
		return null;
	}

	/**
	 * @param $authKey
	 * @return |null
	 */
	public function validateAuthKey($authKey)
	{
		return null;
	}

	/**
	 * Validates password
	 *
	 * @param string $password if password provided is valid for current user
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return $this->senha === sha1($password);
	}
}
