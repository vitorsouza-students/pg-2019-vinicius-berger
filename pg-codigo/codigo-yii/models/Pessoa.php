<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "professor".
 *
 * @property integer $id
 * @property string $nome
 * @property string $sobrenome
 * @property string $email
 * @property string $telefone
 * @property integer $usuario_id
 *
 * @property Usuario $usuario
 */
class Pessoa extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['nome', 'sobrenome', 'email'], 'required'],
			[['usuario_id'], 'integer'],
			[['nome', 'sobrenome', 'email'], 'string', 'max' => 100],
			[['telefone'], 'string', 'max' => 15],
			[['email'], 'email'],
			[['email'], 'unique'],
			[['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'nome' => 'Nome',
			'sobrenome' => 'Sobrenome',
			'email' => 'Email',
			'telefone' => 'Telefone',
			'usuario_id' => 'Usuario ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsuario()
	{
		return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
	}
}
