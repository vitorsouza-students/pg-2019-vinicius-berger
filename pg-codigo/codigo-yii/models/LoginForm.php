<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\httpclient\Client;
use yii\helpers\Json;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
	public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
			['rememberMe', 'boolean'],
			['username', 'validateUsername'],
            ['password', 'validatePassword'],
        ];
    }

	public function attributeLabels()
	{
		return [
			'username' => 'Email',
			'password' => 'Senha',
			'rememberMe' => 'Mantenha-me conectado'
		];
	}

	/**
	 * @param $attribute
	 */
	public function validateUsername($attribute)
	{
		if (!$this->hasErrors()) {
			$user = $this->getUser();

			if (!$user) {
				$this->addError($attribute, 'Login inexistente');
			}
		}
	}

	/**
	 * Validates the password.
	 * This method serves as the inline validation for password.
	 *
	 * @param string $attribute the attribute currently being validated
	 */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Senha incorreta');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();

            if(!$this->hasErrors()){
				return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
			}

        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Usuario::find()->where(['login' => $this->username])->one();
        }

        return $this->_user;
    }
}
