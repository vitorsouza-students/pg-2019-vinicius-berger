<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documento".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $nome_arquivo
 * @property string $data_juntada
 * @property integer $afastamento_id
 *
 * @property Afastamento $afastamento
 */
class Documento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'nome_arquivo', 'data_juntada', 'afastamento_id'], 'required'],
            [['data_juntada'], 'safe'],
            [['afastamento_id'], 'integer'],
            [['titulo', 'nome_arquivo'], 'string', 'max' => 100],
            [['afastamento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Afastamento::className(), 'targetAttribute' => ['afastamento_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'nome_arquivo' => 'Nome Arquivo',
            'data_juntada' => 'Data Juntada',
            'afastamento_id' => 'Afastamento ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAfastamento()
    {
        return $this->hasOne(Afastamento::className(), ['id' => 'afastamento_id']);
    }
}
