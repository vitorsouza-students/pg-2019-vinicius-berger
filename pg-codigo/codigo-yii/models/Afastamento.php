<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "afastamento".
 *
 * @property integer $id
 * @property string $data_solicitacao
 * @property string $data_inicio_afastamento
 * @property string $data_fim_afastamento
 * @property string $data_inicio_evento
 * @property string $data_fim_evento
 * @property string $nome_evento
 * @property string $situacao
 * @property string $tipo
 * @property string $onus
 * @property string $motivo
 * @property integer $professor_solicitante_id
 * @property integer $professor_relator_id
 *
 * @property Professor $professorSolicitante
 * @property Professor $professorRelator
 * @property Documento[] $documentos
 * @property Parecer[] $pareceres
 */
class Afastamento extends ActiveRecord
{
	const TIPO_NACIONAL = 'nacional';
	const TIPO_INTERNACIONAL = 'internacional';

	const ONUS_TOTAL = 'total';
	const ONUS_PARCIAL = 'parcial';
	const ONUS_INEXISTENTE = 'inexistente';

	const SITUACAO_INICIADO = 'iniciado';
	const SITUACAO_AGUARDANDO_PARECER_RELATOR = 'aguardando_parecer_relator';
	const SITUACAO_LIBERADO = 'liberado';
    const SITUACAO_AGUARDANDO_DECISAO_DI = 'aguardando_decisao_di';
	const SITUACAO_APROVADO_DI = 'aprovado_di';
	const SITUACAO_APROVADO_CT = 'aprovado_ct';
	const SITUACAO_APROVADO_PRPPG = 'aprovado_prppg';
	const SITUACAO_ARQUIVADO = 'arquivado';
	const SITUACAO_CANCELADO = 'cancelado';
	const SITUACAO_REPROVADO = 'reprovado';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'afastamento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_solicitacao', 'data_inicio_afastamento', 'data_fim_afastamento', 'data_inicio_evento', 'data_fim_evento', 'nome_evento', 'situacao', 'tipo', 'onus', 'professor_solicitante_id'], 'required'],
            [['data_solicitacao', 'data_inicio_afastamento', 'data_fim_afastamento', 'data_inicio_evento', 'data_fim_evento'], 'safe'],
            [['data_inicio_afastamento', 'data_inicio_evento'], 'validarDataInicio'],
            [['situacao', 'tipo', 'onus'], 'string'],
            [['professor_solicitante_id', 'professor_relator_id'], 'integer'],
            [['nome_evento'], 'string', 'max' => 100],
            [['motivo'], 'string', 'max' => 1000],
			[['data_inicio_afastamento', 'data_fim_afastamento', 'data_inicio_evento', 'data_fim_evento'], 'date', 'format' => 'php:Y-m-d'],
            [['professor_solicitante_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_solicitante_id' => 'id']],
            [['professor_relator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Professor::className(), 'targetAttribute' => ['professor_relator_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_solicitacao' => 'Data da Solicitação',
            'data_inicio_afastamento' => 'Data de Início do Afastamento',
            'data_fim_afastamento' => 'Data de Fim do Afastamento',
            'data_inicio_evento' => 'Data de Início do Evento',
            'data_fim_evento' => 'Data de Fim do Evento',
            'nome_evento' => 'Nome do Evento',
            'situacao' => 'Situação',
            'tipo' => 'Tipo',
            'onus' => 'Ônus',
            'motivo' => 'Motivo',
            'professor_solicitante_id' => 'Professor Solicitante',
            'professor_relator_id' => 'Professor Relator',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessorSolicitante()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_solicitante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfessorRelator()
    {
        return $this->hasOne(Professor::className(), ['id' => 'professor_relator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos()
    {
        return $this->hasMany(Documento::className(), ['afastamento_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPareceres()
    {
        return $this->hasMany(Parecer::className(), ['afastamento_id' => 'id']);
    }

	/**
	 * @return string[]
	 */
    public static function recuperarTipos()
	{
		return [
			self::TIPO_NACIONAL => 'Nacional',
			self::TIPO_INTERNACIONAL => 'Internacional'
		];
	}

	/**
	 * @return string[]
	 */
	public static function recuperarOnus()
	{
		return [
			self::ONUS_TOTAL => 'Total',
			self::ONUS_PARCIAL => 'Parcial',
			self::ONUS_INEXISTENTE => 'Inexistente'
		];
	}

	/**
	 * @return string[]
	 */
	public static function recuperarSituacoes()
	{
		return [
			self::SITUACAO_INICIADO => 'Iniciado',
			self::SITUACAO_AGUARDANDO_PARECER_RELATOR => 'Aguardando parecer relator',
			self::SITUACAO_LIBERADO => 'Liberado',
            self::SITUACAO_AGUARDANDO_DECISAO_DI => 'Aguardando decisão DI',
			self::SITUACAO_APROVADO_DI => 'Aprovado DI',
			self::SITUACAO_APROVADO_CT => 'Aprovado CT',
			self::SITUACAO_APROVADO_PRPPG => 'Aprovado PRPPG',
			self::SITUACAO_ARQUIVADO => 'Arquivado',
			self::SITUACAO_CANCELADO => 'Cancelado',
			self::SITUACAO_REPROVADO => 'Reprovado'
		];
	}

    /**
     * @param $post
     * @return Afastamento
     */
//	public static function create($post)
//    {
//	    return new Afastamento();
//    }

    /**
     * @param $attribute
     * @throws \Exception
     */
    public function validarDataInicio($attribute)
    {
        $data_inicio = new \DateTime($this->$attribute, new \DateTimeZone('America/Sao_Paulo'));

        $nome_atributo_fim = str_replace('inicio', 'fim', $attribute);
        $data_fim = new \DateTime($this->$nome_atributo_fim, new \DateTimeZone('America/Sao_Paulo'));

        if ($data_inicio > $data_fim) {
            $this->addError($attribute, 'A data de início não pode ser posterior à data de fim');
        }
    }
}
