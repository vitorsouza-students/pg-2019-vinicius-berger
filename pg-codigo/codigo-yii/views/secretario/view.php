<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Secretario */

$this->title = 'Visualizar cadastro do Secretário';
$this->params['breadcrumbs'][] = ['label' => 'Secretários', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="secretario-view">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
