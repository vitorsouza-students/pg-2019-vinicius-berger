<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Secretario */

$this->title = 'Cadastrar Secretário';
$this->params['breadcrumbs'][] = ['label' => 'Secretários', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="secretario-create">

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
