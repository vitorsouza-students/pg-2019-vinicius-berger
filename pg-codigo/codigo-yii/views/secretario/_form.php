<?php

use app\models\Professor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Pessoa */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="pessoa-form" style="padding-bottom: 20px;">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
    </header>

	<?php $form = ActiveForm::begin(
		[
			'options' => [
				'enctype' => 'multipart/form-data',
				'autocomplete' => 'off'
			],
			'errorCssClass' => 'has-danger',
			'fieldConfig' => [
				'options' => ['class' => 'form-group form-group--float'],
				'labelOptions' => ['class' => 'form-control-label'],
				'inputOptions' => ['class' => 'form-control'],
				'errorOptions' => ['class' => 'help-block'],
				'template' => '{input}{label}<i class="form-group__bar"></i>{error}'
			]
		]
	);
	?>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Informações Básicas</h2>
            <small class="card-subtitle">Quem é o secretário que você deseja cadastrar?</small>
        </div>

        <div class="card-block">
            <div class="row">
                <div class="col-lg-6">
					<?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
					<?= $form->field($model, 'sobrenome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
					<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
					<?= $form->field($model, 'telefone')->widget(MaskedInput::className(),['mask' => '(99) 99999-9999']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php if ($model->isNewRecord) { ?>
        <div class="alert alert-warning" role="alert">
            <h4 class="alert-heading">Atenção:</h4>
            <p class="mb-0">A senha de acesso deste usuário será igual ao email de cadastro. Ele poderá alterar a senha assim que realizar o login no sistema.</p>
        </div>
    <?php } ?>

    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Cadastrar' : 'Salvar Modificações', ['class' => 'btn btn-primary']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>

<?php if (Yii::$app->controller->action->id === 'view') { ?>
    <script>
        $(document).ready(function() {

            $('select').prop('disabled', true);

            $('button').remove();

            $('.form-control').prop('disabled', true);

            $('.form-control').css('opacity', 1);

            $('.form-control').css('border-bottom', 0);

            $('.form-control').css('cursor', 'default');

        });
    </script>
<?php } ?>
