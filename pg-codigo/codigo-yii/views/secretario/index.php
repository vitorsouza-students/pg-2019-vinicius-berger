<?php

use yii\helpers\Html;
use app\components\MyWidgets\DataTable\DataTable;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SecretarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Secretários';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="secretario-index">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
        <small>Gerencie ou cadastre secretários</small>
    </header>

	<?php
	$columns = [
		[
			'label' => 'Nome Completo',
            'format' => 'html',
			'value' => function ($model) {
				if (!empty(Yii::$app->user->identity->secretario) && $model->id === Yii::$app->user->identity->secretario->id) {
					return $model->nome . ' ' . $model->sobrenome . ' <span class="badge badge-pill badge-default" style="margin-left: 10px;">você</span>';
				}
				return $model->nome . ' ' . $model->sobrenome;
			},
			'headerOptions' => ['class' => 'hidden-lg-down'],
			'contentOptions'=> ['class' => 'hidden-lg-down'],
		],
		[
			'attribute' => 'email',
			'headerOptions' => ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
			'contentOptions'=> ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
		],
		[
			'attribute' => 'telefone',
			'headerOptions' => ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
			'contentOptions'=> ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
		],
		[
			'label' => 'Professores',
			'format' => 'html',
			'value' => function ($model) {
				return $model->nome . ' ' . $model->sobrenome . '<br><i>' . $model->email . '</i>';
			},
			'headerOptions' => ['class' => 'hidden-xl-up'],
			'contentOptions'=> ['class' => 'hidden-xl-up'],
		]
	];

	$inputsFormSearch = [
		[
			'class' => 'col-lg-4',
			'tipo' => 'textinput',
			'atributo' => 'nome'
		],
		[
			'class' => 'col-lg-4',
			'tipo' => 'textinput',
			'atributo' => 'email'
		],
		[
			'class' => 'col-lg-4',
			'tipo' => 'textinput',
			'atributo' => 'telefone'
		]
	];

	$buttonsColunaOpcoes = [
		'update' => function($url, $model, $key) {
			return Html::a('<i class="font-sendpayfont text-primary">e</i>', $url);
		},
		'delete' => function($url, $model, $key) {
			return Html::a('<i class="font-sendpayfont text-primary">r</i>', $url, [
				'data-method' => 'post',
				'data-confirm' => 'Deseja realmente excluir o item?'
			]);
		}
	];

	echo DataTable::widget([
		'labelBotaoCriar' => 'Quero cadastrar um secretário',
		'labelBotaoExportarPDF' => 'Exportar em PDF',
		'urlBotaoExportarPDF' => '',
		'exibirBotaoExportarPDF' => false,
		'labelBotaoExportarCSV' => 'Exportar em CSV',
		'urlBotaoExportarCSV' => '',
		'exibirBotaoExportarCSV' => false,
		'dataProvider' => $dataProvider,
		'searchModel' => $searchModel,
		'columns' => $columns,
		'inputs' => [],
		'buttonsColunaOpcoes' => $buttonsColunaOpcoes
	]);
	?>

</div>
