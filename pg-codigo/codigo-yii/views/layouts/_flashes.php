
<?php

use yii\web\View;

$script = '';

foreach (Yii::$app->session->getAllFlashes() as $message) {

	if ($message['type'] === 'success') {
		$script = "
			swal({
				title: 'Feito!',
				text: '".$message['message']."',
				type: 'success',
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary'
			})
		";
	}
	if ($message['type'] === 'danger') {
		$script = "
			swal({
				title: 'Más notícias...',
				text: '".$message['message']."',
				type: 'error',
				buttonsStyling: false,
				confirmButtonClass: 'btn btn-primary'
			})
		";
	}
}

$this->registerJs($script, View::POS_END);


