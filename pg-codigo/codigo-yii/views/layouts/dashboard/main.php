<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $content string */

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="<?= Url::base() ?>/js/jquery-1.7.2.js"></script>
</head>
<body>

    <?php $this->beginBody() ?>

        <body data-ma-theme="light-blue">
            <main class="main main--alt">

				<?= $this->render('../_flashes') ?>

                <?= $this->render('../_page-loader') ?>

                <?= $this->render('_header') ?>

                <section class="content content--full">
                    <div class="content__inner">
                        <?= $content ?>
                    </div>
                </section>
            </main>

    <?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>

