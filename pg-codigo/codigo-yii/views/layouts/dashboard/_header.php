<?php

use yii\helpers\Html;

$usuario_logado = Yii::$app->user->identity;

$currentController = Yii::$app->controller->id;
$currentAction = Yii::$app->controller->action->id;

/**
 * OPÇÕES (INCLUSIVE DENTRO DE SUB-MENÚS)
 */
$afastamentos = $currentController === 'afastamento' ? 'active' : '';
$professores = $currentController === 'professor' ? 'active' : '';
$secretarios = $currentController === 'secretario' ? 'active' : '';
?>

<header class="header">
    <div class="header__logo">
        <h1><?= Html::a( Html::img('@web/images/logo/logo.svg', ['style' => 'width: 100px;']), ['/afastamento'])?></h1>
    </div>

    <ul class="top-nav">
        <li class="dropdown">
            <a href="" data-toggle="dropdown" aria-expanded="false" style="display: flex; align-items: center;"><i class="font-sendpayfont" style="font-size: 18px;">a</i><span style="margin-left: 10px;"><?= $usuario_logado->login ?></span></a>
            <div class="dropdown-menu dropdown-menu-right">
                <?= Html::a('Trocar senha', ['/usuario/trocar-senha'], ['class' => 'dropdown-item']) ?>
				<?= Html::a('Sair', ['/site/logout'], ['data-method' => 'post', 'class' => 'dropdown-item']) ?>
            </div>
        </li>
    </ul>

    <ul class="nav top-menu">
        <li class="nav-item <?= $afastamentos ?>"><?= Html::a('Afastamentos', ['/afastamento'], ['class' => 'nav-link'])?></li>
        <li class="nav-item <?= $professores ?>"><?= Html::a('Professores', ['/professor'], ['class' => 'nav-link'])?></li>
        <li class="nav-item <?= $secretarios ?>"><?= Html::a('Secretários', ['/secretario'], ['class' => 'nav-link'])?></li>
    </ul>
</header>
