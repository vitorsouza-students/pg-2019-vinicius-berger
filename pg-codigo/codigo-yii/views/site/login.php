<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */


app\assets\AppAsset::register($this);

$this->title = 'Login';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body data-ma-theme="light-blue">
<?php $this->beginBody() ?>

    <?= $this->render('../layouts/_flashes') ?>

    <div class="login">

        <div class="login__block active" id="l-login">
            <div class="login__block__header" style="text-align: center;">
                <h1><?= Html::a(Html::img('@web/images/logo/logo.svg', ['style' => 'width:100px;']), ['/dashboard/index']) ?></h1>
            </div>

            <?php
            $form = ActiveForm::begin(
                [
                    'id' => 'form-pessoa',
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'autocomplete' => 'off'
                    ],
                    'fieldConfig' => [
                        'options' => ['class' => 'form-group form-group--float'],
                        'labelOptions' => ['class' => 'form-control-label'],
                        'inputOptions' => ['class' => 'form-control'],
                        'errorOptions' => ['class' => 'help-block'],
                        'template' => '{input}{label}<i class="form-group__bar"></i>{error}'
                    ],
                ]
            );
            ?>
            <div class="login__block__body">

                <div class="form-group form-group--float form-group--centered">
                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                </div>

                <div class="form-group form-group--float form-group--centered">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>

                <div style="text-align: right; margin-top: 60px;">
                    <?= Html::submitButton('Entrar', ['class'=>'btn btn-primary']) ?>
                </div>

            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
