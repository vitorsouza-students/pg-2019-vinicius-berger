<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Afastamento */

$this->title = 'Editar afastamento';
$this->params['breadcrumbs'][] = ['label' => 'Afastamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="afastamento-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
