<?php

use app\models\Afastamento;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Afastamento */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="afastamento-form" style="padding-bottom: 20px;">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
    </header>

	<?php $form = ActiveForm::begin(
		[
			'options' => [
				'enctype' => 'multipart/form-data',
				'autocomplete' => 'off'
			],
			'errorCssClass' => 'has-danger',
			'fieldConfig' => [
				'options' => ['class' => 'form-group form-group--float'],
				'labelOptions' => ['class' => 'form-control-label'],
				'inputOptions' => ['class' => 'form-control'],
				'errorOptions' => ['class' => 'help-block'],
				'template' => '{input}{label}<i class="form-group__bar"></i>{error}'
			]
		]
	);
	?>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Informações Básicas</h2>
            <small class="card-subtitle">Qual é o período do afastamento e qual o motivo?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-3">
					<?= $form->field($model, 'data_inicio_afastamento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'data_fim_afastamento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'tipo')->dropDownList(Afastamento::recuperarTipos(), ['prompt' => '']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'onus')->dropDownList(Afastamento::recuperarOnus(), ['prompt' => '']) ?>
                </div>
                <div class="col-lg-12">
					<?= $form->field($model, 'motivo')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Sobre o Evento</h2>
            <small class="card-subtitle">Que evento é este e quando vai ocorrer?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-6">
					<?= $form->field($model, 'nome_evento')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'data_inicio_evento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'data_fim_evento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card" id="app">
        <div class="card-header">
            <h2 class="card-title">Documentos</h2>
            <small class="card-subtitle">Quais documentos justificam a solicitação de afastamento?</small>
        </div>
        <div class="card-block__nova-linha animated fadeInUp" v-for="(documento, i) in documentos" :key="i">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Título</label>
                        <input v-model="documento.titulo" :name="'Documento['+i+'][titulo]'" type="text" class="form-control" placeholder="ex: Passagens aéreas, Banner do evento..." autocomplete="off" maxlength="100">
                        <i class="form-group__bar"></i>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Arquivo</label>
                        <input type="file" :name="'Documento['+i+'][nome_arquivo]'" class="form-control" autocomplete="off">
                        <i class="form-group__bar"></i>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="form-group" style="margin-top: 2rem;">
                        <button type="button" class="btn btn-outline-danger waves-effect" @click="removerDocumento(i)">Remover documento</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-3">
                    <button type="button" class="btn btn-outline-primary waves-effect" @click="adicionarDocumento">Adicionar documento</button>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Cadastrar' : 'Salvar Modificações', ['class' => 'btn btn-primary']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>

<!--VUEJS-->
<!-- development version, includes helpful console warnings -->
<script src="<?= Url::base() ?>/js/vue.js"></script>
<!-- production version, optimized for size and speed -->
<!--<script src="https://cdn.jsdelivr.net/npm/vue"></script>-->
<!-- V-MASK -->
<script src="<?= Url::base() ?>/js/v-mask.min.js"></script>
<script>
    Vue.directive('mask', VueMask.VueMaskDirective);
    var app = new Vue({
        el: '#app',
        data: {
            documentos: <?= Json::encode($model->documentos) ?>
        },
        methods: {
            adicionarDocumento: function () {
                let novo = {
                    titulo: '',
                    nome_arquivo: ''
                }
                this.documentos.push(novo)
            },
            removerDocumento: function (i) {
                this.documentos.splice(i, 1)
            }
        }
    })
</script>
