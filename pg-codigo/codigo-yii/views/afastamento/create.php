<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Afastamento */

$this->title = 'Solicitar meu afastamento';
$this->params['breadcrumbs'][] = ['label' => 'Afastamentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="afastamento-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
