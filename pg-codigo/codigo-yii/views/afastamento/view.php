<?php

use app\models\Afastamento;
use app\models\Parecer;
use app\models\Professor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Afastamento */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Acompanhar afastamento';

$model->data_inicio_afastamento = implode('/', array_reverse( explode('-', $model->data_inicio_afastamento)));
$model->data_fim_afastamento = implode('/', array_reverse( explode('-', $model->data_fim_afastamento)));
$model->data_inicio_evento = implode('/', array_reverse( explode('-', $model->data_inicio_evento)));
$model->data_fim_evento = implode('/', array_reverse( explode('-', $model->data_fim_evento)));
?>

<div class="afastamento-view" style="padding-bottom: 20px;">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
    </header>

	<?php $form = ActiveForm::begin(
		[
			'options' => [
				'enctype' => 'multipart/form-data',
				'autocomplete' => 'off'
			],
			'errorCssClass' => 'has-danger',
			'fieldConfig' => [
				'options' => ['class' => 'form-group form-group--float'],
				'labelOptions' => ['class' => 'form-control-label'],
				'inputOptions' => ['class' => 'form-control'],
				'errorOptions' => ['class' => 'help-block'],
				'template' => '{input}{label}<i class="form-group__bar"></i>{error}'
			]
		]
	);
	?>

    <!-- OPÇÕES -->
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Opções</h2>
            <small class="card-subtitle">O que você deseja fazer com este afastamento?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-12">
                    <!-- SOLICITANTE -->
                    <?= Html::button('Cancelar', ['class' => 'btn btn-primary', 'id' => 'button-cancelar', 'disabled' => in_array($model->situacao, [Afastamento::SITUACAO_CANCELADO, Afastamento::SITUACAO_ARQUIVADO, Afastamento::SITUACAO_REPROVADO])]) ?>

                    <!-- CHEFE DO DEPARTAMENTO -->
                    <?= Html::button('Encaminhar', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal-encaminhar-afastamento', 'disabled' => !in_array($model->situacao, [Afastamento::SITUACAO_INICIADO])]) ?>

                    <!-- RELATOR -->
                    <?= Html::button('Registrar parecer relator', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal-parecer-relator', 'disabled' => !in_array($model->situacao, [Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR])]) ?>

                    <!-- PROFESSORES -->
                    <?= Html::button('Manifestar-se contra', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal-manifestar-se-contra', 'disabled' => !in_array($model->situacao, [Afastamento::SITUACAO_LIBERADO])]) ?>

                    <!-- SECRETÁRIOS -->
                    <?= Html::button('Registrar decisão DI', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal-decisao-di', 'disabled' => !in_array($model->situacao, [Afastamento::SITUACAO_AGUARDANDO_DECISAO_DI])]) ?>
                    <?= Html::button('Registrar decisão CT', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal-decisao-ct', 'disabled' => !in_array($model->situacao, [Afastamento::SITUACAO_APROVADO_DI]) || $model->tipo === Afastamento::TIPO_NACIONAL]) ?>
                    <?= Html::button('Registrar decisão PRPPG', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal-decisao-prppg', 'disabled' => !in_array($model->situacao, [Afastamento::SITUACAO_APROVADO_CT]) || $model->tipo === Afastamento::TIPO_NACIONAL]) ?>
                    <?= Html::button('Arquivar', ['class' => 'btn btn-primary', 'id' => 'button-arquivar', 'disabled' => !in_array($model->situacao, [Afastamento::SITUACAO_APROVADO_PRPPG, Afastamento::SITUACAO_REPROVADO, Afastamento::SITUACAO_APROVADO_DI, Afastamento::SITUACAO_CANCELADO]) || ($model->situacao === Afastamento::SITUACAO_APROVADO_DI && $model->tipo === Afastamento::TIPO_INTERNACIONAL)]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Informações Básicas</h2>
            <small class="card-subtitle">Qual é o período do afastamento e qual o motivo?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
						<?php switch ($model->situacao) {
							case Afastamento::SITUACAO_INICIADO:
								echo '<span class="badge badge-pill badge-primary">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR:
								echo '<span class="badge badge-pill badge-warning">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_LIBERADO:
								echo '<span class="badge badge-pill badge-info">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
                            case Afastamento::SITUACAO_AGUARDANDO_DECISAO_DI:
                                echo '<span class="badge badge-pill badge-warning">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_APROVADO_DI:
								echo '<span class="badge badge-pill badge-success">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_APROVADO_CT:
								echo '<span class="badge badge-pill badge-success">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_APROVADO_PRPPG:
								echo '<span class="badge badge-pill badge-success">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_ARQUIVADO:
								echo '<span class="badge badge-pill badge-default">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_CANCELADO:
								echo '<span class="badge badge-pill badge-default">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							case Afastamento::SITUACAO_REPROVADO:
								echo '<span class="badge badge-pill badge-danger">'.$model->recuperarSituacoes()[$model->situacao].'</span>'; break;
							default:
								return $model->situacao;
						} ?>
                    </div>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'data_inicio_afastamento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'data_fim_afastamento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'tipo')->dropDownList(Afastamento::recuperarTipos(), ['prompt' => '']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'onus')->dropDownList(Afastamento::recuperarOnus(), ['prompt' => '']) ?>
                </div>
                <div class="col-lg-12">
					<?= $form->field($model, 'motivo')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Sobre o Evento</h2>
            <small class="card-subtitle">Que evento é este e quando vai ocorrer?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-6">
					<?= $form->field($model, 'nome_evento')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'data_inicio_evento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model, 'data_fim_evento')->widget(MaskedInput::className(),['mask' => '99/99/9999']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Professor Solicitante</h2>
            <small class="card-subtitle">Quem solicitou o afastamento?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-3">
					<?= $form->field($model->professorSolicitante, 'nome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model->professorSolicitante, 'sobrenome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model->professorSolicitante, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model->professorSolicitante, 'telefone')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <?php if (!empty($model->professorRelator)) { ?>
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Professor Relator</h2>
            <small class="card-subtitle">Quem é o relator do afastamento?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-3">
					<?= $form->field($model->professorRelator, 'nome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model->professorRelator, 'sobrenome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model->professorRelator, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-3">
					<?= $form->field($model->professorRelator, 'telefone')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

	<?php if (!empty($model->documentos)) { ?>
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Documentos</h2>
            <small class="card-subtitle">Quais documentos justificam a solicitação de afastamento?</small>
        </div>
        <div class="card-block">
            <div class="row">
				<?php foreach ($model->documentos as $documento) { ?>
                    <div class="col-lg-12">
						<?= Yii::$app->formatter->asDatetime($documento->data_juntada) ?>
                        <b style="margin-left: 10px;">
						    <?= !empty($documento->titulo) ? $documento->titulo : '' ?>
                        </b>
<!--						--><?//= Html::a('Abrir em nova aba', Yii::$app->ftp->getUrlBaseDocumentos().'/'.$documento->nome_arquivo, ['class' => 'btn btn-link', 'target' => '_blank']) ?>
                        <?= Html::a('Abrir em nova aba', '../../../uploads/' . $documento->nome_arquivo, ['class' => 'btn btn-link', 'target' => '_blank']) ?>
                    </div>
				<?php } ?>
            </div>
        </div>
    </div>
	<?php } ?>

	<?php if (!empty($model->pareceres)) { ?>
    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Pareceres</h2>
            <small class="card-subtitle">Quais são os pareceres até o momento?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <?php foreach ($model->pareceres as $parecer) { ?>
                    <div class="col-lg-12">
						<?= $parecer->tipo_parecer === Parecer::TIPO_PARECER_DESFAVORAVEL ? '<i class="zmdi zmdi-close-circle zmdi-hc-fw" style="color: red; font-size: large;"></i>' : '<i class="zmdi zmdi-check-circle zmdi-hc-fw" style="color: green; font-size: large;"></i>' ?>
                        <?= Yii::$app->formatter->asDate($parecer->data_parecer) ?>
                        <b style="margin-left: 10px;">
                            <?= $parecer->professor->nome.' '.$parecer->professor->sobrenome ?>
                            <?= $parecer->professor->id === $model->professor_relator_id ? '(Relator)' : '' ?>
                        </b>
                        <i style="margin-left: 10px;">
                            <?= !empty($parecer->motivo) ? $parecer->motivo : '' ?>
                        </i>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
	<?php } ?>

	<?php ActiveForm::end(); ?>

</div>

<!-- REGISTRAR DECISÃO DI -->
<div class="modal fade" id="modal-decisao-di" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= Html::beginForm(['registrar-decisao-di', 'id' => $model->id], 'post', ['enctype' => 'multipart/form-data', 'onsubmit' => 'exibirLoading()']) ?>
            <div class="modal-header">
                <h5 class="modal-title pull-left">Registrar decisão do DI</h5>
            </div>
            <div class="modal-body">
                <div class="form-group form-group--float">
                    <select class="form-control" name="decisao" aria-invalid="false">
                        <option value="favoravel">Favorável</option>
                        <option value="desfavoravel">Desfavorável</option>
                    </select>
                    <label class="form-control-label">Decisão</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-12">
                        <b>Anexar Documento:</b>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Título</label>
                            <input name="Documento[0][titulo]" type="text" class="form-control" placeholder="ex: ATA da reunião..." autocomplete="off" maxlength="100">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Arquivo</label>
                            <input type="file" name="Documento[0][nome_arquivo]" class="form-control" autocomplete="off">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary waves-effect">Salvar decisão</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>

<!-- REGISTRAR DECISÃO CT -->
<div class="modal fade" id="modal-decisao-ct" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
			<?= Html::beginForm(['registrar-decisao-ct', 'id' => $model->id], 'post', ['enctype' => 'multipart/form-data', 'onsubmit' => 'exibirLoading()']) ?>
            <div class="modal-header">
                <h5 class="modal-title pull-left">Registrar decisão do CT</h5>
            </div>
            <div class="modal-body">
                <div class="form-group form-group--float">
                    <select class="form-control" name="decisao" aria-invalid="false">
                        <option value="favoravel">Favorável</option>
                        <option value="desfavoravel">Desfavorável</option>
                    </select>
                    <label class="form-control-label">Decisão</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-12">
                        <b>Anexar Documento:</b>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Título</label>
                            <input name="Documento[0][titulo]" type="text" class="form-control" placeholder="ex: ATA da reunião..." autocomplete="off" maxlength="100">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Arquivo</label>
                            <input type="file" name="Documento[0][nome_arquivo]" class="form-control" autocomplete="off">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary waves-effect">Salvar decisão</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
			<?= Html::endForm() ?>
        </div>
    </div>
</div>

<!-- REGISTRAR DECISÃO PRPPG -->
<div class="modal fade" id="modal-decisao-prppg" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
			<?= Html::beginForm(['registrar-decisao-prppg', 'id' => $model->id], 'post', ['enctype' => 'multipart/form-data', 'onsubmit' => 'exibirLoading()']) ?>
            <div class="modal-header">
                <h5 class="modal-title pull-left">Registrar decisão da PRPPG</h5>
            </div>
            <div class="modal-body">
                <div class="form-group form-group--float">
                    <select class="form-control" name="decisao" aria-invalid="false">
                        <option value="favoravel">Favorável</option>
                        <option value="desfavoravel">Desfavorável</option>
                    </select>
                    <label class="form-control-label">Decisão</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-12">
                        <b>Anexar Documento:</b>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Título</label>
                            <input name="Documento[0][titulo]" type="text" class="form-control" placeholder="ex: ATA da reunião..." autocomplete="off" maxlength="100">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Arquivo</label>
                            <input type="file" name="Documento[0][nome_arquivo]" class="form-control" autocomplete="off">
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary waves-effect">Salvar decisão</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
			<?= Html::endForm() ?>
        </div>
    </div>
</div>

<!-- REGISTRAR PARECER RELATOR -->
<div class="modal fade" id="modal-parecer-relator" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
			<?= Html::beginForm(['registrar-parecer-relator', 'id' => $model->id], 'post', ['onsubmit' => 'exibirLoading()']) ?>
            <div class="modal-header">
                <h5 class="modal-title pull-left">Registrar parecer do Relator</h5>
            </div>
            <div class="modal-body">
                <div class="form-group form-group--float">
                    <select class="form-control" name="Parecer[tipo_parecer]" aria-invalid="false">
                        <option value="favoravel">Favorável</option>
                        <option value="desfavoravel">Desfavorável</option>
                    </select>
                    <label class="form-control-label">Parecer</label>
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group">
                    <textarea name="Parecer[motivo]" class="form-control" rows="5" placeholder="Digite o motivo deste parecer..."></textarea>
                    <i class="form-group__bar"></i>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary waves-effect">Salvar parecer</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
			<?= Html::endForm() ?>
        </div>
    </div>
</div>

<!-- MANIFESTAR-SE CONTRA -->
<div class="modal fade" id="modal-manifestar-se-contra" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
			<?= Html::beginForm(['manifestar-se-contra', 'id' => $model->id], 'post', ['onsubmit' => 'exibirLoading()']) ?>
            <div class="modal-header">
                <h5 class="modal-title pull-left">Manifestar-se contra este afastamento</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea name="Parecer[motivo]" class="form-control" rows="5" placeholder="Digite o motivo deste parecer..."></textarea>
                    <i class="form-group__bar"></i>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary waves-effect">Salvar parecer</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
			<?= Html::endForm() ?>
        </div>
    </div>
</div>

<!-- ENCAMINHAR AFASTAMENTO -->
<div class="modal fade" id="modal-encaminhar-afastamento" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
			<?= Html::beginForm(['encaminhar-afastamento', 'id' => $model->id], 'post', ['onsubmit' => 'exibirLoading()']) ?>
            <div class="modal-header">
                <h5 class="modal-title pull-left">Encaminhar afastamento</h5>
            </div>
            <div class="modal-body">
                <div class="form-group form-group--float">
                    <select class="form-control" name="Afastamento[professor_relator_id]" aria-invalid="false">
						<?php
						$professores = ArrayHelper::map(Professor::find()->all(), 'id', function ($model) {return $model->nome.' '.$model->sobrenome;});
						$ids_parentes = ArrayHelper::getColumn($model->professorSolicitante->meusParentes, 'professor_parente_id');
						foreach ($professores as $id => $nome) {
							if ($id !== $model->professorSolicitante->id) {
								echo in_array($id, $ids_parentes) ?
									'<option value="'.$id.'" disabled>'.$nome.' (é parente do solicitante)</option>' :
									'<option value="'.$id.'">'.$nome.'</option>';
							}
						}
						?>

                    </select>
                    <label class="form-control-label">Relator</label>
                    <i class="form-group__bar"></i>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary waves-effect">Salvar relator</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancelar</button>
            </div>
			<?= Html::endForm() ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('.afastamento-view .form-control').prop('disabled', true);

        $('.afastamento-view .form-control').css('opacity', 1);

        $('.afastamento-view .form-control').css('border-bottom', 0);

        $('.afastamento-view .form-control').css('cursor', 'default');

        // CANCELAR AFASTAMENTO
        $('#button-cancelar').click(function(){
            exibirConfirmacao().then(function(){
                exibirLoading()
                window.location.href = "cancelar?id=<?= $model->id ?>"
            });
        });

        // ARQUIVAR AFASTAMENTO
        $('#button-arquivar').click(function(){
            exibirConfirmacao().then(function(){
                exibirLoading()
                window.location.href = "arquivar?id=<?= $model->id ?>"
            });
        });
    });

    function exibirConfirmacao(){
        return swal({
            title: 'Tem certeza que deseja fazer isso?',
            text: 'Você não poderá desfazer essa ação depois',
            type: 'warning',
            showCancelButton: true,
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-danger',
            confirmButtonText: 'Sim, eu tenho certeza!',
            cancelButtonClass: 'btn btn-secondary',
            cancelButtonText: 'Cancelar',
        })
    }

    function exibirLoading(){
        return swal({
            html: "" +
                "<div class='page-loader__spinner' style='left: 50%; transform: translate(-50%, 0);'>" +
                "<svg viewBox='25 25 50 50'>" +
                "<circle cx='50' cy='50' r='20' fill='none' stroke-width='2' stroke-miterlimit='10' />" +
                "</svg>" +
                "</div>",
            title: "Aguarde um instante...",
            showConfirmButton: false,
            allowOutsideClick: false
        })
    }
</script>
