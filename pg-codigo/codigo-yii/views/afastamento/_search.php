<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AfastamentoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="afastamento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'data_solicitacao') ?>

    <?= $form->field($model, 'data_inicio_afastamento') ?>

    <?= $form->field($model, 'data_fim_afastamento') ?>

    <?= $form->field($model, 'data_inicio_evento') ?>

    <?php // echo $form->field($model, 'data_fim_evento') ?>

    <?php // echo $form->field($model, 'nome_evento') ?>

    <?php // echo $form->field($model, 'situacao') ?>

    <?php // echo $form->field($model, 'tipo') ?>

    <?php // echo $form->field($model, 'onus') ?>

    <?php // echo $form->field($model, 'motivo') ?>

    <?php // echo $form->field($model, 'professor_solicitante_id') ?>

    <?php // echo $form->field($model, 'professor_relator_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
