<?php

use app\models\Afastamento;
use app\models\Professor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\components\MyWidgets\DataTable\DataTable;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AfastamentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Afastamentos';
$this->params['breadcrumbs'][] = $this->title;

function badgeSituacao ($model) {
    switch ($model->situacao) {
        case Afastamento::SITUACAO_INICIADO:
            return '<span class="badge badge-pill badge-primary">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR:
            return '<span class="badge badge-pill badge-warning">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_LIBERADO:
            return '<span class="badge badge-pill badge-info">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_AGUARDANDO_DECISAO_DI:
            return '<span class="badge badge-pill badge-warning">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_APROVADO_DI:
            return '<span class="badge badge-pill badge-success">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_APROVADO_CT:
            return '<span class="badge badge-pill badge-success">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_APROVADO_PRPPG:
            return '<span class="badge badge-pill badge-success">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_ARQUIVADO:
            return '<span class="badge badge-pill badge-default">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_CANCELADO:
            return '<span class="badge badge-pill badge-default">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        case Afastamento::SITUACAO_REPROVADO:
            return '<span class="badge badge-pill badge-danger">' . $model->recuperarSituacoes()[$model->situacao] . '</span>';
        default:
            return $model->situacao;
    }
}
?>
<div class="afastamento-index">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
        <small>Acompanhe todos os afastamentos solicitados por professores</small>
    </header>

	<?php
	$columns = [
		[
			'attribute' => 'data_solicitacao',
			'format' => ['date', 'php:d/m/Y'],
			'headerOptions' => ['class' => 'hidden-lg-down'],
			'contentOptions'=> ['class' => 'hidden-lg-down'],
		],
		[
            'label' => 'Professor Solicitante',
			'format' => 'html',
			'value' => function ($model) {
				return $model->professorSolicitante->nome.' '.$model->professorSolicitante->sobrenome;
			},
			'headerOptions' => ['class' => 'hidden-lg-down'],
			'contentOptions'=> ['class' => 'hidden-lg-down'],
		],
		[
			'attribute' => 'tipo',
			'format' => 'html',
			'value' => function ($model) {
	            return $model->tipo === Afastamento::TIPO_NACIONAL ? Html::img('@web/images/brasil.svg', ['style' => 'width: 30px;']) :  Html::img('@web/images/mundo.svg', ['style' => 'width: 30px;']);
			},
			'headerOptions' => ['class' => 'hidden-lg-down'],
			'contentOptions'=> ['class' => 'hidden-lg-down'],
		],
		[
			'attribute' => 'situacao',
			'format' => 'html',
			'value' => function ($model) {
	            return badgeSituacao($model);
            },
			'headerOptions' => ['class' => 'hidden-lg-down'],
			'contentOptions'=> ['class' => 'hidden-lg-down'],
		],
		[
			'label' => 'Afastamentos',
			'format' => 'html',
			'value' => function ($data) {
				return
                    $data->professorSolicitante->nome.' '.$data->professorSolicitante->sobrenome.'<br>'.
                    '<i style="margin-right: 20px;">'.implode('/', array_reverse( explode('-', $data->data_solicitacao))).'</i>'.
                    badgeSituacao($data);
			},
			'headerOptions' => ['class' => 'hidden-xl-up'],
			'contentOptions'=> ['class' => 'hidden-xl-up'],
		]
	];

	$inputsFormSearch = [
		[
			'class' => 'col-lg-3',
			'tipo' => 'data',
			'atributo' => 'data_solicitacao'
		],
		[
			'class' => 'col-lg-3',
			'tipo' => 'dropdown',
			'atributo' => 'professor_solicitante_id',
			'opcoes' => ArrayHelper::map(
				Professor::find()->all(),
				'id',
				function ($model) {
					return $model->nome.' '.$model->sobrenome;
				}
			)
		],
		[
			'class' => 'col-lg-3',
			'tipo' => 'dropdown',
			'atributo' => 'tipo',
			'opcoes' => Afastamento::recuperarTipos()
		],
		[
			'class' => 'col-lg-3',
			'tipo' => 'dropdown',
			'atributo' => 'situacao',
			'opcoes' => Afastamento::recuperarSituacoes()
		]
	];

	$buttonsColunaOpcoes = [
		'view' => function($url, $model, $key) {
			return Html::a('<i class="font-sendpayfont text-primary">y</i>', $url);
		}
	];

	echo DataTable::widget([
		'labelBotaoCriar' => 'Quero solicitar meu afastamento',
		'labelBotaoExportarPDF' => 'Exportar em PDF',
		'urlBotaoExportarPDF' => '',
		'exibirBotaoExportarPDF' => false,
		'labelBotaoExportarCSV' => 'Exportar em CSV',
		'urlBotaoExportarCSV' => '',
		'exibirBotaoExportarCSV' => false,
		'dataProvider' => $dataProvider,
		'searchModel' => $searchModel,
		'columns' => $columns,
		'inputs' => $inputsFormSearch,
		'buttonsColunaOpcoes' => $buttonsColunaOpcoes
	]);
	?>
</div>
