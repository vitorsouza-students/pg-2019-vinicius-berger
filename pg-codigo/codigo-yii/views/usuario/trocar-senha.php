<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = 'Trocar senha';
$this->params['breadcrumbs'][] = $this->title;

$model->senha = '';
?>
<div class="usuario-trocar-senha">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
    </header>

    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'enctype' => 'multipart/form-data',
                'autocomplete' => 'off'
            ],
            'errorCssClass' => 'has-danger',
            'fieldConfig' => [
                'options' => ['class' => 'form-group form-group--float'],
                'labelOptions' => ['class' => 'form-control-label'],
                'inputOptions' => ['class' => 'form-control'],
                'errorOptions' => ['class' => 'help-block'],
                'template' => '{input}{label}<i class="form-group__bar"></i>{error}'
            ]
        ]
    );
    ?>

    <div class="card" style="max-width: 500px;">
        <div class="card-header">
            <h2 class="card-title">Nova Senha</h2>
            <small class="card-subtitle">Qual é a nova senha que você deseja usar?</small>
        </div>

        <div class="card-block">
            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'senha')->passwordInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Confirmar alteração', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
