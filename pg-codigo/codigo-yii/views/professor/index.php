<?php

use app\models\Mandato;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\components\MyWidgets\DataTable\DataTable;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfessorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Professores';
$this->params['breadcrumbs'][] = $this->title;

$chefes_departamento = Mandato::recuperarChefeDepartamento();
$ids_chefes_departamento = ArrayHelper::getColumn($chefes_departamento, 'id');
?>
<div class="professor-index">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
        <small>Gerencie ou cadastre professores</small>
    </header>

	<?php if (empty($ids_chefes_departamento)) { ?>
        <div class="alert alert-warning" role="alert">
            <h4 class="alert-heading">Atenção:</h4>
            <p class="mb-0">
                Nenhum professor está exercendo o cargo de chefe do departamento,
                pois não há mandato cadastrado que esteja em vigor. Por favor,
                localize e edite o cadastro do professor que efetivamente é o
                chefe do departamento, fornecendo as datas de início e término
                do mandato.
            </p>
        </div>
	<?php } ?>

	<?php
	$columns = [
		[
			'label' => 'Nome Completo',
            'format' => 'html',
            'value' => function ($model) use ($ids_chefes_departamento) {
				if (in_array($model->id, $ids_chefes_departamento)) {
					return $model->nome . ' ' . $model->sobrenome . ' <span class="badge badge-pill badge-default" style="margin-left: 10px;">chefe do departamento</span>';
				}
	            return $model->nome . ' ' . $model->sobrenome;
            },
			'headerOptions' => ['class' => 'hidden-lg-down'],
			'contentOptions'=> ['class' => 'hidden-lg-down'],
		],
		[
			'attribute' => 'email',
			'headerOptions' => ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
			'contentOptions'=> ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
		],
		[
			'attribute' => 'telefone',
			'headerOptions' => ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
			'contentOptions'=> ['class' => 'hidden-lg-down', 'style' => 'text-align: right'],
		],
		[
			'label' => 'Professores',
			'format' => 'html',
			'value' => function ($model) {
				return $model->nome . ' ' . $model->sobrenome . '<br><i>' . $model->email . '</i>';
			},
			'headerOptions' => ['class' => 'hidden-xl-up'],
			'contentOptions'=> ['class' => 'hidden-xl-up'],
		]
	];

	$inputsFormSearch = [
		[
			'class' => 'col-lg-4',
			'tipo' => 'textinput',
			'atributo' => 'nome'
		],
		[
			'class' => 'col-lg-4',
			'tipo' => 'textinput',
			'atributo' => 'email'
		],
		[
            'class' => 'col-lg-4',
            'tipo' => 'textinput',
            'atributo' => 'telefone'
        ]
	];

	$buttonsColunaOpcoes = [
		'update' => function($url, $model, $key) {
			return Html::a('<i class="font-sendpayfont text-primary">e</i>', $url);
		},
		'delete' => function($url, $model, $key) {
			return Html::a('<i class="font-sendpayfont text-primary">r</i>', $url, [
				'data-method' => 'post',
				'data-confirm' => 'Deseja realmente excluir o item?'
			]);
		}
	];

	echo DataTable::widget([
		'labelBotaoCriar' => 'Quero cadastrar um professor',
		'labelBotaoExportarPDF' => 'Exportar em PDF',
		'urlBotaoExportarPDF' => '',
		'exibirBotaoExportarPDF' => false,
		'labelBotaoExportarCSV' => 'Exportar em CSV',
		'urlBotaoExportarCSV' => '',
		'exibirBotaoExportarCSV' => false,
		'dataProvider' => $dataProvider,
		'searchModel' => $searchModel,
		'columns' => $columns,
		'inputs' => [],
		'buttonsColunaOpcoes' => $buttonsColunaOpcoes
	]);
	?>

</div>
