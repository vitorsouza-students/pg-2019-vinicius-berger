<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Professor */

$this->title = 'Editar cadastro do Professor';
$this->params['breadcrumbs'][] = ['label' => 'Professores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="professor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
