<?php

use app\models\Professor;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\models\Pessoa */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="pessoa-form" style="padding-bottom: 20px;">

    <header class="content__title">
        <h1><?= Html::encode($this->title) ?></h1>
    </header>

	<?php $form = ActiveForm::begin(
		[
			'options' => [
				'enctype' => 'multipart/form-data',
				'autocomplete' => 'off'
			],
			'errorCssClass' => 'has-danger',
			'fieldConfig' => [
				'options' => ['class' => 'form-group form-group--float'],
				'labelOptions' => ['class' => 'form-control-label'],
				'inputOptions' => ['class' => 'form-control'],
				'errorOptions' => ['class' => 'help-block'],
				'template' => '{input}{label}<i class="form-group__bar"></i>{error}'
			]
		]
	);
	?>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Informações Básicas</h2>
            <small class="card-subtitle">Quem é o professor que você deseja cadastrar?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-6">
					<?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
					<?= $form->field($model, 'sobrenome')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
					<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-6">
					<?= $form->field($model, 'telefone')->widget(MaskedInput::className(),['mask' => '(99) 99999-9999']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2 class="card-title">Parentescos</h2>
            <small class="card-subtitle">Quem são os parentes deste professor?</small>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Parentes:</label>
                        <select class="select2" multiple="" data-placeholder="Selecione..." tabindex="-1" aria-hidden="true" name="Parentesco[]">
							<?php
							foreach (Professor::find()->where(['not', ['id' => $model->id]])->all() as $professor) {
								$selecionado = false;
								foreach ($model->meusParentes as $parentesco) {
									if ($professor->id == $parentesco->professor_parente_id) {
										$selecionado = true;
										break;
									}
								}
								echo '<option value="'.$professor->id.'" '.($selecionado ? 'selected' : '' ).'>'.$professor->nome.' '.$professor->sobrenome.'</option>';
							}
							?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card" id="app">
        <div class="card-header">
            <h2 class="card-title">Mandatos</h2>
            <small class="card-subtitle">Quando este professor foi chefe do departamento?</small>
        </div>
        <div class="card-block__nova-linha animated fadeInUp" v-for="(mandato, i) in mandatos" :key="i">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Data de início do mandato</label>
                        <input v-model="mandato.data_inicio" v-mask="'##/##/####'" :name="'Mandato['+i+'][data_inicio]'" type="text" class="form-control" placeholder="ex: 23/05/2014" autocomplete="off" maxlength="10">
                        <i class="form-group__bar"></i>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label>Data de término do mandato</label>
                        <input v-model="mandato.data_fim" type="text" v-mask="'##/##/####'" :name="'Mandato['+i+'][data_fim]'" class="form-control" placeholder="ex: 23/05/2014" autocomplete="off" maxlength="10">
                        <i class="form-group__bar"></i>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="form-group" style="margin-top: 2rem;">
                        <button type="button" class="btn btn-outline-danger waves-effect" @click="removerMandato(i)">Remover mandato</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-block">
            <div class="row">
                <div class="col-lg-3">
                    <button type="button" class="btn btn-outline-primary waves-effect" @click="adicionarMandato">Adicionar mandato</button>
                </div>
            </div>
        </div>
    </div>

	<?php if ($model->isNewRecord) { ?>
        <div class="alert alert-warning" role="alert">
            <h4 class="alert-heading">Atenção:</h4>
            <p class="mb-0">A senha de acesso deste usuário será igual ao email de cadastro. Ele poderá alterar a senha assim que realizar o login no sistema.</p>
        </div>
	<?php } ?>

    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Cadastrar' : 'Salvar Modificações', ['class' => 'btn btn-primary']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>

<?php if (Yii::$app->controller->action->id === 'view') { ?>
<script>
    $(document).ready(function() {

        $('select').prop('disabled', true);

        $('button').remove();

        $('.form-control').prop('disabled', true);

        $('.form-control').css('opacity', 1);

        $('.form-control').css('border-bottom', 0);

        $('.form-control').css('cursor', 'default');

    });
</script>
<?php } ?>

<!--VUEJS-->
<!-- development version, includes helpful console warnings -->
<script src="<?= Url::base() ?>/js/vue.js"></script>
<!-- production version, optimized for size and speed -->
<!--<script src="https://cdn.jsdelivr.net/npm/vue"></script>-->
<!-- V-MASK -->
<script src="<?= Url::base() ?>/js/v-mask.min.js"></script>
<script>
    Vue.directive('mask', VueMask.VueMaskDirective);
    var app = new Vue({
        el: '#app',
        data: {
            mandatos: <?= Json::encode($model->mandatos) ?>
        },
        methods: {
            adicionarMandato: function () {
                let novo = {
                    data_inicio: '',
                    data_fim: ''
                }
                this.mandatos.push(novo)
            },
            removerMandato: function (i) {
                this.mandatos.splice(i, 1)
            }
        },
        created: function () {
            this.mandatos = this.mandatos.map((v) => {
                return Object.assign(v, {
                    data_inicio: v.data_inicio.split('-').reverse().join('/'),
                    data_fim: v.data_fim.split('-').reverse().join('/')
                })
            })
        }
    })
</script>
