<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />

    <style type="text/css">
        a.btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        a.btn:focus,
        a.btn:active:focus,
        a.btn.active:focus,
        a.btn.focus,
        a.btn:active.focus,
        a.btn.active.focus {
            outline: 5px auto -webkit-focus-ring-color;
            outline-offset: -2px;
        }
        a.btn:hover,
        a.btn:focus,
        a.btn.focus {
            color: #333;
            text-decoration: none;
        }
        a.btn:active,
        a.btn.active {
            background-image: none;
            outline: 0;
            -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
            box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
        }
        a.btn.disabled,
        a.btn[disabled],
        fieldset[disabled] .btn {
            cursor: not-allowed;
            filter: alpha(opacity=65);
            -webkit-box-shadow: none;
            box-shadow: none;
            opacity: .65;
        }
        a.btn.disabled,
        fieldset[disabled] a.btn {
            pointer-events: none;
        }

        a.btn-primary {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
        }
        a.btn-primary:focus,
        a.btn-primary.focus {
            color: #fff;
            background-color: #286090;
            border-color: #122b40;
        }
        a.btn-primary:hover {
            color: #fff;
            background-color: #286090;
            border-color: #204d74;
        }
        a.btn-primary:active,
        a.btn-primary.active,
        .open > .dropdown-toggle.btn-primary {
            color: #fff;
            background-color: #286090;
            border-color: #204d74;
        }
        a.btn-primary:active:hover,
        a.btn-primary.active:hover,
        .open > .dropdown-toggle.btn-primary:hover,
        a.btn-primary:active:focus,
        a.btn-primary.active:focus,
        .open > .dropdown-toggle.btn-primary:focus,
        a.btn-primary:active.focus,
        a.btn-primary.active.focus,
        .open > .dropdown-toggle.btn-primary.focus {
            color: #fff;
            background-color: #204d74;
            border-color: #122b40;
        }
        a.btn-primary:active,
        a.btn-primary.active,
        .open > .dropdown-toggle.btn-primary {
            background-image: none;
        }
        a.btn-primary.disabled:hover,
        a.btn-primary[disabled]:hover,
        fieldset[disabled] .btn-primary:hover,
        a.btn-primary.disabled:focus,
        a.btn-primary[disabled]:focus,
        fieldset[disabled] .btn-primary:focus,
        a.btn-primary.disabled.focus,
        a.btn-primary[disabled].focus,
        fieldset[disabled] .btn-primary.focus {
            background-color: #337ab7;
            border-color: #2e6da4;
        }
        a.btn-primary .badge {
            color: #337ab7;
            background-color: #fff;
        }

        a.btn-warning {
            color: #fff;
            background-color: #f0ad4e;
            border-color: #eea236;
        }
        a.btn-warning:focus,
        a.btn-warning.focus {
            color: #fff;
            background-color: #ec971f;
            border-color: #985f0d;
        }
        a.btn-warning:hover {
            color: #fff;
            background-color: #ec971f;
            border-color: #d58512;
        }
        a.btn-warning:active,
        a.btn-warning.active,
        .open > .dropdown-toggle.btn-warning {
            color: #fff;
            background-color: #ec971f;
            border-color: #d58512;
        }
        a.btn-warning:active:hover,
        a.btn-warning.active:hover,
        .open > .dropdown-toggle.btn-warning:hover,
        a.btn-warning:active:focus,
        a.btn-warning.active:focus,
        .open > .dropdown-toggle.btn-warning:focus,
        a.btn-warning:active.focus,
        a.btn-warning.active.focus,
        .open > .dropdown-toggle.btn-warning.focus {
            color: #fff;
            background-color: #d58512;
            border-color: #985f0d;
        }
        a.btn-warning:active,
        a.btn-warning.active,
        .open > .dropdown-toggle.btn-warning {
            background-image: none;
        }
        a.btn-warning.disabled:hover,
        a.btn-warning[disabled]:hover,
        fieldset[disabled] .btn-warning:hover,
        a.btn-warning.disabled:focus,
        a.btn-warning[disabled]:focus,
        fieldset[disabled] .btn-warning:focus,
        a.btn-warning.disabled.focus,
        a.btn-warning[disabled].focus,
        fieldset[disabled] .btn-warning.focus {
            background-color: #f0ad4e;
            border-color: #eea236;
        }
    </style>

    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="background-color: #f7f7f7; padding: 0px; margin: 0;">
<?php $this->beginBody() ?>

<div class="div-central" style="border: 1px solid #d9d9d9; background-color: white; margin: 0px auto; max-width: 600px; overflow: auto;">
    <table cellpadding="0" cellspacing="0" style="margin: 0px; padding: 0px; width: 100%;">
        <tr>
            <td style="text-align: center; padding: 30px;">
                <?= yii\helpers\Html::img($message->embed(Yii::getAlias('@app/web/images/logo/logo-colorida.png')), ['style' => 'width:200px;']) ?>
            </td>
        </tr>
        <?= $content ?>

        <!-- AGRADECIMENTO -->
        <tr>
            <td style="padding: 0 30px 30px 30px; text-align: center;">
                <p>Atenciosamente,</p>
                <?= yii\helpers\Html::img($message->embed(Yii::getAlias('@app/web/images/logo/logo-preta.png')), ['style' => 'width:100px;']) ?>
            </td>
        </tr>

    </table>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
