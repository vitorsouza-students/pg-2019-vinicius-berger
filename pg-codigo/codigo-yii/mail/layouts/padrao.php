<?php

use yii\helpers\Json;

/**
 * @var $titulo
 * @var $mensagem
 * @var $detalhes
 */

?>

<!-- TITULO -->
<tr>
    <td style="text-align: center; background-color:  #25B6D1; color: white; font-size: 20px; padding: 20px;">
        <span><?= $titulo ?></span>
    </td>
</tr>

<!-- MENSAGEM -->
<tr>
    <td style="padding: 30px;">
        <p>
            <?= $mensagem ?>
        </p>
    </td>
</tr>

<!-- DETALHES -->
<?php if (!empty($detalhes)) { ?>
<tr>
    <td style="background-color: #f5f5f5; padding: 30px;">
        <p>
            <?= Json::encode($detalhes) ?>
        </p>
    </td>
</tr>
<?php } ?>
