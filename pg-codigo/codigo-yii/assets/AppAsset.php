<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		[
			'href' => 'images/logo/logo.svg',
			'rel' => 'icon',
			'sizes' => '32x32',
		],
        '../vendor/material-admin/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css',
        '../vendor/material-admin/vendors/bower_components/animate.css/animate.min.css',
        '../vendor/material-admin/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css',
        '../vendor/material-admin/vendors/bower_components/select2/dist/css/select2.min.css',
        '../vendor/material-admin/vendors/bower_components/dropzone/dist/dropzone.css',
        '../vendor/material-admin/vendors/bower_components/flatpickr/dist/flatpickr.min.css',
        '../vendor/material-admin/vendors/bower_components/nouislider/distribute/nouislider.min.css',
        '../vendor/material-admin/vendors/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css',
        '../vendor/material-admin/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
        '../vendor/material-admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css',
        '../vendor/croppie/croppie.css',
        '../vendor/material-admin/css/app.min.css',
        '../vendor/material-admin/demo/css/demo.css',
        '../vendor/fontawesome-free-5.10.1-web/css/all.min.css',
		'css/site.css',
    ];
   public $js = [
       // vendors
       '../vendor/material-admin/vendors/bower_components/tether/dist/js/tether.min.js',
       '../vendor/material-admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js',
       '../vendor/material-admin/vendors/bower_components/Waves/dist/waves.min.js',
       '../vendor/material-admin/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js',
       '../vendor/material-admin/vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js',
       '../vendor/material-admin/vendors/bower_components/Waves/dist/waves.min.js',
       '../vendor/material-admin/vendors/bower_components/flot/jquery.flot.js',
       '../vendor/material-admin/vendors/bower_components/flot/jquery.flot.pie.js',
       '../vendor/material-admin/vendors/bower_components/flot/jquery.flot.resize.js',
       '../vendor/material-admin/vendors/bower_components/flot.curvedlines/curvedLines.js',
       '../vendor/material-admin/vendors/bower_components/flot.orderbars/js/jquery.flot.orderBars.js',
       '../vendor/material-admin/vendors/bower_components/jqvmap/dist/jquery.vmap.min.js',
       '../vendor/material-admin/vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js',
       '../vendor/material-admin/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
       '../vendor/material-admin/vendors/bower_components/salvattore/dist/salvattore.min.js',
       '../vendor/material-admin/vendors/jquery.sparkline/jquery.sparkline.min.js',
       '../vendor/material-admin/vendors/bower_components/moment/min/moment.min.js',
       '../vendor/material-admin/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js',
	   '../vendor/material-admin/vendors/bower_components/fullcalendar/dist/locale/pt-br.js',
       '../vendor/croppie/croppie.js',
       '../vendor/maskmoney/jquery.maskMoney.js',
       // Notifications
       '../vendor/material-admin/vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js',
       '../vendor/material-admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js',
       // Forms components
       '../vendor/material-admin/vendors/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js',
       '../vendor/material-admin/vendors/bower_components/select2/dist/js/select2.full.min.js',
       '../vendor/material-admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js',
       '../vendor/material-admin/vendors/bower_components/moment/min/moment.min.js',
       '../vendor/material-admin/vendors/bower_components/flatpickr/dist/flatpickr.min.js',
       '../vendor/material-admin/vendors/bower_components/nouislider/distribute/nouislider.min.js',
       '../vendor/material-admin/vendors/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
       // Charts and maps
       //'../vendor/material-admin/demo/js/flot-charts/curved-line.js',
       //'../vendor/material-admin/demo/js/flot-charts/line.js',
       '../vendor/material-admin/demo/js/flot-charts/chart-tooltips.js',
       '../vendor/material-admin/demo/js/other-charts.js',
       //'../vendor/material-admin/demo/js/flot-charts/bar.js',
       '../vendor/material-admin/demo/js/jqvmap.js',
	   '../vendor/flot-tooltip-master/jquery.flot.tooltip.min.js',
       // Data tables
       '../vendor/material-admin/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js',
       '../vendor/material-admin/vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js',
       '../vendor/material-admin/vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js',
       '../vendor/material-admin/vendors/bower_components/jszip/dist/jszip.min.js',
       '../vendor/material-admin/vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js',
       // App functions and actions
       //'../vendor/material-admin/js/app.min.js',
	   '../vendor/material-admin/js/inc/functions/app.js',
	   '../vendor/material-admin/js/inc/functions/vendors.js',
	   '../vendor/material-admin/js/inc/actions.js',
       //Component to search for cep
       '../vendor/cep/cep.js',
	   // Funções úteis
	   'js/copiarParaAreaDeTransferencia.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
