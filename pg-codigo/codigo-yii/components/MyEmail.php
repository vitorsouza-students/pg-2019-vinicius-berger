<?php
namespace app\components;
use Yii;

/**
 * Implementação de um componente que envia emails em um formato padronizado
 */
class MyEmail extends \yii\base\Component
{
	/**
	 * @param $titulo
	 * @param $mensagem
	 * @param null $detalhes
	 * @param null $destinatarios
	 */
	public function enviarEmailPadrao($titulo, $mensagem, $detalhes=null, $destinatarios=null)
	{
		$destinatarios = $destinatarios === null ?
			Yii::$app->params['notificacao_erros'] :
			$destinatarios;

		\Yii::$app->mailer->compose('layouts/padrao', [
			'titulo' => $titulo,
			'mensagem' => $mensagem,
			'detalhes' => $detalhes
		])
			->setFrom([Yii::$app->params['nao-responda'] => "SCAP"])
			->setTo($destinatarios)
			->setSubject('SCAP - '.$titulo)
			->send();
	}
}
