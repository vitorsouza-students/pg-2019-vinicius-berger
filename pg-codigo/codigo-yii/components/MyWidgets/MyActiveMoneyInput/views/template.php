<?php
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $model */
/** @var string $attribute */
/** @var string $options */
/** @var string $autoIdPrefix */

$id_hidden_input = Html::getInputId($model, $attribute);
$id_input = $id_hidden_input.'-visivel';
?>

<?= Html::activeHiddenInput($model, $attribute); ?>
<?= Html::textInput('', '', array_merge($options, ['id' => $id_input])) ?>
<label class="form-control-label">
    <?= $model->attributeLabels()[$attribute] ?>
</label>
<i class="form-group__bar"></i>
<div class="help-block"></div>

<script>
    $(document).ready(function() {

        // Máscara de valor
        $('#<?= $id_input ?>').maskMoney({
            prefix: 'R$ ',
            allowNegative: false,
            thousands: '.',
            decimal: ',',
            affixesStay: true,
            allowZero: true,
            allowEmpty: true
        });

        // Inicializa
        let valor = $('#<?= $id_hidden_input ?>').val().split('.').join(',');
        $('#<?= $id_input ?>').val('R$ ' + valor);

        // Manipulador de alterações
        $('#<?= $id_input ?>').change(function () {
            let valor = ($('#<?= $id_input ?>').val().split(' '))[1].split('.').join('').split(',').join('.');
            console.log(valor);
            $('#<?= $id_hidden_input ?>').val(valor);
            $('#<?= $id_hidden_input ?>').closest('form').yiiActiveForm('validateAttribute', '<?= $id_hidden_input ?>');
        });
    });
</script>
