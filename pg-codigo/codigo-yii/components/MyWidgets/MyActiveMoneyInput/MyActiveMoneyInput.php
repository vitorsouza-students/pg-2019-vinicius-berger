<?php
namespace app\components\MyWidgets\MyActiveMoneyInput;

use yii\widgets\InputWidget;
use yii\helpers\Html;

class MyActiveMoneyInput extends InputWidget
{
	public $options = [];

	public function run()
	{
//		if ($this->hasModel()) {
//			$hidden_input = Html::activeHiddenInput($this->model, $this->attribute);
//			$input = Html::textInput('', '', $this->options);
//			$label = $this->model->attributeLabels()[$this->attribute];
//		}

		return $this->render('template', [
//			'hidden_input' => $hidden_input,
//			'input' => $input,
//			'label' => $this->model->attributeLabels()[$this->attribute],
			'attribute' => $this->attribute,
			'model' => $this->model,
			'options' => $this->options,
			'autoIdPrefix' => $this->getId()
		]);
	}
}
