<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model \yii\base\Model */
/* @var $form yii\widgets\ActiveForm */

// Se for para iniciar já aberto
$exibir_form_search = '';
foreach ($inputs as $input) {
    if (!empty($model[$input['atributo']])) {
        $exibir_form_search = 'show';
    }
}

// Foca o input após a exibição dos filtros
$this->registerJs("
    $('#filtros').on('shown.bs.collapse', function () {
        $('#formsearch-".$inputs[0]['atributo']."').focus()
    })",
	\yii\web\View::POS_LOAD
);
?>

<div class="collapse <?= $exibir_form_search ?>" id="filtros" aria-expanded="false" style="padding-bottom: 28px;">

    <?php $form = ActiveForm::begin([
        'method' => 'get',
		'options' => ['autocomplete' => 'off'],
        'errorCssClass' => 'has-danger',
        'id' => 'form-search',
        'fieldConfig' => [
            'options' => ['class' => 'form-group form-group--float'],
            'labelOptions' => ['class' => 'form-control-label'],
            'inputOptions' => ['class' => 'form-control'],
            'errorOptions' => ['class' => 'help-block'],
            'template' => '{input}{label}<i class="form-group__bar"></i>{error}'
        ],
    ]); ?>

    <div class="row">

        <?php foreach ($inputs as $input) { ?>

            <div class="<?= $input['class'] ?>">

                <?php if ($input['tipo'] === 'dropdown') { ?>
					<?= $form->field($model, $input['atributo'])->dropDownList($input['opcoes'], ['prompt' => '', 'id' => 'formsearch-'.$input['atributo']]) ?>
                <?php } ?>

				<?php if ($input['tipo'] === 'textinput') { ?>
					<?= $form->field($model, $input['atributo'])->textInput(['id' => 'formsearch-'.$input['atributo']]) ?>
				<?php } ?>

				<?php if ($input['tipo'] === 'data') { ?>
					<?= $form->field($model, $input['atributo'])->widget(MaskedInput::className(),['mask' => '99/99/9999', 'options' => ['id' => 'formsearch-'.$input['atributo'], 'class' => 'form-control']]) ?>
				<?php } ?>

				<?php if ($input['tipo'] === 'float') { ?>
					<?= $form->field($model, $input['atributo'])->textInput(['id' => 'formsearch-'.$input['atributo'], 'class' => 'valor form-control']) ?>
				<?php } ?>

            </div>

        <?php } ?>

        <div class="col-lg-12" style="text-align: right">
            <div class="btn-demo">
				<?= Html::submitButton('Filtrar', ['class' => 'btn btn-outline-primary text-primary btn--icon-text']) ?>
				<?= Html::button('Limpar filtros', ['class' => 'btn btn-link text-primary btn--icon-text', 'style' => 'padding-right: 0;', 'onclick' => 'limparCampos(this)']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>

<script>
    $(document).ready(function(){
        $(".valor").maskMoney({prefix:'', allowNegative: false, thousands:'.', decimal:'.', affixesStay: false, allowZero: true, allowEmpty: true});
    });

    function limparCampos(botao){
        <?php foreach ($inputs as $input) {?>
            $('#formsearch-<?= $input['atributo'] ?>').val('').removeClass('form-control--active');
		<?php } ?>
        $('#form-search').submit();
    }
</script>
