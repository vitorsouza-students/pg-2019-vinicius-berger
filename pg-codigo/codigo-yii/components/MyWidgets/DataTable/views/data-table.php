<?php
use yii\helpers\Html;
use yii\grid\GridView;

// Adiciona a coluna de opções
array_push($columns, [
	'header' => 'Opções',
	'class' => 'yii\grid\ActionColumn',
	'template' => $templateColunaOpcoes,
	'headerOptions' => ['style' => 'text-align: center;'],
	'contentOptions'=>['style'=>'width: 134px; text-align: center; vertical-align: middle !important;', 'class' => 'theme-options'],
	'buttons' => $buttonsColunaOpcoes
]);

$renderizar_form_search = !empty($inputs);
?>

<div class="card card-data-table">
    <div class="card-header" style="padding-bottom: 0;">
        <h2 class="card-title"><?= $titulo ?></h2>
        <small class="card-subtitle"><?= $subtitulo ?></small>
		<?php if ($renderizar_form_search) {
		        echo $this->render('_search', ['model' => $searchModel, 'inputs' => $inputs]);
		} ?>
        <div style="padding-right: 50px;">
			<?= Html::a($labelBotaoCriar,       [$urlBotaoCriar],       ['class' => 'btn btn-primary', 'style' => $exibirBotaoCriar ? 'margin-right: 10px; margin-bottom: 25px;' : 'margin-right: 10px; margin-bottom: 25px; display: none;']) ?>
			<?= Html::a($labelBotaoExportarPDF, [$urlBotaoExportarPDF], ['class' => 'btn btn-outline-primary text-primary', 'style' => $exibirBotaoExportarPDF ? 'margin-right: 10px; margin-bottom: 25px;' : 'margin-right: 10px; margin-bottom: 25px; display: none;']) ?>
			<?= Html::a($labelBotaoExportarCSV, [$urlBotaoExportarCSV], ['class' => 'btn btn-outline-primary text-primary', 'style' => $exibirBotaoExportarCSV ? 'margin-right: 10px; margin-bottom: 25px;' : 'margin-right: 10px; margin-bottom: 25px; display: none;']) ?>

            <!-- Botão que existe só para reservar o espaço da altura de um botão, caso nenhum botão acima seja exibido -->
			<?= !$exibirBotaoCriar && !$exibirBotaoExportarPDF && !$exibirBotaoExportarCSV && !$titulo ? Html::a('Espaço Reservado', 'javascript:void(0)', ['class' => 'btn btn-outline-primary text-primary', 'style' => 'visibility: hidden; margin-bottom: 25px;']) : '' ?>
        </div>
        <?php if ($renderizar_form_search) { ?>
            <div class="actions">
                <a class="actions__item collapsed" data-toggle="collapse" href="#filtros" aria-expanded="false" aria-controls="filtros"><i class="font-sendpayfont" style="font-size: 15px;">s</i></a>
            </div>
        <?php } ?>
    </div>

    <div class="table-responsive">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'layout' => '{items}<span style="text-align:center;font-size:.9rem;color: #9c9c9c;">{summary}{pager}</span>',
			'showHeader' => $dataProvider->getTotalCount(),
            'headerRowOptions' => ['class'=>'thead-default theme-options' ],
			'tableOptions' => ['class' => 'table table-hover'],
			'summaryOptions' => ['class' => 'summary dataTables_info'],
			'pager' => ['class' => \app\components\MyWidgets\LinkPager\LinkPagerEstilizado::className()],
			'columns' => $columns
		]); ?>
    </div>
</div>
