<?php
namespace app\components\MyWidgets\DataTable;

use yii\base\Widget;
use yii\helpers\Html;

class DataTable extends Widget
{
	public $labelBotaoCriar;
	public $urlBotaoCriar;
	public $exibirBotaoCriar;
	public $labelBotaoExportarPDF;
	public $urlBotaoExportarPDF;
	public $exibirBotaoExportarPDF;
	public $labelBotaoExportarCSV;
	public $urlBotaoExportarCSV;
	public $exibirBotaoExportarCSV;
	public $dataProvider;
	public $searchModel;
	public $columns;
	public $inputs;
	public $templateColunaOpcoes;
	public $buttonsColunaOpcoes;
	public $titulo;
	public $subtitulo;

	public function init()
	{
		parent::init();

		$this->urlBotaoCriar =
			empty($this->urlBotaoCriar) ?
				'create' :
				$this->urlBotaoCriar;

		$this->exibirBotaoCriar =
			!isset($this->exibirBotaoCriar) ?
				true :
				$this->exibirBotaoCriar;

		$this->exibirBotaoExportarPDF =
			!isset($this->exibirBotaoExportarPDF) ?
				false :
				$this->exibirBotaoExportarPDF;

		$this->exibirBotaoExportarCSV =
			!isset($this->exibirBotaoExportarCSV) ?
				false :
				$this->exibirBotaoExportarCSV;

		$this->buttonsColunaOpcoes = empty($this->buttonsColunaOpcoes) ?
			[
				'view' => function($url, $model, $key) {
					return Html::a('<i class="font-sendpayfont text-primary">y</i>', $url);
				},
				'update' => function($url, $model, $key) {
					return Html::a('<i class="font-sendpayfont text-primary">e</i>', $url);
				},
				'delete' => function($url, $model, $key) {
					return Html::a('<i class="font-sendpayfont text-primary">r</i>', $url, [
						'data-method' => 'post',
						'data-confirm' => 'Deseja realmente excluir o item?'
					]);
				}
			] : $this->buttonsColunaOpcoes;

		if (!isset($this->templateColunaOpcoes)) {
			foreach ($this->buttonsColunaOpcoes as $key => $button) {
				$this->templateColunaOpcoes .= '{'.$key.'} ';
			}
		}
	}

	public function run()
	{
		return $this->render('data-table', [
			'labelBotaoCriar' => $this->labelBotaoCriar,
			'urlBotaoCriar' => $this->urlBotaoCriar,
			'exibirBotaoCriar' => $this->exibirBotaoCriar,
			'labelBotaoExportarPDF' => $this->labelBotaoExportarPDF,
			'urlBotaoExportarPDF' => $this->urlBotaoExportarPDF,
			'exibirBotaoExportarPDF' => $this->exibirBotaoExportarPDF,
			'labelBotaoExportarCSV' => $this->labelBotaoExportarCSV,
			'urlBotaoExportarCSV' => $this->urlBotaoExportarCSV,
			'exibirBotaoExportarCSV' => $this->exibirBotaoExportarCSV,
			'dataProvider' => $this->dataProvider,
			'searchModel' => $this->searchModel,
			'columns' => $this->columns,
			'inputs' => $this->inputs,
			'templateColunaOpcoes' => $this->templateColunaOpcoes,
			'buttonsColunaOpcoes' => $this->buttonsColunaOpcoes,
			'titulo' => $this->titulo,
			'subtitulo' => $this->subtitulo
		]);
	}
}
