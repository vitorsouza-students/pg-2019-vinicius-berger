<?php

namespace app\components;

use yii\base\Component;

/**
 * Class MyFTPAccess
 * @package app\components
 */
class MyFTPAccess extends Component{

    /**
     * @param $arquivo
     * @param $nome_arquivo
     * @return null
     */
    function enviarArquivoLocal($arquivo, $nome_arquivo)
    {
        if(empty($arquivo) || empty($nome_arquivo)){
            return null;
        }

        $arquivo->saveAs('../../uploads/' . $nome_arquivo);

        return $nome_arquivo;
    }
}
