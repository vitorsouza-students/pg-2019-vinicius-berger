<?php

namespace app\components;

use yii\helpers\Json;

class MyFormatter extends \yii\i18n\Formatter
{
    public $datetimeFormat='d/m/Y H:i:s';

    public $dateFormat='d/m/Y';

    public $booleanFormat=array('Não','Sim');

    public $numberFormat = array('decimals'=>2, 'decimalSeparator'=>',', 'thousandSeparator'=>'.');

    public $timeZone = 'America/Sao_Paulo';

        /**
     * Formats the value as a number using PHP number_format() function.
     * new: if the given $value is null/empty, return null/empty string
     * @param mixed $value the value to be formatted
     * @return string the formatted result
     * @see numberFormat
     */
    public function formatNumber($value) {
        if($value === null) {return null;}    // new
        if($value === '') {return '';}        // new
        return number_format($value, $this->numberFormat['decimals'], $this->numberFormat['decimalSeparator'], $this->numberFormat['thousandSeparator']);
    }

    /*
     * new function unformatNumber():
     * turns the given formatted number (string) into a float
     * @param string $formatted_number A formatted number
     * (usually formatted with the formatNumber() function)
     * @return float the 'unformatted' number
     */
    public function unformatNumber($formatted_number) {
        if($formatted_number === null) {return null;}
        if($formatted_number === '') {return '';}
        if(is_float($formatted_number)) {return $formatted_number;} // only 'unformat' if parameter is not float already

        $value = str_replace($this->numberFormat['thousandSeparator'], '', $formatted_number);
        $value = str_replace($this->numberFormat['decimalSeparator'], '.', $value);
        return (float) $value;
    }

    /**
     * @param $value
     * @return string|null
     * @throws \yii\base\InvalidConfigException
     */
    public function formatDataPorExtenso($value) {
        if($value === null) {return null;}    // new
        if($value === '') {return '';}        // new
        return \Yii::$app->formatter->asDate($value, 'php:jS').' de '.\Yii::$app->formatter->asDate($value, 'php:F').' de '.\Yii::$app->formatter->asDate($value, 'php:Y');
    }

    /**
     * @param $mensagem
     * @param $array_erros
     * @return string
     */
    public function formatarNotify($mensagem, $campos_com_erros) {
        $conteudo = $mensagem.'<br><ul style="margin-top: 10px;">';

        foreach ($campos_com_erros as $erros_campo) {
            foreach ($erros_campo as $erro) {
                $conteudo .= '<li>'.$erro.'</li>';
            }
        }

        $conteudo .= '</ul>';

        return $conteudo;
    }
}

?>

