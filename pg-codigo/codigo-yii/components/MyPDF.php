<?php
namespace app\components;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\Component;

/**
 * Implementação de um componente que gera PDFs num formato padronizado
 */
class MyPDF extends Component
{
	/**
	 * @param $nomeArquivo
	 * @param $destino
	 * @param $view
	 * @param $parametrosView
	 * @return mixed
	 * @throws \Exception
	 */
	public function gerarPdf($nomeArquivo, $destino, $view, $parametrosView)
	{
		date_default_timezone_set('America/Sao_Paulo');
		$dataHoraAtual = date('Y-m-d H:i:s');
		$data = date('d/m/Y', strtotime($dataHoraAtual));
		$hora = date('H:i:s', strtotime($dataHoraAtual));

		$pdf = new Pdf([
			'mode' => Pdf::MODE_UTF8,
			'format' => Pdf::FORMAT_A4,
			'defaultFontSize' => 5,
			'marginTop' => 20,
			'marginRight' => 10,
			'marginBottom' => 20,
			'marginLeft' => 10,
			'content' => Yii::$app->controller->renderPartial($view, $parametrosView),
			'options' => [
				'title' => 'Verbochurch',
				'subject' => '',
				'defaultfooterline' => false
			],
			'methods' => [
				'SetHeader' => [$_SERVER['HTTP_HOST']. '||<img src="../web/images/logo/logo-colorida.svg" width="150">'],
				'SetFooter' => ['Emitido em '.$data.' às '.$hora.'||Página {PAGENO}'],
			],
			'filename' => $nomeArquivo,
			'destination' => $destino
		]);

		return $pdf->render();
	}
}
