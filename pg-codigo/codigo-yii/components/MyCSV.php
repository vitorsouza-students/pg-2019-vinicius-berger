<?php
namespace app\components;
use Yii;

/**
 * Implementação de um componente que gera CSVs num formato padronizado
 */
class MyCSV extends \yii\base\Component
{
	/**
	 * @param $nomeArquivo
	 * @param $searchModel
	 * @param $dataProvider
	 * @param $filtrosOcultos [$attibute1, $attribute2]
	 * @param $colunas array [$attribute => function($model)]
	 * @throws \yii\web\HttpException
	 */
	public function gerarCsv($nomeArquivo, $searchModel, $dataProvider, $filtrosOcultos, $colunas)
	{
		// Cria um descritor de arquivo temporário
		$handle = fopen('php://temp/maxmemory:20971520', 'w+');

		// Preenche os filtros utilizados
		foreach ($searchModel->attributes as $key => $attribute) {

			if (!in_array($key, $filtrosOcultos) && !empty($attribute))
				fputcsv($handle, [$searchModel->attributeLabels()[$key].' -->', $attribute], ';');
		}
		fputcsv($handle, [], ';');

		// Preenche o cabecalho
		$cabecalho = [];
		foreach ($colunas as $coluna => $formatador) {
			array_push($cabecalho, (isset($searchModel->attributeLabels()[$coluna]) ? $searchModel->attributeLabels()[$coluna] : $coluna));
		}
		fputcsv($handle, $cabecalho, ';');

		// Preenche as linhas
		foreach ($dataProvider->models as $model) {

			$linha = [];
			foreach ($colunas as $coluna => $formatador) {
				array_push($linha, $formatador($model));
			}
			fputcsv($handle, $linha, ';');
		}

		// Envia o arquivo csv gerado
		\Yii::$app->response->sendStreamAsFile(
			$handle,
			$nomeArquivo,
			[
				'mimeType' => 'application/csv',
				'inline'   => false
			]
		);
	}
}
