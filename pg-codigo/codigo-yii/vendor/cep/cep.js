function limpa_formulario_cep(elements) {
    // Limpa valores do formulário de cep.
    for (const [prop, value] of Object.entries(elements)) {
        if(prop != "cep"){
            if(typeof value === 'object'){
                value.clean();
            }else {
                $("#" + value).val("");
                $("#" + value).removeClass("form-control--active");
            }
        }
    }
}

function busca_cep(elements){
    //Quando o campo cep perde o foco ou quando clica no botão.
    $("#"+ elements.cep).blur(cep);

    function cep() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {
            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {
                //Preenche os campos com "..." enquanto consulta webservice.
                limpa_formulario_cep(elements)

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?&param=" + new Date().getTime(), function(dados) {
                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta
                        for (const [prop, value] of Object.entries(elements)) {
                            if(prop != "cep"){
                                if(typeof value === 'object'){
                                    value.success(dados[prop]);
                                }else{
                                    $("#"+ value).val(dados[prop]);
                                    // $("#" + value).addClass("form-control--active");
                                    $("#"+ value).trigger('focus');
                                    $("#"+ value).trigger('onblur');
                                }

                            }
                        }
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulario_cep(elements);
                        for (const [prop, value] of Object.entries(elements)) {
                            if(prop != "cep" && typeof value === 'object' &&  typeof value.failure !== 'undefined'){
                                value.failure(dados);
                            }
                        }
                        //alert("CEP não encontrado.");
                    }
                    $.ajaxSetup({ cache: false });
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulario_cep(elements);
                //alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulario_cep(elements);
        }
    };

}
