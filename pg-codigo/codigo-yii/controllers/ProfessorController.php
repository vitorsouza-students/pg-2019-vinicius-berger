<?php

namespace app\controllers;

use app\models\Mandato;
use app\models\Parentesco;
use app\models\Usuario;
use Exception;
use Yii;
use app\models\Professor;
use app\models\ProfessorSearch;
use yii\base\Security;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProfessorController implements the CRUD actions for Professor model.
 */
class ProfessorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Professor models.
     * @return mixed
     */
    public function actionIndex()
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        $searchModel = new ProfessorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Professor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	/**
	 * Creates a new Professor model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 * @throws Exception
	 */
    public function actionCreate()
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        $model = new Professor();
        $model_usuario = new Usuario();
        $post = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();

		try {
			if ($model->load($post)) {
				$model_usuario->login = $model->email;
				$model_usuario->senha = sha1($model->email);
				if (!$model_usuario->save()) {
					throw new Exception(Json::encode($model_usuario->getErrors()));
				}
				$model->usuario_id = $model_usuario->id;
				if (!$model->save()) {
					throw new Exception(Json::encode($model->getErrors()));
				}
				if (!empty($post['Parentesco'])) {
					foreach ($post['Parentesco'] as $professor_parente_id) {
						$model_parentesco = new Parentesco();
						$model_parentesco->professor_parente_id = $professor_parente_id;
						$model_parentesco->professor_id = $model->id;
						$model_parentesco->parentesco = Parentesco::PARENTESCO_SANGUINEO;
						if (!$model_parentesco->save()) {
							throw new Exception(Json::encode($model_parentesco->getErrors()));
						}
						$model_parentesco_relacionado = new Parentesco();
						$model_parentesco_relacionado->professor_parente_id = $model->id;
						$model_parentesco_relacionado->professor_id = $professor_parente_id;
						$model_parentesco_relacionado->parentesco = Parentesco::PARENTESCO_SANGUINEO;
						if (!$model_parentesco_relacionado->save()) {
							throw new Exception(Json::encode($model_parentesco_relacionado->getErrors()));
						}
					}
				}
				if (!empty($post['Mandato'])) {
					foreach ($post['Mandato'] as $mandato) {
						$model_mandato = new Mandato();
						$model_mandato->data_inicio = !empty($mandato['data_inicio']) ? implode('-', array_reverse( explode('/', $mandato['data_inicio']))) : null;
						$model_mandato->data_fim = !empty($mandato['data_fim']) ? implode('-', array_reverse( explode('/', $mandato['data_fim']))) : null;
						$model_mandato->professor_id = $model->id;
						if (!$model_mandato->save()) {
							throw new Exception(Json::encode($model_mandato->getErrors()));
						}
					}
				}
				$transaction->commit();
				// Manda email para o professor
				Yii::$app->email->enviarEmailPadrao(
					'Seja bem-vindo',
					'Você foi cadastrado na plataforma SCAP - Sistema de Controle de Afastamento de Professores 
					como "Professor". Seus dados de acesso estão listados a seguir (sugerimos que troque sua 
					senha assim que entrar no sistema).',
					'<b>Login:</b> '.$model->email.'<br><b>Senha provisória:</b> '.$model->email,
					$model->email
				);
                Yii::$app->getSession()->setFlash('mensagens', [
                    'type' => 'success',
                    'message' => 'O professor foi criado com sucesso',
                ]);
				return $this->redirect(['index']);
			} else {
				return $this->render('create', [
					'model' => $model,
				]);
			}
		} catch (Exception $e) {
			$transaction->rollBack();
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => $e->getMessage(),
			]);
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Professor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        $model = $this->findModel($id);
		$post = Yii::$app->request->post();
		$transaction = Yii::$app->db->beginTransaction();

		try {
			if ($model->load($post)) {
				if (!$model->save()) {
					throw new Exception(Json::encode($model->getErrors()));
				}
				Parentesco::deleteAll(['professor_id' => $model->id]);
				Parentesco::deleteAll(['professor_parente_id' => $model->id]);
				if (!empty($post['Parentesco'])) {
					foreach ($post['Parentesco'] as $professor_parente_id) {
						$model_parentesco = new Parentesco();
						$model_parentesco->professor_parente_id = $professor_parente_id;
						$model_parentesco->professor_id = $model->id;
						$model_parentesco->parentesco = Parentesco::PARENTESCO_SANGUINEO;
						if (!$model_parentesco->save()) {
							throw new Exception(Json::encode($model_parentesco->getErrors()));
						}
						$model_parentesco_relacionado = new Parentesco();
						$model_parentesco_relacionado->professor_parente_id = $model->id;
						$model_parentesco_relacionado->professor_id = $professor_parente_id;
						$model_parentesco_relacionado->parentesco = Parentesco::PARENTESCO_SANGUINEO;
						if (!$model_parentesco_relacionado->save()) {
							throw new Exception(Json::encode($model_parentesco_relacionado->getErrors()));
						}
					}
				}
				Mandato::deleteAll(['professor_id' => $model->id]);
				if (!empty($post['Mandato'])) {
					foreach ($post['Mandato'] as $mandato) {
						$model_mandato = new Mandato();
						$model_mandato->data_inicio = !empty($mandato['data_inicio']) ? implode('-', array_reverse( explode('/', $mandato['data_inicio']))) : null;
						$model_mandato->data_fim = !empty($mandato['data_fim']) ? implode('-', array_reverse( explode('/', $mandato['data_fim']))) : null;
						$model_mandato->professor_id = $model->id;
						if (!$model_mandato->save()) {
							throw new Exception(Json::encode($model_mandato->getErrors()));
						}
					}
				}
                Yii::$app->getSession()->setFlash('mensagens', [
                    'type' => 'success',
                    'message' => 'O professor foi atualizado com sucesso',
                ]);
				$transaction->commit();
				return $this->redirect(['index']);
			} else {
				return $this->render('update', [
					'model' => $model,
				]);
			}
		} catch (Exception $e) {
			$transaction->rollBack();
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => $e->getMessage(),
			]);
			return $this->render('update', [
				'model' => $model,
			]);
		}
    }

    /**
     * Deletes an existing Professor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

    	$model = $this->findModel($id);

    	Parentesco::deleteAll(['professor_id' => $model->id]);
		Parentesco::deleteAll(['professor_parente_id' => $model->id]);
		Mandato::deleteAll(['professor_id' => $model->id]);

        if ($model->delete()) {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'success',
                'message' => 'O professor foi removido com sucesso',
            ]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Professor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Professor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Professor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
