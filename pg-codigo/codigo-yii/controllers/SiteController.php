<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ContaDigital;
use app\models\CriarContaForm;
use yii\web\Response;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

	/**
	 * @return array|string[]
	 */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction'
            ],
        ];
    }

	/**
	 * @return Response
	 */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('afastamento/index');
        }

        return $this->redirect('site/login');
    }

	/**
	 * @return string|Response
	 */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

		return $this->renderPartial('login', [
			'model' => $model,
		]);
    }

	/**
	 * @return Response
	 */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('login');
    }
}
