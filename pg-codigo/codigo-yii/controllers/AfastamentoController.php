<?php

namespace app\controllers;

use app\models\Documento;
use app\models\Mandato;
use app\models\Parecer;
use app\models\Professor;
use Exception;
use Yii;
use app\models\Afastamento;
use app\models\AfastamentoSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * AfastamentoController implements the CRUD actions for Afastamento model.
 */
class AfastamentoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Afastamento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AfastamentoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Afastamento model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Afastamento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um professor
        if (empty(Yii::$app->user->identity->professor)) {
            throw new Exception('Ação não permitida');
        }

        $model = new Afastamento();
        $transaction = Yii::$app->db->beginTransaction();
        $post = Yii::$app->request->post();

        try {
			if ($model->load($post)) {
				$model->professor_solicitante_id = Yii::$app->user->identity->professor->id;
				$model->data_solicitacao = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y-m-d');
				$model->situacao = Afastamento::SITUACAO_INICIADO;
				$model->data_inicio_afastamento = !empty($model->data_inicio_afastamento) ? implode('-', array_reverse( explode('/', $model->data_inicio_afastamento))) : null;
				$model->data_fim_afastamento = !empty($model->data_fim_afastamento) ? implode('-', array_reverse( explode('/', $model->data_fim_afastamento))) : null;
				$model->data_inicio_evento = !empty($model->data_inicio_evento) ? implode('-', array_reverse( explode('/', $model->data_inicio_evento))) : null;
				$model->data_fim_evento = !empty($model->data_fim_evento) ? implode('-', array_reverse( explode('/', $model->data_fim_evento))) : null;

				if ($model->tipo === Afastamento::TIPO_NACIONAL) {
				    // Muda a situação para Liberado
                    $model->situacao = Afastamento::SITUACAO_LIBERADO;
					// Manda email para todos os professores (menos pro solicitante)
					$emails_professores = ArrayHelper::getColumn(Professor::find()->where(['not', ['id' => $model->professorSolicitante->id]])->all(), 'email');
					Yii::$app->email->enviarEmailPadrao(
						'Um novo afastamento foi cadastrado',
						'Acabamos de receber um novo pedido de afastamento. Você tem 10 dias para se manifestar contra, caso contrário ele será aprovado automaticamente.',
						'<b>Professor Solicitante:</b> '.$model->professorSolicitante->nome.' '.$model->professorSolicitante->sobrenome,
						$emails_professores
					);
				} else {
					// Manda email ao chefe do departamento
					$chefes = Mandato::recuperarChefeDepartamento();
					if (!empty($chefes)) {
						Yii::$app->email->enviarEmailPadrao(
							'Um novo afastamento foi cadastrado',
							'Acabamos de receber um novo pedido de afastamento.	Você, como chefe do departamento, precisa indicar um relator.',
							'<b>Professor Solicitante:</b> '.$model->professorSolicitante->nome.' '.$model->professorSolicitante->sobrenome,
							ArrayHelper::getColumn($chefes, 'email')
						);
					}
				}

				if (!$model->save()) {
					throw new Exception(Json::encode($model->getErrors()));
				}

				// Salva documentos no FTP
				if (!empty($post['Documento'])) {
					foreach ($post['Documento'] as $key => $documento) {
						$arquivo = UploadedFile::getInstanceByName('Documento['.$key.'][nome_arquivo]');
						$model_documento = new Documento();
						$model_documento->titulo = $documento['titulo'];
						$model_documento->nome_arquivo = !empty($arquivo) ? Yii::$app->ftp->enviarArquivoLocal($arquivo, date('Y-m-d_H-i-s').'_'.$model->id.'.'.$arquivo->extension) : null;
						$model_documento->data_juntada = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y-m-d H:i:s');
						$model_documento->afastamento_id = $model->id;
						if (!$model_documento->save()) {
							throw new Exception(Json::encode($model_documento->getErrors()));
						}
					}
				}
				Yii::$app->getSession()->setFlash('mensagens', [
					'type' => 'success',
					'message' => 'Afastamento criado com sucesso',
				]);
				$transaction->commit();
				return $this->redirect(['index']);
			} else {
				return $this->render('create', [
					'model' => $model,
				]);
			}
		} catch (Exception $e) {
			$model->data_inicio_afastamento = !empty($model->data_inicio_afastamento) ? implode('/', array_reverse( explode('-', $model->data_inicio_afastamento))) : null;
			$model->data_fim_afastamento = !empty($model->data_fim_afastamento) ? implode('/', array_reverse( explode('-', $model->data_fim_afastamento))) : null;
			$model->data_inicio_evento = !empty($model->data_inicio_evento) ? implode('/', array_reverse( explode('-', $model->data_inicio_evento))) : null;
			$model->data_fim_evento = !empty($model->data_fim_evento) ? implode('/', array_reverse( explode('-', $model->data_fim_evento))) : null;
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => $e->getMessage(),
			]);
			$transaction->rollBack();
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Creates a new Afastamento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreateSimples()
    // {
    //     $post = Yii::$app->request->post();
    //     $model = Afastamento::create($post);

    //     if ($model->isNewRecord) {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     } else {
    //         return $this->redirect(['index']);
    //     }
    // }

    /**
     * Finds the Afastamento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Afastamento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Afastamento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $id
     * @return Response
     * @throws Exception
     */
    public function actionRegistrarDecisaoDi($id)
    {
        $afastamento = Afastamento::findOne($id);

        // CONDIÇÕES
        // - Situação é Aguardando decisão DI
        // - O usuário logado deve ser um secretário
        if (
            $afastamento->situacao !== Afastamento::SITUACAO_AGUARDANDO_DECISAO_DI ||
            empty(Yii::$app->user->identity->secretario)
        ) {
            throw new Exception('Ação não permitida');
        }

        $post = Yii::$app->request->post();
        $decisao = $post['decisao'];

        $afastamento->situacao =
            $decisao === 'favoravel' ?
                Afastamento::SITUACAO_APROVADO_DI :
                Afastamento::SITUACAO_REPROVADO;

        // Salva documentos no FTP
        if (!empty($post['Documento'])) {
            foreach ($post['Documento'] as $key => $documento) {
                $arquivo = UploadedFile::getInstanceByName('Documento['.$key.'][nome_arquivo]');
                $model_documento = new Documento();
                $model_documento->titulo = $documento['titulo'];
                $model_documento->nome_arquivo = !empty($arquivo) ? Yii::$app->ftp->enviarArquivoLocal($arquivo, date('Y-m-d_H-i-s').'_'.$afastamento->id.'.'.$arquivo->extension) : null;
                $model_documento->data_juntada = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y-m-d H:i:s');
                $model_documento->afastamento_id = $afastamento->id;
                if (!$model_documento->save()) {
                    throw new Exception(Json::encode($model_documento->getErrors()));
                }
            }
        }

        if ($afastamento->save()) {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'success',
                'message' => 'A decisão do DI foi registrada com sucesso!',
            ]);
        } else {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'danger',
                'message' => 'Houve uma falha ao registrar a decisão do DI. '.Json::encode($afastamento->getErrors()),
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

	/**
	 * @param $id
	 * @return Response
	 */
    public function actionRegistrarDecisaoCt($id)
	{
        $afastamento = Afastamento::findOne($id);

		// CONDIÇÕES
		// - Afastamento é internacional
		// - Situação é Aprovado-DI
		// - O usuário logado deve ser um secretário
        if (
            $afastamento->tipo !== Afastamento::TIPO_INTERNACIONAL ||
            $afastamento->situacao !== Afastamento::SITUACAO_APROVADO_DI ||
            empty(Yii::$app->user->identity->secretario)
        ) {
            throw new Exception('Ação não permitida');
        }

        $post = Yii::$app->request->post();
        $decisao = $post['decisao'];

        $afastamento->situacao =
            $decisao === 'favoravel' ?
                Afastamento::SITUACAO_APROVADO_CT :
                Afastamento::SITUACAO_REPROVADO;

        // Salva documentos no FTP
        if (!empty($post['Documento'])) {
            foreach ($post['Documento'] as $key => $documento) {
                $arquivo = UploadedFile::getInstanceByName('Documento['.$key.'][nome_arquivo]');
                $model_documento = new Documento();
                $model_documento->titulo = $documento['titulo'];
                $model_documento->nome_arquivo = !empty($arquivo) ? Yii::$app->ftp->enviarArquivoLocal($arquivo, date('Y-m-d_H-i-s').'_'.$afastamento->id.'.'.$arquivo->extension) : null;
                $model_documento->data_juntada = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y-m-d H:i:s');
                $model_documento->afastamento_id = $afastamento->id;
                if (!$model_documento->save()) {
                    throw new Exception(Json::encode($model_documento->getErrors()));
                }
            }
        }

        if ($afastamento->save()) {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'success',
                'message' => 'A decisão do CT foi registrada com sucesso!',
            ]);
        } else {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'danger',
                'message' => 'Houve uma falha ao registrar a decisão do CT. '.Json::encode($afastamento->getErrors()),
            ]);
        }

		return $this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * @param $id
	 * @return Response
	 */
	public function actionRegistrarDecisaoPrppg($id)
	{
        $afastamento = Afastamento::findOne($id);

		// CONDIÇÕES
		// - Afastamento é internacional
		// - Situação é Aprovado-CT
		// - O usuário logado deve ser um secretário
        if (
            $afastamento->tipo !== Afastamento::TIPO_INTERNACIONAL ||
            $afastamento->situacao !== Afastamento::SITUACAO_APROVADO_CT ||
            empty(Yii::$app->user->identity->secretario)
        ) {
            throw new Exception('Ação não permitida');
        }

        $post = Yii::$app->request->post();
        $decisao = $post['decisao'];

        $afastamento->situacao =
            $decisao === 'favoravel' ?
                Afastamento::SITUACAO_APROVADO_PRPPG :
                Afastamento::SITUACAO_REPROVADO;

        // Salva documentos no FTP
        if (!empty($post['Documento'])) {
            foreach ($post['Documento'] as $key => $documento) {
                $arquivo = UploadedFile::getInstanceByName('Documento['.$key.'][nome_arquivo]');
                $model_documento = new Documento();
                $model_documento->titulo = $documento['titulo'];
                $model_documento->nome_arquivo = !empty($arquivo) ? Yii::$app->ftp->enviarArquivoLocal($arquivo, date('Y-m-d_H-i-s').'_'.$afastamento->id.'.'.$arquivo->extension) : null;
                $model_documento->data_juntada = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y-m-d H:i:s');
                $model_documento->afastamento_id = $afastamento->id;
                if (!$model_documento->save()) {
                    throw new Exception(Json::encode($model_documento->getErrors()));
                }
            }
        }

        if ($afastamento->save()) {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'success',
                'message' => 'A decisão do PRPPG foi registrada com sucesso!',
            ]);
        } else {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'danger',
                'message' => 'Houve uma falha ao registrar a decisão do PRPPG. '.Json::encode($afastamento->getErrors()),
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer);
	}

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws Exception
     */
	public function actionRegistrarParecerRelator($id)
	{
        $model = $this->findModel($id);

		// CONDIÇÕES
		// - Afastamento é internacional
		// - Situação é Aguardando parecer relator
        // - O usuário logado deve ser um professor
		// - O usuário logado deve ser o relator
        if (
            $model->tipo !== Afastamento::TIPO_INTERNACIONAL ||
            $model->situacao !== Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR ||
            empty(Yii::$app->user->identity->professor) ||
            $model->professorRelator->id !== Yii::$app->user->identity->professor->id
        ) {
            throw new Exception('Ação não permitida');
        }

		$model->situacao = Afastamento::SITUACAO_LIBERADO;
		$model_parecer = new Parecer();
		$model_parecer->afastamento_id = $id;
		$model_parecer->professor_id = $model->professorRelator->id;
		$model_parecer->data_parecer = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y-m-d');
		$post = Yii::$app->request->post();

		if ($model_parecer->load($post) && $model_parecer->save() && $model->save()) {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'success',
				'message' => 'O Parecer do relator foi registrado com sucesso!',
			]);
		} else {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => 'Houve uma falha ao registrar o parecer do relator. '.Json::encode($model_parecer->getErrors().' '.Json::encode($model->getErrors())),
			]);
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * @param $id
	 * @return Response
	 */
	public function actionManifestarSeContra($id)
	{
        $model = $this->findModel($id);

		// CONDIÇÕES
		// - Situação é Liberado
		// - O usuário logado deve ser um professor
        if (
            $model->situacao !== Afastamento::SITUACAO_LIBERADO ||
            empty(Yii::$app->user->identity->professor)
        ) {
            throw new Exception('Ação não permitida');
        }

        $model->situacao = Afastamento::SITUACAO_AGUARDANDO_DECISAO_DI;
		$model_parecer = new Parecer();
		$model_parecer->afastamento_id = $id;
		$model_parecer->professor_id = Yii::$app->user->identity->professor->id;
		$model_parecer->data_parecer = (new \DateTime('now', new \DateTimeZone('America/Sao_Paulo')))->format('Y-m-d');
		$post = Yii::$app->request->post();

		unset($post['Parecer']['tipo_parecer']);
		$model_parecer->tipo_parecer = Parecer::TIPO_PARECER_DESFAVORAVEL;

		if ($model_parecer->load($post) && $model_parecer->save() && $model->save()) {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'success',
				'message' => 'O Parecer do professor foi registrado com sucesso!',
			]);
		} else {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => 'Houve uma falha ao registrar o parecer do professor. '.Json::encode($model_parecer->getErrors()),
			]);
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * @param $id
	 * @return Response
	 * @throws NotFoundHttpException
	 */
	public function actionEncaminharAfastamento($id)
	{
        $model = $this->findModel($id);

		// CONDIÇÕES
        // - Afastamento é internacional
        // - Situação é Iniciado
        // - O usuário logado deve ser um professor
        // - Um professor deve estar exercendo o cargo de chefe do departamento
        // - O usuário logado deve ser o professor chefe do departamento
        if (
            $model->tipo !== Afastamento::TIPO_INTERNACIONAL ||
            $model->situacao !== Afastamento::SITUACAO_INICIADO ||
            empty(Yii::$app->user->identity->professor) ||
            !Mandato::recuperarChefeDepartamento() ||
            !in_array(Yii::$app->user->identity->professor->id, ArrayHelper::getColumn(Mandato::recuperarChefeDepartamento(), 'id'))
        ) {
            throw new Exception('Ação não permitida');
        }

		$post = Yii::$app->request->post();

        $model->situacao = Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR;

		if ($model->load($post) && $model->save()) {
			// Manda email para o relator
			Yii::$app->email->enviarEmailPadrao(
				'Você foi convocado a ser o relator de um afastamento',
				'Você acabou de ser convocado pelo chefe do departamento a ser o relator de um afastamento.',
				'<b>Professor Solicitante:</b> '.$model->professorSolicitante->nome.' '.$model->professorSolicitante->sobrenome,
				$model->professorRelator->email
			);
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'success',
				'message' => 'O relator do afastamento foi registrado com sucesso!',
			]);
		} else {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => 'Houve uma falha ao registrar o relator do afastamento. '.Json::encode($model->getErrors()),
			]);
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * @param $id
	 * @return Response
	 * @throws NotFoundHttpException
	 */
	public function actionArquivar($id)
	{
        $model = $this->findModel($id);

        // CONDIÇÕES
        // - Situação é Reprovado ou Cancelado ou (Aprovado-PRPPG e Internacional) ou (Aprovado-DI e Nacional)
        // - O usuário logado deve ser um secretário
        if (
            empty(Yii::$app->user->identity->secretario) ||
            (
                $model->situacao !== Afastamento::SITUACAO_REPROVADO &&
                $model->situacao !== Afastamento::SITUACAO_CANCELADO &&
                ($model->situacao !== Afastamento::SITUACAO_APROVADO_PRPPG || $model->tipo !== Afastamento::TIPO_INTERNACIONAL) &&
                ($model->situacao !== Afastamento::SITUACAO_APROVADO_DI || $model->tipo !== Afastamento::TIPO_NACIONAL)
            )
        ) {
            throw new \Exception('Ação não permitida');
        }

		$model->situacao = Afastamento::SITUACAO_ARQUIVADO;
		if ($model->save()) {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'success',
				'message' => 'O afastamento foi arquivado com sucesso!',
			]);
		} else {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => 'Houve uma falha ao arquivar o afastamento. '.Json::encode($model->getErrors()),
			]);
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * @param $id
	 * @return Response
	 * @throws NotFoundHttpException
	 */
	public function actionCancelar($id)
	{
        $model = $this->findModel($id);

        // CONDIÇÕES
        // - Situação deve ser diferente de Arquivado e diferente de Reprovado
        // - O usuário logado deve ser um professor
        // - O usuário logado deve ser o professor solicitante
        if (
            in_array($model->situacao, [Afastamento::SITUACAO_ARQUIVADO, Afastamento::SITUACAO_REPROVADO]) ||
            empty(Yii::$app->user->identity->professor) ||
            $model->professor_solicitante_id !== Yii::$app->user->identity->professor->id
        ) {
            throw new \Exception('Ação não permitida');
        }

		$model->situacao = Afastamento::SITUACAO_CANCELADO;

		if ($model->save()) {
			// Manda email ao chefe do departamento
			$chefes = Mandato::recuperarChefeDepartamento();
			if (!empty($chefes)) {
				Yii::$app->email->enviarEmailPadrao(
					'Um afastamento foi cancelado',
					'Um afastamento acaba de ser cancelado pelo solicitante.',
					'<b>Professor Solicitante:</b> '.$model->professorSolicitante->nome.' '.$model->professorSolicitante->sobrenome,
                    ArrayHelper::getColumn($chefes, 'email')
				);
			}
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'success',
				'message' => 'O afastamento foi cancelado com sucesso!',
			]);
		} else {
			Yii::$app->getSession()->setFlash('mensagens', [
				'type' => 'danger',
				'message' => 'Houve uma falha ao cancelar o afastamento. '.Json::encode($model->getErrors()),
			]);
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	/**
	 * @return Response
	 */
	public function actionAtualizarSituacoes()
	{
		$models = Afastamento::find()
			->where('situacao="liberado" and CURRENT_DATE > adddate(data_solicitacao, INTERVAL 10 days)')
			->all();

		$quantidade_afastamentos_aprovados = 0;

		foreach ($models as $model) {
			$quantidade_desfavoravel = 0;
			if (!empty($model->pareceres)) {
				foreach ($model->pareceres as $parecer) {
					if ($parecer->tipo_parecer === Parecer::TIPO_PARECER_DESFAVORAVEL &&
                        $parecer->afastamento->professorRelator->id !== $parecer->professor->id) {
						$quantidade_desfavoravel++;
					}
				}
			}
			if ($quantidade_desfavoravel === 0) {
				$model->situacao = Afastamento::SITUACAO_APROVADO_DI;
				$quantidade_afastamentos_aprovados++;
			}
		}

		Yii::$app->getSession()->setFlash('mensagens', [
			'type' => 'success',
			'message' => 'Foram aprovados '.$quantidade_afastamentos_aprovados.' afastamentos.',
		]);

		return $this->redirect(Yii::$app->request->referrer);
	}
}
