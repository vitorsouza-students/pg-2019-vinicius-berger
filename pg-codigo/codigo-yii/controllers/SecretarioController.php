<?php

namespace app\controllers;

use app\models\Usuario;
use Yii;
use app\models\Secretario;
use app\models\SecretarioSearch;
use yii\base\Security;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Exception;

/**
 * SecretarioController implements the CRUD actions for Secretario model.
 */
class SecretarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Secretario models.
     * @return mixed
     */
    public function actionIndex()
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        $searchModel = new SecretarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Secretario model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Secretario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        $model = new Secretario();
		$model_usuario = new Usuario();
		$transaction = Yii::$app->db->beginTransaction();

		try {
			if ($model->load(Yii::$app->request->post())) {
				$model_usuario->login = $model->email;
				$model_usuario->senha = sha1($model->email);
				if (!$model_usuario->save()) {
					throw new \Exception(Json::encode($model_usuario->getErrors()));
				}
				$model->usuario_id = $model_usuario->id;
				if (!$model->save()) {
					throw new \Exception(Json::encode($model->getErrors()));
				}
				$transaction->commit();
				// Manda email para o secretário
				Yii::$app->email->enviarEmailPadrao(
					'Seja bem-vindo',
					'Você foi cadastrado na plataforma SCAP - Sistema de Controle de Afastamento de Professores 
					como "Secretário". Seus dados de acesso estão listados a seguir (sugerimos que troque sua 
					senha assim que entrar no sistema).',
					'<b>Login:</b> '.$model->email.'<br><b>Senha provisória:</b> '.$model->email,
					$model->email
				);
                Yii::$app->getSession()->setFlash('mensagens', [
                    'type' => 'success',
                    'message' => 'O secretário foi criado com sucesso',
                ]);
				return $this->redirect(['index']);
			} else {
				return $this->render('create', [
					'model' => $model,
				]);
			}
		} catch (\Exception $e) {
			$transaction->rollBack();
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'danger',
                'message' => $e->getMessage(),
            ]);
			return $this->render('create', [
				'model' => $model,
			]);
		}
    }

    /**
     * Updates an existing Secretario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        $model = $this->findModel($id);

        try {
            if ($model->load(Yii::$app->request->post())) {
                if (!$model->save()) {
                    throw new Exception(Json::encode($model->getErrors()));
                }
                Yii::$app->getSession()->setFlash('mensagens', [
                    'type' => 'success',
                    'message' => 'O secretário foi atualizado com sucesso',
                ]);
                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } catch (Exception $e) {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'danger',
                'message' => $e->getMessage(),
            ]);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Secretario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        // CONDIÇÕES
        // - O usuário logado deve ser um secretário
        if (empty(Yii::$app->user->identity->secretario)) {
            throw new Exception('Ação não permitida');
        }

        if ($this->findModel($id)->delete()) {
            Yii::$app->getSession()->setFlash('mensagens', [
                'type' => 'success',
                'message' => 'O secretário foi removido com sucesso',
            ]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Secretario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Secretario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Secretario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
