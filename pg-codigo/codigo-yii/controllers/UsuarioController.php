<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionTrocarSenha()
    {
        $model = Usuario::findOne(Yii::$app->user->identity->getId());
        $senha = Yii::$app->request->post('Usuario')['senha'];

        if (!empty($senha)) {
            $model->senha = sha1($senha);
            if($model->save()){
                Yii::$app->getSession()->setFlash('mensagens', [
                    'type' => 'success',
                    'message' => 'Senha alterada com sucesso',
                ]);
            } else {
                Yii::$app->getSession()->setFlash('mensagens', [
                    'type' => 'danger',
                    'message' => 'Não foi possível alterar a senha',
                ]);
            }
            return $this->goBack();
        } else {
            return $this->render('trocar-senha', [
                'model' => $model,
            ]);
        }
    }
}
