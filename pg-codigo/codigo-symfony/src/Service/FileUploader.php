<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploader
{
    const
        URL_BASE_DOCUMENTOS = 'https://www.viniciush4.com/documentos',
        SERVIDOR = 'viniciush4.com',
        USUARIO = 'u498025531',
        SENHA = 'Ftp123456',
        DIRETORIO_DOCUMENTOS = 'domains/viniciush4.com/public_html/documentos';

    /**
     * @param $file
     * @param $filename
     */
//    public function upload($file, $filename)
//    {
//        try {
//            $uploadDir = '../var/uploads';
//            $file->move($uploadDir, $filename);
//        } catch (FileException $e){
//            throw new FileException('Failed to upload file');
//        }
//    }

    /**
     * @return string
     */
    public function getUrlBaseDocumentos()
    {
        return self::URL_BASE_DOCUMENTOS;
    }

    /**
     * @param $arquivo
     * @param $nome_arquivo
     * @return string|null
     * @throws \Exception
     */
    function enviarArquivo($arquivo, $nome_arquivo)
    {
        if(empty($arquivo) || empty($nome_arquivo)){
            return null;
        }

        $con_id = $this->connectarFTP();

        ftp_chdir($con_id, self::DIRETORIO_DOCUMENTOS);

        $arquivo_aberto = fopen($arquivo, 'r');

        if(!ftp_fput($con_id, $nome_arquivo, $arquivo_aberto, FTP_BINARY)){
            return null;
        }

        ftp_close($con_id);

        return $nome_arquivo;
    }

    /**
     * @param $nome_arquivo
     * @return string
     */
    function removerArquivo($nome_arquivo)
    {
        if(empty($nome_arquivo)){
            return null;
        }

        $con_id = $this->connectarFTP();

        ftp_chdir($con_id, self::DIRETORIO_DOCUMENTOS);

        ftp_delete($con_id, $nome_arquivo);

        ftp_close($con_id);

        return $nome_arquivo;
    }

    /**
     * @return type Conexão do FTP.
     */
    function connectarFTP()
    {
        $con_id = ftp_connect(self::SERVIDOR);

        $login_result = ftp_login( $con_id, self::USUARIO, self::SENHA );

        if ((!$con_id) || (!$login_result)) {
            die("A conexão FTP falhou!");
        }

        // Libera a transferência de arquivo
        ftp_pasv($con_id, true);

        return $con_id;
    }
}
