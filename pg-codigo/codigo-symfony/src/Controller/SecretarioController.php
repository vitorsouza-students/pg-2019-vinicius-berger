<?php

namespace App\Controller;

use App\Entity\Secretario;
use App\Entity\Usuario;
use App\Form\SecretarioType;
use App\Repository\SecretarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/secretario")
 * @IsGranted("ROLE_SECRETARIO")
 */
class SecretarioController extends Controller
{
    /**
     * @Route("/", name="secretario_index", methods={"GET"})
     */
    public function index(SecretarioRepository $secretarioRepository): Response
    {
        return $this->render('secretario/index.html.twig', [
            'secretarios' => $secretarioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="secretario_new", methods={"GET","POST"})
     */
    public function new(Request $request, \Swift_Mailer $mailer): Response
    {
        $secretario = new Secretario();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(SecretarioType::class, $secretario);
        $form->handleRequest($request);

        $entityManager->getConnection()->beginTransaction();

        try {
            if ($form->isSubmitted() && $form->isValid()) {

                $usuario = new Usuario();
                $usuario->setLogin($secretario->getEmail());
                $usuario->setPassword(sha1($secretario->getEmail()));
                $entityManager->persist($usuario);

                $secretario->setUsuario($usuario);
                $entityManager->persist($secretario);

                $entityManager->flush();
                $entityManager->getConnection()->commit();

                // Envia email para o secretário
                $mailer->send(
                    (new \Swift_Message())
                        ->setSubject('SCAP - Seja bem-vindo')
                        ->setFrom(['nao-responda@viniciush4.com' => 'SCAP'])
                        ->setTo([$secretario->getEmail() => $secretario->getNome()])
                        ->setBody(
                            $this->renderView(
                                'emails/base.html.twig',
                                [
                                    'titulo' => 'Seja bem-vindo',
                                    'mensagem' => 'Você foi cadastrado na plataforma SCAP - Sistema de Controle de Afastamento de Professores 
					                    como "Secretário". Seus dados de acesso estão listados a seguir (sugerimos que troque sua 
					                    senha assim que entrar no sistema).',
                                    'detalhes' => '<b>Login:</b> '.$secretario->getEmail().'<br><b>Senha provisória:</b> '.$secretario->getEmail()
                                ]
                            ),
                            'text/html'
                        )
                );

                $this->addFlash(
                    'success',
                    'Secretário cadastrado com sucesso'
                );

                return $this->redirectToRoute('secretario_index');
            }
        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            $this->addFlash(
                'danger',
                $e->getMessage()
            );
        }

        return $this->render('secretario/new.html.twig', [
            'secretario' => $secretario,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="secretario_show", methods={"GET"})
     */
    public function show(Secretario $secretario): Response
    {
        return $this->render('secretario/show.html.twig', [
            'secretario' => $secretario,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="secretario_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Secretario $secretario): Response
    {
        $form = $this->createForm(SecretarioType::class, $secretario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('secretario_index');
        }

        return $this->render('secretario/edit.html.twig', [
            'secretario' => $secretario,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="secretario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Secretario $secretario): Response
    {
        if ($this->isCsrfTokenValid('delete'.$secretario->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($secretario);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Secretário removido com sucesso'
            );
        }

        return $this->redirectToRoute('secretario_index');
    }
}
