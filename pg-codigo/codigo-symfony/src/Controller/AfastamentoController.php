<?php

namespace App\Controller;

use App\Entity\Afastamento;
use App\Entity\Documento;
use App\Entity\Parecer;
use App\Form\AfastamentoType;
use App\Repository\AfastamentoRepository;
use App\Repository\MandatoRepository;
use App\Repository\ProfessorRepository;
use App\Service\FileUploader;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use yii\helpers\Json;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/afastamento")
 */
class AfastamentoController extends Controller
{
    /**
     * @Route("/", name="afastamento_index", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function index(Request $request, AfastamentoRepository $afastamentoRepository, ProfessorRepository $professorRepository): Response
    {
        $search = $request->get('AfastamentoSearch');

        if ($search) {
            $professor_solicitante_id = $search['professor_solicitante_id'] ? $search['professor_solicitante_id'] : null;
            $data_solicitacao = $search['data_solicitacao'] ? implode('-', array_reverse( explode('/', $search['data_solicitacao']))) : null;
            $tipo = $search['tipo'] ? $search['tipo'] : null;
            $situacao = $search['situacao'] ? $search['situacao'] : null;

            return $this->render('afastamento/index.html.twig', [
                'afastamentos' => $afastamentoRepository->buscarAfastamentos(
                    $professor_solicitante_id,
                    $data_solicitacao,
                    $tipo,
                    $situacao
                ),
                'professores' => $professorRepository->findAll()
            ]);
        }

        return $this->render('afastamento/index.html.twig', [
            'afastamentos' => $afastamentoRepository->findAll(),
            'professores' => $professorRepository->findAll()
        ]);
    }

    /**
     * @Route("/atualizar-situacoes", name="afastamento_atualizar_situacoes", methods={"GET"})
     */
    public function atualizarSituacoes(AfastamentoRepository $afastamentoRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $afastamentos_ativos = $afastamentoRepository->recuperarAfastamentosAtivos();

        $quantidade_afastamentos_aprovados = 0;

        foreach ($afastamentos_ativos as $afastamento) {
            $quantidade_desfavoravel = 0;
            if (!empty($afastamento->getPareceres())) {
                foreach ($afastamento->getPareceres() as $parecer) {
                    if ($parecer->getTipoParecer() === Parecer::TIPO_PARECER_DESFAVORAVEL &&
                        $parecer->getAfastamento()->getProfessorRelatorId() !== $parecer->getProfessorId()) {
                        $quantidade_desfavoravel++;
                    }
                }
            }
            if ($quantidade_desfavoravel === 0) {
                $afastamento->setSituacao(Afastamento::SITUACAO_APROVADO_DI);
                $entityManager->flush();
                $quantidade_afastamentos_aprovados++;
            }
        }

        $this->addFlash(
            'success',
            'Foram aprovados '.$quantidade_afastamentos_aprovados.' afastamentos.'
        );

        return $this->redirectToRoute('afastamento_index');
    }

    /**
     * @Route("/new", name="afastamento_new", methods={"GET","POST"})
     * @IsGranted("ROLE_PROFESSOR")
     */
    public function new(Request $request, FileUploader $uploader, \Swift_Mailer $mailer, ProfessorRepository $professorRepository, MandatoRepository $mandatoRepository): Response
    {
        $afastamento = new Afastamento();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(AfastamentoType::class, $afastamento);
        $form->handleRequest($request);

        $entityManager->getConnection()->beginTransaction();

        try {
            if ($form->isSubmitted() && $form->isValid()) {

                $afastamento->setDataSolicitacao(new DateTime('now'));
                $afastamento->setSituacao(Afastamento::SITUACAO_INICIADO);
                $afastamento->setProfessorSolicitante($this->getUser()->getProfessor());

                if ($form->getData()->getTipo() === Afastamento::TIPO_NACIONAL) {
                    // Muda a situação para Liberado
                    $afastamento->setSituacao(Afastamento::SITUACAO_LIBERADO);
                    // Manda email para todos os professores (menos pro solicitante)
                    $emails_professores = array_column($professorRepository->recuperarTodosEmails(), 'email');
                    $mailer->send(
                        (new \Swift_Message())
                            ->setSubject('SCAP - Um novo afastamento foi cadastrado')
                            ->setFrom(['nao-responda@viniciush4.com' => 'SCAP'])
                            ->setTo($emails_professores)
                            ->setBody(
                                $this->renderView(
                                    'emails/base.html.twig',
                                    [
                                        'titulo' => 'Um novo afastamento foi cadastrado',
                                        'mensagem' => 'Acabamos de receber um novo pedido de afastamento. Você tem 10 dias para se manifestar contra, caso contrário ele será aprovado automaticamente.',
                                        'detalhes' => '<b>Professor Solicitante:</b> '.$afastamento->getProfessorSolicitante()->getNome().' '.$afastamento->getProfessorSolicitante()->getSobrenome()
                                    ]
                                ),
                                'text/html'
                            )
                    );
                } else {
                    // Manda email ao chefe do departamento
                    $chefe = $mandatoRepository->recuperarChefeDepartamento();
                    if (!empty($chefe)) {
                        $mailer->send(
                            (new \Swift_Message())
                                ->setSubject('SCAP - Um novo afastamento foi cadastrado')
                                ->setFrom(['nao-responda@viniciush4.com' => 'SCAP'])
                                ->setTo([$chefe->getEmail() => $chefe->getNome()])
                                ->setBody(
                                    $this->renderView(
                                        'emails/base.html.twig',
                                        [
                                            'titulo' => 'Um novo afastamento foi cadastrado',
                                            'mensagem' => 'Acabamos de receber um novo pedido de afastamento. Você, como chefe do departamento, precisa indicar um relator.',
                                            'detalhes' => '<b>Professor Solicitante:</b> '.$afastamento->getProfessorSolicitante()->getNome().' '.$afastamento->getProfessorSolicitante()->getSobrenome()
                                        ]
                                    ),
                                    'text/html'
                                )
                        );
                    }
                }

                $entityManager->persist($afastamento);

                $documentos = $request->get('Documento');

                if (!empty($documentos)) {
                    foreach ($documentos as $key => $documento) {
                        $file = $request->files->get('Documento')[$key]['nome_arquivo'];

                        if (empty($file)) {
                            throw new \Exception('Selecione um arquivo para o documento '.$documento['titulo']);
                        }

                        $filename = date('Y-m-d_H-i-s').'_'.$file->getClientOriginalName();
                        $uploader->enviarArquivo($file, $filename);

                        $model_documento = new Documento();
                        $model_documento->setDataJuntada(new DateTime('now'));
                        $model_documento->setTitulo($documento['titulo']);
                        $model_documento->setNomeArquivo($filename);
                        $model_documento->setAfastamento($afastamento);
                        $entityManager->persist($model_documento);
                    }
                }

                $entityManager->flush();
                $entityManager->getConnection()->commit();

                $this->addFlash(
                    'success',
                    'Afastamento cadastrado com sucesso'
                );

                return $this->redirectToRoute('afastamento_index');
            }
        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            $this->addFlash(
                'danger',
                $e->getMessage()
            );
        }

        return $this->render('afastamento/new.html.twig', [
            'afastamento' => $afastamento,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="afastamento_show", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function show(Afastamento $afastamento, ProfessorRepository $professorRepository): Response
    {
        return $this->render('afastamento/show.html.twig', [
            'afastamento' => $afastamento,
            'professores' => $professorRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}/registrar-decisao-di", name="afastamento_registrar_decisao_di", methods={"POST"})
     * @IsGranted("ROLE_SECRETARIO")
     */
    public function registrarDecisaoDi(Request $request, Afastamento $afastamento, FileUploader $uploader): Response
    {
        // CONDIÇÕES
        // - Situação é Aguardando decisão DI
        // - O usuário logado deve ser um secretário (já garantido)
        if ($afastamento->getSituacao() !== Afastamento::SITUACAO_AGUARDANDO_DECISAO_DI) {
            throw new \Exception('Ação não permitida');
        }

        $decisao = $request->get('decisao');
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->getConnection()->beginTransaction();

        try {
            $afastamento->setSituacao(
                $decisao === 'favoravel' ?
                    Afastamento::SITUACAO_APROVADO_DI :
                    Afastamento::SITUACAO_REPROVADO
            );

            if (!empty($request->get('Documento'))) {
                foreach ($request->get('Documento') as $key => $documento) {
                    $file = $request->files->get('Documento')[$key]['nome_arquivo'];

                    if (empty($file)) {
                        throw new \Exception('Selecione um arquivo para o documento '.$documento['titulo']);
                    }

                    $filename = date('Y-m-d_H-i-s').'_'.$file->getClientOriginalName();
                    $uploader->enviarArquivo($file, $filename);

                    $model_documento = new Documento();
                    $model_documento->setDataJuntada(new DateTime('now'));
                    $model_documento->setTitulo($documento['titulo']);
                    $model_documento->setNomeArquivo($filename);
                    $model_documento->setAfastamento($afastamento);
                    $entityManager->persist($model_documento);
                }
            }

            $entityManager->flush();
            $entityManager->getConnection()->commit();

            $this->addFlash(
                'success',
                'Decisão do DI registrada com sucesso.'
            );

        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            $this->addFlash(
                'danger',
                'Erro ao registrar decisão do DI.'
            );
        }

        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }

    /**
     * @Route("/{id}/registrar-decisao-ct", name="afastamento_registrar_decisao_ct", methods={"POST"})
     * @IsGranted("ROLE_SECRETARIO")
     */
    public function registrarDecisaoCt(Request $request, Afastamento $afastamento, FileUploader $uploader): Response
    {
        // CONDIÇÕES
		// - Afastamento é internacional
		// - Situação é Aprovado-DI
		// - O usuário logado deve ser um secretário (já garantido)
        if (
            $afastamento->getTipo() !== Afastamento::TIPO_INTERNACIONAL ||
            $afastamento->getSituacao() !== Afastamento::SITUACAO_APROVADO_DI
        ) {
            throw new \Exception('Ação não permitida');
        }

        $decisao = $request->get('decisao');
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->getConnection()->beginTransaction();

        try {
            $afastamento->setSituacao(
                $decisao === 'favoravel' ?
                    Afastamento::SITUACAO_APROVADO_CT :
                    Afastamento::SITUACAO_REPROVADO
            );

            if (!empty($request->get('Documento'))) {
                foreach ($request->get('Documento') as $key => $documento) {
                    $file = $request->files->get('Documento')[$key]['nome_arquivo'];

                    if (empty($file)) {
                        throw new \Exception('Selecione um arquivo para o documento '.$documento['titulo']);
                    }

                    $filename = date('Y-m-d_H-i-s').'_'.$file->getClientOriginalName();
                    $uploader->enviarArquivo($file, $filename);

                    $model_documento = new Documento();
                    $model_documento->setDataJuntada(new DateTime('now'));
                    $model_documento->setTitulo($documento['titulo']);
                    $model_documento->setNomeArquivo($filename);
                    $model_documento->setAfastamento($afastamento);
                    $entityManager->persist($model_documento);
                }
            }

            $entityManager->flush();
            $entityManager->getConnection()->commit();

            $this->addFlash(
                'success',
                'Decisão do CT registrada com sucesso.'
            );

        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            $this->addFlash(
                'danger',
                'Erro ao registrar decisão do CT.'
            );
        }

        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }

    /**
     * @Route("/{id}/registrar-decisao-prppg", name="afastamento_registrar_decisao_prppg", methods={"POST"})
     * @IsGranted("ROLE_SECRETARIO")
     */
    public function registrarDecisaoPrppg(Request $request, Afastamento $afastamento, FileUploader $uploader): Response
    {
        // CONDIÇÕES
		// - Afastamento é internacional
		// - Situação é Aprovado-CT
		// - O usuário logado deve ser um secretário (já garantido)
        if (
            $afastamento->getTipo() !== Afastamento::TIPO_INTERNACIONAL ||
            $afastamento->getSituacao() !== Afastamento::SITUACAO_APROVADO_CT
        ) {
            throw new \Exception('Ação não permitida');
        }

        $decisao = $request->get('decisao');
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->getConnection()->beginTransaction();

        try {
            $afastamento->setSituacao(
                $decisao === 'favoravel' ?
                    Afastamento::SITUACAO_APROVADO_PRPPG :
                    Afastamento::SITUACAO_REPROVADO
            );

            if (!empty($request->get('Documento'))) {
                foreach ($request->get('Documento') as $key => $documento) {
                    $file = $request->files->get('Documento')[$key]['nome_arquivo'];

                    if (empty($file)) {
                        throw new \Exception('Selecione um arquivo para o documento '.$documento['titulo']);
                    }

                    $filename = date('Y-m-d_H-i-s').'_'.$file->getClientOriginalName();
                    $uploader->enviarArquivo($file, $filename);

                    $model_documento = new Documento();
                    $model_documento->setDataJuntada(new DateTime('now'));
                    $model_documento->setTitulo($documento['titulo']);
                    $model_documento->setNomeArquivo($filename);
                    $model_documento->setAfastamento($afastamento);
                    $entityManager->persist($model_documento);
                }
            }

            $entityManager->flush();
            $entityManager->getConnection()->commit();

            $this->addFlash(
                'success',
                'Decisão da PRPPG registrada com sucesso.'
            );

        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            $this->addFlash(
                'danger',
                'Erro ao registrar decisão da PRPPG.'
            );
        }

        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }

    /**
     * @Route("/{id}/registrar-parecer-relator", name="afastamento_registrar_parecer_relator", methods={"POST"})
     * @IsGranted("ROLE_PROFESSOR")
     */
    public function registrarParecerRelator(Request $request, Afastamento $afastamento): Response
    {
        // CONDIÇÕES
		// - Afastamento é internacional
		// - Situação é Aguardando parecer relator
        // - O usuário logado deve ser um professor (já garantido)
		// - O usuário logado deve ser o relator
        if (
            $afastamento->getTipo() !== Afastamento::TIPO_INTERNACIONAL ||
            $afastamento->getSituacao() !== Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR ||
            $afastamento->getProfessorRelatorId() !== $this->getUser()->getProfessor()->getId()
        ) {
            throw new \Exception('Ação não permitida');
        }

        $post = $request->get('Parecer');

        $model_parecer = new Parecer();
        $model_parecer->setAfastamento($afastamento);
        $model_parecer->setProfessor($afastamento->getProfessorRelator());
        $model_parecer->setDataParecer(new DateTime('now'));
        $model_parecer->setTipoParecer($post['tipo_parecer']);
        $model_parecer->setMotivo($post['motivo']);

        $afastamento->setSituacao(Afastamento::SITUACAO_LIBERADO);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($model_parecer);
        $entityManager->flush();

        if ($model_parecer->getId()) {
            $this->addFlash(
                'success',
                'Parecer do Relator registrado com sucesso.'
            );
        } else {
            $this->addFlash(
                'danger',
                'Erro ao registrar parecer do Relator.'
            );
        }
        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }

    /**
     * @Route("/{id}/manifestar-se-contra", name="afastamento_manifestar_se_contra", methods={"POST"})
     * @IsGranted("ROLE_PROFESSOR")
     */
    public function manifestarSeContra(Request $request, Afastamento $afastamento): Response
    {
        // CONDIÇÕES
		// - Situação é Liberado
		// - O usuário logado deve ser um professor (já garantido)
        if ($afastamento->getSituacao() !== Afastamento::SITUACAO_LIBERADO) {
            throw new \Exception('Ação não permitida');
        }

        $post = $request->get('Parecer');

        $model_parecer = new Parecer();
        $model_parecer->setAfastamento($afastamento);
        $model_parecer->setProfessor($this->getUser()->getProfessor());
        $model_parecer->setDataParecer(new DateTime('now'));
        $model_parecer->setTipoParecer(Parecer::TIPO_PARECER_DESFAVORAVEL);
        $model_parecer->setMotivo($post['motivo']);

        $afastamento->setSituacao(Afastamento::SITUACAO_AGUARDANDO_DECISAO_DI);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($model_parecer);
        $entityManager->flush();

        if ($model_parecer->getId()) {
            $this->addFlash(
                'success',
                'Parecer do Professor registrado com sucesso.'
            );
        } else {
            $this->addFlash(
                'danger',
                'Erro ao registrar parecer do Professor.'
            );
        }
        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }

    /**
     * @Route("/{id}/encaminhar-afastamento", name="afastamento_encaminhar_afastamento", methods={"POST"})
     * @IsGranted("ROLE_PROFESSOR")
     */
    public function encaminharAfastamento(Request $request, Afastamento $afastamento, MandatoRepository $mandatoRepository, \Swift_Mailer $mailer): Response
    {
        // CONDIÇÕES
        // - Afastamento é internacional
        // - Situação é Iniciado
        // - O usuário logado deve ser um professor (já garantido)
        // - Um professor deve estar exercendo o cargo de chefe do departamento
        // - O usuário logado deve ser o professor chefe do departamento
        if (
            $afastamento->getTipo() !== Afastamento::TIPO_INTERNACIONAL ||
            $afastamento->getSituacao() !== Afastamento::SITUACAO_INICIADO ||
            empty($mandatoRepository->recuperarChefeDepartamento()) ||
            $this->getUser()->getProfessor()->getId() !== $mandatoRepository->recuperarChefeDepartamento()->getId()
        ) {
            throw new \Exception('Ação não permitida');
        }

        $post = $request->get('Afastamento');

        $afastamento->setProfessorRelatorId($post['professor_relator_id']);
        $afastamento->setSituacao(Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($afastamento);
        $entityManager->flush();
        $entityManager->refresh($afastamento);

        // Envia email para o relator
        $mailer->send(
            (new \Swift_Message())
                ->setSubject('SCAP - Você foi convocado a ser o relator de um afastamento')
                ->setFrom(['nao-responda@viniciush4.com' => 'SCAP'])
                ->setTo([$afastamento->getProfessorRelator()->getEmail() => $afastamento->getProfessorRelator()->getNome()])
                ->setBody(
                    $this->renderView(
                        'emails/base.html.twig',
                        [
                            'titulo' => 'Você foi convocado a ser o relator de um afastamento',
                            'mensagem' => 'Você acabou de ser convocado pelo chefe do departamento a ser o relator de um afastamento.',
                            'detalhes' => '<b>Professor Solicitante:</b> '.$afastamento->getProfessorSolicitante()->getNome().' '.$afastamento->getProfessorSolicitante()->getSobrenome()
                        ]
                    ),
                    'text/html'
                )
        );

        $this->addFlash(
            'success',
            'O relator do afastamento foi registrado com sucesso.'
        );

        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }

    /**
     * @Route("/{id}/arquivar", name="afastamento_arquivar", methods={"GET"})
     * @IsGranted("ROLE_SECRETARIO")
     */
    public function arquivar(Request $request, Afastamento $afastamento): Response
    {
        // CONDIÇÕES
        // - Situação é Reprovado ou Cancelado ou (Aprovado-PRPPG e Internacional) ou (Aprovado-DI e Nacional)
		// - O usuário logado deve ser um secretário (já garantido)
        if (
            $afastamento->getSituacao() !== Afastamento::SITUACAO_REPROVADO &&
            $afastamento->getSituacao() !== Afastamento::SITUACAO_CANCELADO &&
            ($afastamento->getSituacao() !== Afastamento::SITUACAO_APROVADO_PRPPG || $afastamento->getTipo() !== Afastamento::TIPO_INTERNACIONAL) &&
            ($afastamento->getSituacao() !== Afastamento::SITUACAO_APROVADO_DI || $afastamento->getTipo() !== Afastamento::TIPO_NACIONAL)
        ) {
            throw new \Exception('Ação não permitida');
        }

        $afastamento->setSituacao(Afastamento::SITUACAO_ARQUIVADO);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($afastamento);
        $entityManager->flush();

        $this->addFlash(
            'success',
            'O afastamento foi arquivado com sucesso.'
        );

        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }

    /**
     * @Route("/{id}/cancelar", name="afastamento_cancelar", methods={"GET"})
     * @IsGranted("ROLE_PROFESSOR")
     */
    public function cancelar(Request $request, Afastamento $afastamento, MandatoRepository $mandatoRepository, \Swift_Mailer $mailer): Response
    {
        // CONDIÇÕES
        // - Situação deve ser diferente de Arquivado e diferente de Reprovado
        // - O usuário logado deve ser um professor (já garantido)
        // - O usuário logado deve ser o professor solicitante
        if (
            in_array($afastamento->getSituacao(), [Afastamento::SITUACAO_ARQUIVADO, Afastamento::SITUACAO_REPROVADO]) ||
            $afastamento->getProfessorSolicitanteId() !== $this->getUser()->getProfessor()->getId()
        ) {
            throw new \Exception('Ação não permitida');
        }

        $afastamento->setSituacao(Afastamento::SITUACAO_CANCELADO);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($afastamento);
        $entityManager->flush();

        // Manda email ao chefe do departamento
        $chefe = $mandatoRepository->recuperarChefeDepartamento();
        if (!empty($chefe)) {
            $mailer->send(
                (new \Swift_Message())
                    ->setSubject('SCAP - Um afastamento foi cancelado')
                    ->setFrom(['nao-responda@viniciush4.com' => 'SCAP'])
                    ->setTo([$chefe->getEmail() => $chefe->getNome()])
                    ->setBody(
                        $this->renderView(
                            'emails/base.html.twig',
                            [
                                'titulo' => 'Um afastamento foi cancelado',
                                'mensagem' => 'Um afastamento acaba de ser cancelado pelo solicitante.',
                                'detalhes' => '<b>Professor Solicitante:</b> '.$afastamento->getProfessorSolicitante()->getNome().' '.$afastamento->getProfessorSolicitante()->getSobrenome()
                            ]
                        ),
                        'text/html'
                    )
            );
        }

        $this->addFlash(
            'success',
            'O afastamento foi cancelado com sucesso.'
        );

        return $this->redirectToRoute('afastamento_show', [
            'id' => $afastamento->getId()
        ]);
    }
}
