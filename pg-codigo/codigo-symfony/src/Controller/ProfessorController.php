<?php

namespace App\Controller;

use App\Entity\Mandato;
use App\Entity\Parentesco;
use App\Entity\Professor;
use App\Entity\Usuario;
use App\Form\ProfessorType;
use App\Repository\MandatoRepository;
use App\Repository\ProfessorRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/professor")
 * @IsGranted("ROLE_SECRETARIO")
 */
class ProfessorController extends Controller
{
    /**
     * @Route("/", name="professor_index", methods={"GET"})
     */
    public function index(ProfessorRepository $professorRepository, MandatoRepository $mandatoRepository): Response
    {
        return $this->render('professor/index.html.twig', [
            'professors' => $professorRepository->findAll(),
            'chefe_departamento' => $mandatoRepository->recuperarChefeDepartamento()
        ]);
    }

    /**
     * @Route("/new", name="professor_new", methods={"GET","POST"})
     */
    public function new(Request $request, ProfessorRepository $professorRepository, \Swift_Mailer $mailer): Response
    {
        $professor = new Professor();
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProfessorType::class, $professor);
        $form->handleRequest($request);

        $entityManager->getConnection()->beginTransaction();

        try {
            if ($form->isSubmitted() && $form->isValid()) {

                $parentescos = $request->get('Parentesco');
                $mandatos = $request->get('Mandato');

                $usuario = new Usuario();
                $usuario->setLogin($professor->getEmail());
                $usuario->setPassword(sha1($professor->getEmail()));
                $entityManager->persist($usuario);

                $professor->setUsuario($usuario);
                $entityManager->persist($professor);

                if (!empty($parentescos)) {
                    foreach ($parentescos as $parentesco) {
                        $model_parentesco = new Parentesco();
                        $model_parentesco->setProfessor($professor);
                        $model_parentesco->setProfessorParente($professorRepository->find($parentesco));
                        $model_parentesco->setParentesco(Parentesco::PARENTESCO_SANGUINEO);
                        $entityManager->persist($model_parentesco);
                    }
                }

                if (!empty($mandatos)) {
                    foreach ($mandatos as $mandato) {
                        $data_inicio = implode('-', array_reverse(explode('/', $mandato['data_inicio'])));
                        $data_fim = implode('-', array_reverse(explode('/', $mandato['data_fim'])));
                        $model_mandato = new Mandato();
                        $model_mandato->setProfessor($professor);
                        $model_mandato->setDataInicio(new DateTime($data_inicio));
                        $model_mandato->setDataFim(new DateTime($data_fim));
                        $entityManager->persist($model_mandato);
                    }
                }

                $entityManager->flush();
                $entityManager->getConnection()->commit();

                // Envia email para o professor
                $mailer->send(
                    (new \Swift_Message())
                        ->setSubject('SCAP - Seja bem-vindo')
                        ->setFrom(['nao-responda@viniciush4.com' => 'SCAP'])
                        ->setTo([$professor->getEmail() => $professor->getNome()])
                        ->setBody(
                            $this->renderView(
                                'emails/base.html.twig',
                                [
                                    'titulo' => 'Seja bem-vindo',
                                    'mensagem' => 'Você foi cadastrado na plataforma SCAP - Sistema de Controle de Afastamento de Professores 
					                    como "Professor". Seus dados de acesso estão listados a seguir (sugerimos que troque sua 
					                    senha assim que entrar no sistema).',
                                    'detalhes' => '<b>Login:</b> '.$professor->getEmail().'<br><b>Senha provisória:</b> '.$professor->getEmail()
                                ]
                            ),
                            'text/html'
                        )
                );

                $this->addFlash(
                    'success',
                    'Professor cadastrado com sucesso'
                );

                return $this->redirectToRoute('professor_index');
            }
        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            $this->addFlash(
                'danger',
                $e->getMessage()
            );
        }

        return $this->render('professor/new.html.twig', [
            'professores' => $professorRepository->findAll(),
            'professor' => $professor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="professor_show", methods={"GET"})
     */
    public function show(Professor $professor): Response
    {
        return $this->render('professor/show.html.twig', [
            'professor' => $professor,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="professor_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Professor $professor, ProfessorRepository $professorRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $form = $this->createForm(ProfessorType::class, $professor);
        $form->handleRequest($request);

        $entityManager->getConnection()->beginTransaction();

        try {
            if ($form->isSubmitted() && $form->isValid()) {

                $parentescos = $request->get('Parentesco');
                $mandatos = $request->get('Mandato');

                $entityManager->createQuery(
                    'DELETE FROM App\Entity\Parentesco p WHERE p.professor_id = :professor_id'
                )->setParameter('professor_id', $professor->getId())->execute();

                $entityManager->createQuery(
                    'DELETE FROM App\Entity\Mandato m WHERE m.professor_id = :professor_id'
                )->setParameter('professor_id', $professor->getId())->execute();

                if (!empty($parentescos)) {
                    foreach ($parentescos as $parentesco) {
                        $model_parentesco = new Parentesco();
                        $model_parentesco->setProfessor($professor);
                        $model_parentesco->setProfessorParente($professorRepository->find($parentesco));
                        $model_parentesco->setParentesco(Parentesco::PARENTESCO_SANGUINEO);
                        $entityManager->persist($model_parentesco);
                    }
                }

                if (!empty($mandatos)) {
                    foreach ($mandatos as $mandato) {
                        $data_inicio = implode('-', array_reverse(explode('/', $mandato['data_inicio'])));
                        $data_fim = implode('-', array_reverse(explode('/', $mandato['data_fim'])));
                        $model_mandato = new Mandato();
                        $model_mandato->setProfessor($professor);
                        $model_mandato->setDataInicio(new DateTime($data_inicio));
                        $model_mandato->setDataFim(new DateTime($data_fim));
                        $entityManager->persist($model_mandato);
                    }
                }

                $this->getDoctrine()->getManager()->flush();
                $entityManager->getConnection()->commit();

                $this->addFlash(
                    'success',
                    'Professor editado com sucesso'
                );

                return $this->redirectToRoute('professor_index');
            }
        } catch (\Exception $e) {
            $entityManager->getConnection()->rollBack();
            $this->addFlash(
                'danger',
                $e->getMessage()
            );
        }

        return $this->render('professor/edit.html.twig', [
            'professores' => $professorRepository->findAll(),
            'professor' => $professor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="professor_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Professor $professor): Response
    {
        if ($this->isCsrfTokenValid('delete'.$professor->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($professor);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Professor removido com sucesso'
            );
        }

        return $this->redirectToRoute('professor_index');
    }
}
