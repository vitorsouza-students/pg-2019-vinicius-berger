<?php

namespace App\Repository;

use App\Entity\Afastamento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * @method Afastamento|null find($id, $lockMode = null, $lockVersion = null)
 * @method Afastamento|null findOneBy(array $criteria, array $orderBy = null)
 * @method Afastamento[]    findAll()
 * @method Afastamento[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AfastamentoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Afastamento::class);
    }

    /**
     * @param int|null $professor_solicitante_id
     * @param string|null $data_solicitacao
     * @return int|mixed|string
     */
    public function buscarAfastamentos(?int $professor_solicitante_id, ?string $data_solicitacao, ?string $tipo, ?string $situacao)
    {
        $qb = $this->createQueryBuilder('a');

        if (!empty($professor_solicitante_id)) {
            $qb->andWhere('a.professor_solicitante_id = :professor_solicitante_id');
            $qb->setParameter('professor_solicitante_id', $professor_solicitante_id);
        }

        if (!empty($data_solicitacao)) {
            $qb->andWhere('a.data_solicitacao = :data_solicitacao');
            $qb->setParameter('data_solicitacao', $data_solicitacao);
        }

        if (!empty($tipo)) {
            $qb->andWhere('a.tipo = :tipo');
            $qb->setParameter('tipo', $tipo);
        }

        if (!empty($situacao)) {
            $qb->andWhere('a.situacao = :situacao');
            $qb->setParameter('situacao', $situacao);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Afastamento[] Returns an array of Afastamento objects
     */
    public function recuperarAfastamentosAtivos()
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.situacao = :situacao')
            ->andWhere("CURRENT_DATE() > DATE_ADD(a.data_solicitacao, 10, 'day')")
            ->setParameter('situacao', 'liberado')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Afastamento[] Returns an array of Afastamento objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Afastamento
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
