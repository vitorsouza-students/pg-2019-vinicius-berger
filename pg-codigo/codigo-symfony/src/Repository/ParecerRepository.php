<?php

namespace App\Repository;

use App\Entity\Parecer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Parecer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Parecer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Parecer[]    findAll()
 * @method Parecer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParecerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Parecer::class);
    }

    // /**
    //  * @return Parecer[] Returns an array of Parecer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Parecer
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
