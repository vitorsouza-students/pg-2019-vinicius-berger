<?php

namespace App\Entity;

use App\Repository\UsuarioRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UsuarioRepository::class)
 */
class Usuario implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

//    /**
//     * @ORM\Column(type="json")
//     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $senha;

    /**
     * @ORM\OneToOne(targetEntity=Secretario::class, mappedBy="usuario", cascade={"persist", "remove"})
     */
    private $secretario;

    /**
     * @ORM\OneToOne(targetEntity=Professor::class, mappedBy="usuario", cascade={"persist", "remove"})
     */
    private $professor;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->login;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        if (!empty($this->professor)) {
            $roles[] = 'ROLE_PROFESSOR';
        }

        if (!empty($this->secretario)) {
            $roles[] = 'ROLE_SECRETARIO';
        }

        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->senha;
        // return '$2y$13$hscGXnh.JkiOQaYt6upJmuhAEAa6Q7oHCfF9RY3ba31w29C4dHoqy';
    }

    public function setPassword(string $password): self
    {
        $this->senha = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getSecretario(): ?Secretario
    {
        return $this->secretario;
    }

    public function setSecretario(Secretario $secretario): self
    {
        $this->secretario = $secretario;

        // set the owning side of the relation if necessary
        if ($secretario->getUsuario() !== $this) {
            $secretario->setUsuario($this);
        }

        return $this;
    }

    public function getProfessor(): ?Professor
    {
        return $this->professor;
    }

    public function setProfessor(Professor $professor): self
    {
        $this->professor = $professor;

        // set the owning side of the relation if necessary
        if ($professor->getUsuario() !== $this) {
            $professor->setUsuario($this);
        }

        return $this;
    }

    public function __toString() {
        return !empty($this->professor) ? $this->professor->getNome() : $this->secretario->getNome();
    }
}
