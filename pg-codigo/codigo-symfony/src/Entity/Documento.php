<?php

namespace App\Entity;

use App\Repository\DocumentoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DocumentoRepository::class)
 */
class Documento
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nome_arquivo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $data_juntada;

    /**
     * @ORM\Column(type="integer")
     */
    private $afastamento_id;

    /**
     * @ORM\ManyToOne(targetEntity=Afastamento::class, inversedBy="documentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $afastamento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getNomeArquivo(): ?string
    {
        return $this->nome_arquivo;
    }

    public function setNomeArquivo(string $nome_arquivo): self
    {
        $this->nome_arquivo = $nome_arquivo;

        return $this;
    }

    public function getDataJuntada(): ?\DateTimeInterface
    {
        return $this->data_juntada;
    }

    public function setDataJuntada(\DateTimeInterface $data_juntada): self
    {
        $this->data_juntada = $data_juntada;

        return $this;
    }

    public function getAfastamentoId(): ?int
    {
        return $this->afastamento_id;
    }

    public function setAfastamentoId(int $afastamento_id): self
    {
        $this->afastamento_id = $afastamento_id;

        return $this;
    }

    public function getAfastamento(): ?Afastamento
    {
        return $this->afastamento;
    }

    public function setAfastamento(?Afastamento $afastamento): self
    {
        $this->afastamento = $afastamento;

        return $this;
    }
}
