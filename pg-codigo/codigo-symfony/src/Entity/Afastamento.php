<?php

namespace App\Entity;

use App\Repository\AfastamentoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AfastamentoRepository::class)
 */
class Afastamento
{
    const TIPO_NACIONAL = 'nacional';
    const TIPO_INTERNACIONAL = 'internacional';

    const ONUS_TOTAL = 'total';
    const ONUS_PARCIAL = 'parcial';
    const ONUS_INEXISTENTE = 'inexistente';

    const SITUACAO_INICIADO = 'iniciado';
	const SITUACAO_AGUARDANDO_PARECER_RELATOR = 'aguardando_parecer_relator';
	const SITUACAO_LIBERADO = 'liberado';
    const SITUACAO_AGUARDANDO_DECISAO_DI = 'aguardando_decisao_di';
	const SITUACAO_APROVADO_DI = 'aprovado_di';
	const SITUACAO_APROVADO_CT = 'aprovado_ct';
	const SITUACAO_APROVADO_PRPPG = 'aprovado_prppg';
	const SITUACAO_ARQUIVADO = 'arquivado';
	const SITUACAO_CANCELADO = 'cancelado';
	const SITUACAO_REPROVADO = 'reprovado';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $data_solicitacao;

    /**
     * @ORM\Column(type="date")
     */
    private $data_inicio_afastamento;

    /**
     * @ORM\Column(type="date")
     */
    private $data_fim_afastamento;

    /**
     * @ORM\Column(type="date")
     */
    private $data_inicio_evento;

    /**
     * @ORM\Column(type="date")
     */
    private $data_fim_evento;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nome_evento;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $situacao;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $onus;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $motivo;

    /**
     * @ORM\Column(type="integer")
     */
    private $professor_solicitante_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $professor_relator_id;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="meusAfastamentos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professorSolicitante;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="afastamentosSouRelator")
     */
    private $professorRelator;

    /**
     * @ORM\OneToMany(targetEntity=Parecer::class, mappedBy="afastamento")
     */
    private $pareceres;

    /**
     * @ORM\OneToMany(targetEntity=Documento::class, mappedBy="afastamento")
     */
    private $documentos;

    public function __construct()
    {
        $this->pareceres = new ArrayCollection();
        $this->documentos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataSolicitacao(): ?\DateTimeInterface
    {
        return $this->data_solicitacao;
    }

    public function setDataSolicitacao(\DateTimeInterface $data_solicitacao): self
    {
        $this->data_solicitacao = $data_solicitacao;

        return $this;
    }

    public function getDataInicioAfastamento(): ?\DateTimeInterface
    {
        return $this->data_inicio_afastamento;
    }

    public function setDataInicioAfastamento(\DateTimeInterface $data_inicio_afastamento): self
    {
        $this->data_inicio_afastamento = $data_inicio_afastamento;

        return $this;
    }

    public function getDataFimAfastamento(): ?\DateTimeInterface
    {
        return $this->data_fim_afastamento;
    }

    public function setDataFimAfastamento(\DateTimeInterface $data_fim_afastamento): self
    {
        $this->data_fim_afastamento = $data_fim_afastamento;

        return $this;
    }

    public function getDataInicioEvento(): ?\DateTimeInterface
    {
        return $this->data_inicio_evento;
    }

    public function setDataInicioEvento(\DateTimeInterface $data_inicio_evento): self
    {
        $this->data_inicio_evento = $data_inicio_evento;

        return $this;
    }

    public function getDataFimEvento(): ?\DateTimeInterface
    {
        return $this->data_fim_evento;
    }

    public function setDataFimEvento(\DateTimeInterface $data_fim_evento): self
    {
        $this->data_fim_evento = $data_fim_evento;

        return $this;
    }

    public function getNomeEvento(): ?string
    {
        return $this->nome_evento;
    }

    public function setNomeEvento(string $nome_evento): self
    {
        $this->nome_evento = $nome_evento;

        return $this;
    }

    public function getSituacao(): ?string
    {
        return $this->situacao;
    }

    public function setSituacao(string $situacao): self
    {
        $this->situacao = $situacao;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getOnus(): ?string
    {
        return $this->onus;
    }

    public function setOnus(string $onus): self
    {
        $this->onus = $onus;

        return $this;
    }

    public function getMotivo(): ?string
    {
        return $this->motivo;
    }

    public function setMotivo(?string $motivo): self
    {
        $this->motivo = $motivo;

        return $this;
    }

    public function getProfessorSolicitanteId(): ?int
    {
        return $this->professor_solicitante_id;
    }

    public function setProfessorSolicitanteId(int $professor_solicitante_id): self
    {
        $this->professor_solicitante_id = $professor_solicitante_id;

        return $this;
    }

    public function getProfessorRelatorId(): ?int
    {
        return $this->professor_relator_id;
    }

    public function setProfessorRelatorId(?int $professor_relator_id): self
    {
        $this->professor_relator_id = $professor_relator_id;

        return $this;
    }

    public function getProfessorSolicitante(): ?Professor
    {
        return $this->professorSolicitante;
    }

    public function setProfessorSolicitante(?Professor $professorSolicitante): self
    {
        $this->professorSolicitante = $professorSolicitante;

        return $this;
    }

    public function getProfessorRelator(): ?Professor
    {
        return $this->professorRelator;
    }

    public function setProfessorRelator(?Professor $professorRelator): self
    {
        $this->professorRelator = $professorRelator;

        return $this;
    }

    /**
     * @return Collection|Parecer[]
     */
    public function getPareceres(): Collection
    {
        return $this->pareceres;
    }

    public function addParecere(Parecer $parecere): self
    {
        if (!$this->pareceres->contains($parecere)) {
            $this->pareceres[] = $parecere;
            $parecere->setAfastamento($this);
        }

        return $this;
    }

    public function removeParecere(Parecer $parecere): self
    {
        if ($this->pareceres->contains($parecere)) {
            $this->pareceres->removeElement($parecere);
            // set the owning side to null (unless already changed)
            if ($parecere->getAfastamento() === $this) {
                $parecere->setAfastamento(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Documento[]
     */
    public function getDocumentos(): Collection
    {
        return $this->documentos;
    }

    public function addDocumento(Documento $documento): self
    {
        if (!$this->documentos->contains($documento)) {
            $this->documentos[] = $documento;
            $documento->setAfastamento($this);
        }

        return $this;
    }

    public function removeDocumento(Documento $documento): self
    {
        if ($this->documentos->contains($documento)) {
            $this->documentos->removeElement($documento);
            // set the owning side to null (unless already changed)
            if ($documento->getAfastamento() === $this) {
                $documento->setAfastamento(null);
            }
        }

        return $this;
    }
}
