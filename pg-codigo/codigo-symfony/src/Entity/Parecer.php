<?php

namespace App\Entity;

use App\Repository\ParecerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParecerRepository::class)
 */
class Parecer
{
    const TIPO_PARECER_FAVORAVEL = 'favoravel';
    const TIPO_PARECER_DESFAVORAVEL = 'desfavoravel';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $data_parecer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo_parecer;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $motivo;

    /**
     * @ORM\Column(type="integer")
     */
    private $afastamento_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $professor_id;

    /**
     * @ORM\ManyToOne(targetEntity=Afastamento::class, inversedBy="pareceres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $afastamento;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class)
     */
    private $professor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataParecer(): ?\DateTimeInterface
    {
        return $this->data_parecer;
    }

    public function setDataParecer(\DateTimeInterface $data_parecer): self
    {
        $this->data_parecer = $data_parecer;

        return $this;
    }

    public function getTipoParecer(): ?string
    {
        return $this->tipo_parecer;
    }

    public function setTipoParecer(string $tipo_parecer): self
    {
        $this->tipo_parecer = $tipo_parecer;

        return $this;
    }

    public function getMotivo(): ?string
    {
        return $this->motivo;
    }

    public function setMotivo(?string $motivo): self
    {
        $this->motivo = $motivo;

        return $this;
    }

    public function getAfastamentoId(): ?int
    {
        return $this->afastamento_id;
    }

    public function setAfastamentoId(int $afastamento_id): self
    {
        $this->afastamento_id = $afastamento_id;

        return $this;
    }

    public function getProfessorId(): ?int
    {
        return $this->professor_id;
    }

    public function setProfessorId(?int $professor_id): self
    {
        $this->professor_id = $professor_id;

        return $this;
    }

    public function getAfastamento(): ?Afastamento
    {
        return $this->afastamento;
    }

    public function setAfastamento(?Afastamento $afastamento): self
    {
        $this->afastamento = $afastamento;

        return $this;
    }

    public function getProfessor(): ?Professor
    {
        return $this->professor;
    }

    public function setProfessor(?Professor $professor): self
    {
        $this->professor = $professor;

        return $this;
    }
}
