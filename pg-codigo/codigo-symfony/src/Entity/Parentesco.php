<?php

namespace App\Entity;

use App\Repository\ParentescoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParentescoRepository::class)
 */
class Parentesco
{
    const PARENTESCO_SANGUINEO = 'sanguineo';
    const PARENTESCO_MATRIMONIAL = 'matrimonial';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $parentesco;

    /**
     * @ORM\Column(type="integer")
     */
    private $professor_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $professor_parente_id;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="meusParentes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professor;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="souParente")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professorParente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParentesco(): ?string
    {
        return $this->parentesco;
    }

    public function setParentesco(string $parentesco): self
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    public function getProfessorId(): ?int
    {
        return $this->professor_id;
    }

    public function setProfessorId(int $professor_id): self
    {
        $this->professor_id = $professor_id;

        return $this;
    }

    public function getProfessorParenteId(): ?int
    {
        return $this->professor_parente_id;
    }

    public function setProfessorParenteId(int $professor_parente_id): self
    {
        $this->professor_parente_id = $professor_parente_id;

        return $this;
    }

    public function getProfessor(): ?Professor
    {
        return $this->professor;
    }

    public function setProfessor(?Professor $professor): self
    {
        $this->professor = $professor;

        return $this;
    }

    public function getProfessorParente(): ?Professor
    {
        return $this->professorParente;
    }

    public function setProfessorParente(?Professor $professorParente): self
    {
        $this->professorParente = $professorParente;

        return $this;
    }
}
