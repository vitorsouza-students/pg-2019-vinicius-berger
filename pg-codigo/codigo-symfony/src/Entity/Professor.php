<?php

namespace App\Entity;

use App\Repository\ProfessorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use function Sodium\add;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ProfessorRepository::class)
 * @UniqueEntity("email")
 */
class Professor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $sobrenome;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $telefone;

    /**
     * @ORM\Column(type="integer")
     */
    private $usuario_id;

    /**
     * @ORM\OneToOne(targetEntity=Usuario::class, inversedBy="professor", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\OneToMany(targetEntity=Parentesco::class, mappedBy="professor", orphanRemoval=true)
     */
    private $meusParentes;

    /**
     * @ORM\OneToMany(targetEntity=Parentesco::class, mappedBy="professorParente", orphanRemoval=true)
     */
    private $souParente;

    /**
     * @ORM\OneToMany(targetEntity=Mandato::class, mappedBy="professor")
     */
    private $mandatos;

    /**
     * @ORM\OneToMany(targetEntity=Afastamento::class, mappedBy="professorSolicitante")
     */
    private $meusAfastamentos;

    /**
     * @ORM\OneToMany(targetEntity=Afastamento::class, mappedBy="professorRelator")
     */
    private $afastamentosSouRelator;

    public function __construct()
    {
        $this->meusParentes = new ArrayCollection();
        $this->souParente = new ArrayCollection();
        $this->mandatos = new ArrayCollection();
        $this->meusAfastamentos = new ArrayCollection();
        $this->afastamentosSouRelator = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getSobrenome(): ?string
    {
        return $this->sobrenome;
    }

    public function setSobrenome(string $sobrenome): self
    {
        $this->sobrenome = $sobrenome;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    public function setTelefone(?string $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }

    public function getUsuarioId(): ?int
    {
        return $this->usuario_id;
    }

    public function setUsuarioId(int $usuario_id): self
    {
        $this->usuario_id = $usuario_id;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * @return Collection|Parentesco[]
     */
    public function getMeusParentes(): Collection
    {
        return $this->meusParentes;
    }

    public function addMeusParente(Parentesco $meusParente): self
    {
        if (!$this->meusParentes->contains($meusParente)) {
            $this->meusParentes[] = $meusParente;
            $meusParente->setProfessor($this);
        }

        return $this;
    }

    public function removeMeusParente(Parentesco $meusParente): self
    {
        if ($this->meusParentes->contains($meusParente)) {
            $this->meusParentes->removeElement($meusParente);
            // set the owning side to null (unless already changed)
            if ($meusParente->getProfessor() === $this) {
                $meusParente->setProfessor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Parentesco[]
     */
    public function getSouParente(): Collection
    {
        return $this->souParente;
    }

    public function addSouParente(Parentesco $souParente): self
    {
        if (!$this->souParente->contains($souParente)) {
            $this->souParente[] = $souParente;
            $souParente->setProfessorParente($this);
        }

        return $this;
    }

    public function removeSouParente(Parentesco $souParente): self
    {
        if ($this->souParente->contains($souParente)) {
            $this->souParente->removeElement($souParente);
            // set the owning side to null (unless already changed)
            if ($souParente->getProfessorParente() === $this) {
                $souParente->setProfessorParente(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Mandato[]
     */
    public function getMandatos(): Collection
    {
        return $this->mandatos;
    }

    public function addMandato(Mandato $mandato): self
    {
        if (!$this->mandatos->contains($mandato)) {
            $this->mandatos[] = $mandato;
            $mandato->setProfessor($this);
        }

        return $this;
    }

    public function removeMandato(Mandato $mandato): self
    {
        if ($this->mandatos->contains($mandato)) {
            $this->mandatos->removeElement($mandato);
            // set the owning side to null (unless already changed)
            if ($mandato->getProfessor() === $this) {
                $mandato->setProfessor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Afastamento[]
     */
    public function getMeusAfastamentos(): Collection
    {
        return $this->meusAfastamentos;
    }

    public function addMeuAfastamento(Afastamento $meuAfastamento): self
    {
        if (!$this->meusAfastamentos->contains($meuAfastamento)) {
            $this->meusAfastamentos[] = $meuAfastamento;
            $meuAfastamento->setProfessorSolicitante($this);
        }

        return $this;
    }

    public function removeMeuAfastamento(Afastamento $meuAfastamento): self
    {
        if ($this->meusAfastamentos->contains($meuAfastamento)) {
            $this->meusAfastamentos->removeElement($meuAfastamento);
            // set the owning side to null (unless already changed)
            if ($meuAfastamento->getProfessorSolicitante() === $this) {
                $meuAfastamento->setProfessorSolicitante(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Afastamento[]
     */
    public function getAfastamentosSouRelator(): Collection
    {
        return $this->afastamentosSouRelator;
    }

    public function addAfastamentoSouRelator(Afastamento $afastamento): self
    {
        if (!$this->afastamentosSouRelator->contains($afastamento)) {
            $this->afastamentosSouRelator[] = $afastamento;
            $afastamento->setAfastamentosSouRelator($this);
        }

        return $this;
    }

    public function removeAfastamentoSouRelator(Afastamento $afastamento): self
    {
        if ($this->afastamentosSouRelator->contains($afastamento)) {
            $this->afastamentosSouRelator->removeElement($afastamento);
            // set the owning side to null (unless already changed)
            if ($afastamento->getAfastamentosSouRelator() === $this) {
                $afastamento->setAfastamentosSouRelator(null);
            }
        }

        return $this;
    }

    public function getmeusParentesProfessores(): ArrayCollection
    {
        $meusParentesProfessores = new ArrayCollection();
        foreach ($this->meusParentes as $parentesco) {
            $meusParentesProfessores->add($parentesco->getProfessorParente());
        }
        return $meusParentesProfessores;
    }
}
