<?php

namespace App\Entity;

use App\Repository\MandatoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MandatoRepository::class)
 */
class Mandato
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $data_inicio;

    /**
     * @ORM\Column(type="date")
     */
    private $data_fim;

    /**
     * @ORM\Column(type="integer")
     */
    private $professor_id;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="mandatos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $professor;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataInicio(): ?\DateTimeInterface
    {
        return $this->data_inicio;
    }

    public function setDataInicio(\DateTimeInterface $data_inicio): self
    {
        $this->data_inicio = $data_inicio;

        return $this;
    }

    public function getDataFim(): ?\DateTimeInterface
    {
        return $this->data_fim;
    }

    public function setDataFim(\DateTimeInterface $data_fim): self
    {
        $this->data_fim = $data_fim;

        return $this;
    }

    public function getProfessorId(): ?int
    {
        return $this->professor_id;
    }

    public function setProfessorId(int $professor_id): self
    {
        $this->professor_id = $professor_id;

        return $this;
    }

    public function getProfessor(): ?Professor
    {
        return $this->professor;
    }

    public function setProfessor(?Professor $professor): self
    {
        $this->professor = $professor;

        return $this;
    }
}
