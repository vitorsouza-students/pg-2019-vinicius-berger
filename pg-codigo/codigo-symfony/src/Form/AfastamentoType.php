<?php

namespace App\Form;

use App\Entity\Afastamento;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AfastamentoType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('data_inicio_afastamento', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('data_fim_afastamento', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('tipo', ChoiceType::class, [
                'choices' => [
                    'Nacional' => 'nacional',
                    'Internacional' => 'internacional'
                ]
            ])
            ->add('onus', ChoiceType::class, [
                'choices' => [
                    'Total' => 'total',
                    'Parcial' => 'parcial',
                    'Inexistente' => 'inexistente'
                ]
            ])
            ->add('motivo')
            ->add('nome_evento')
            ->add('data_inicio_evento', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('data_fim_evento', DateType::class, [
                'widget' => 'single_text',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Afastamento::class,
        ]);
    }
}
