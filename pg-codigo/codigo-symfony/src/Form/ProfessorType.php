<?php

namespace App\Form;

use App\Entity\Parentesco;
use App\Entity\Professor;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfessorType extends AbstractType
{
//    private $entityManager;
//
//    public function __construct(EntityManagerInterface $entityManager)
//    {
//        $this->entityManager = $entityManager;
//    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome')
            ->add('sobrenome')
            ->add('email')
            ->add('telefone');
//            ->add('meusParentesProfessores', EntityType::class, [
//                'class' => Professor::class,
//                'choice_label' => function (?Professor $professor) {
//                    return $professor ? $professor->getNome().' '.$professor->getSobrenome() : '';
//                },
//                'multiple' => true
//            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Professor::class,
        ]);
    }
}
