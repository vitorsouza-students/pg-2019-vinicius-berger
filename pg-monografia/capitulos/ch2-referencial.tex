% ==============================================================================
% TCC - Vinícius Berger
% Capítulo 2 - Referencial Teórico
% ==============================================================================


\chapter{Referencial Teórico}
\label{sec-referencial}

Devido ao grande número de usuários, investimento em pesquisas e o consequente avanço da tecnologia computacional no início dos anos 90, grandes sistemas comerciais começaram a ser desenvolvidos para o ambiente Web. \citeonline{pressman:2011} afirma que, com o passar do tempo, tais sistemas começaram não apenas a oferecer funções especializadas aos usuários, como também foram integrados aos bancos de dados corporativos e às aplicações de negócio, tornando-os ferramentas computacionais sofisticadas. 

Porém, na maioria dos casos, o desenvolvimento desses sistemas ocorreu sem uma abordagem sistemática e sem procedimentos de controle e garantia de qualidade, o que gerou uma crescente e legítima preocupação sobre a maneira como os sistemas baseados na Web eram desenvolvidos e sua qualidade e integridade \cite{murugesan-et-al:2001}.

Considerando que pessoas, negócios e governos dependem cada vez mais de software para tomada de decisões estratégicas e táticas e que a falha de um software pode gerar falhas catastróficas para esses indivíduos \cite{pressman:2011}, é imprescindível que tais aplicações sejam construídas de forma a garantir o máximo de qualidade do produto final.

\citeonline{murugesan-et-al:2001} afirma que para obter maior sucesso no desenvolvimento de aplicações e sistemas baseados na Web, há a necessidade de abordagens disciplinadas e uso de novos métodos e ferramentas (para desenvolvimento, implantação e avaliação de sistemas), que sejam específicos para essa plataforma.

Algumas características do ambiente Web citadas por \citeonline[p.~37]{pressman:2011}, como concorrência, carga imprevisível, disponibilidade, sensibilidade ao conteúdo, evolução dinâmica, imediatismo, segurança e estética, segundo \citeonline[p.~19]{souza:2007} ``imprimiram uma nova dinâmica aos processos já existentes, dando origem a uma nova sub-área da Engenharia de Software, a Engenharia Web''.


\section{Engenharia Web}
\label{sec-engenharia-web}

A \citeonline[p.~67]{ieee:1990} define Engenharia de Software como sendo ``(1) A aplicação de uma abordagem sistemática, disciplinada e quantificável no desenvolvimento, na operação e na manutenção de software; isto é, a aplicação de engenharia ao software. (2) O estudo de abordagens como definido em (1)''.

A Engenharia Web, para \citeonline{pressman:2011}, é uma versão adaptada da Engenharia de Software. \citeonline{murugesan-et-al:2001} afirma que os princípios da Engenharia de Software são incorporados pela Engenharia Web, a qual aplica-os para a natureza mais aberta e flexível da Web, considerando também outros elementos específicos desse ambiente.

\begin{citacao} 
% PARTE RETIRADA DA CITAÇÃO: "e gerenciamento sólidos", pois não entendi o que significa (ou se traduzi de forma errada)
A Engenharia Web se preocupa com o estabelecimento e uso de princípios científicos, de engenharia [...] e abordagens disciplinadas e sistemáticas para o desenvolvimento, implantação e manutenção bem-sucedidos de sistemas e aplicativos de alta qualidade baseados na Web~\cite[p.~4, \textit{tradução nossa}]{murugesan-et-al:2001}. 
\end{citacao}

É importante observar que as definições convergem para o fato de que a Engenharia Web possui uma abordagem disciplinada para o desenvolvimento de um software profissional. ``Disciplina'' para~\citeonline[p.~225]{fernandes-et-al:1996} significa, entre outras coisas, a ``observância de normas ou preceitos''. 

Essa abordagem disciplinada, para \citeonline{pressman:2011}, ocorre quando há a adoção de um \textbf{processo}, um conjunto de \textbf{métodos} (práticas) e um leque de \textbf{ferramentas} que possibilitem o desenvolvimento de software com alto padrão de qualidade.

Nas próximas seções serão apresentadas as etapas de um processo de software genérico descrito por \citeonline{pressman:2011}, um método específico para etapa de Modelagem chamado FrameWeb, proposto por \citeonline{souza:2007} e um tipo de ferramenta utilizada para auxiliar a etapa de Contrução: os \textit{frameworks}. 


\section{Processo de Software}
\label{sec-processo-software}

``Processo é um conjunto de atividades, ações e tarefas realizadas na criação de algum produto de trabalho'' \cite{pressman:2011}.

Quando se fala de Processo, no contexto da Engenharia de Software, \citeonline{pressman:2011} afirma que o mesmo não deve ser considerado uma prescrição rígida de como desenvolver um software, mas sim uma abordagem adaptativa que possibilita à equipe envolvida a escolha de um conjunto de tarefas que sejam mais apropriadas à sua realidade, sempre lembrando que o objetivo é entregar software de qualidade e dentro do prazo.

\begin{citacao}
	Uma metodologia de processo estabelece o alicerce para um processo de engenharia de software completo, por meio da identificação de um pequeno número de atividades estruturais aplicáveis a todos os projetos de software, independentemente de tamanho ou complexidade \cite[p. 40]{pressman:2011}.
\end{citacao}


Segundo \citeonline{pressman:2011}, uma metodologia de processo genérica para engenharia de software compreende cinco atividades:

\begin{enumerate}
	
	\item \textbf{Comunicação}: também chamada de ``Especificação de Requisitos'', é a atividade onde se procura compreender os objetivos das partes interessadas para com o projeto e fazer o levantamento das necessidades que elucidarão as funções e características do software.
	
	\item \textbf{Planejamento}: também chamada de ``Análise'', nesta etapa, com base no que foi levantado na etapa anterior, cria-se um ``mapa'' que norteará as ações da equipe. Esse mapa --- denominado plano de projeto de software --- descreve as tarefas técnicas a serem conduzidas, os riscos prováveis, os recursos que serão necessários, os produtos resultantes a serem produzidos e um cronograma de trabalho.
	
	\item \textbf{Modelagem}: também chamada de ``Projeto'', nesta etapa cria-se um ``esboço'' do sistema, de modo que se possa ter uma ideia do todo. São produzidos modelos para melhor entender as necessidades do software e o projeto que irá atender a essas necessidades.
	
	\item \textbf{Construção}: também chamada de ``Implementação'', essa atividade combina geração de código (manual ou automatizada) e testes necessários para revelar erros na codificação.
	
	\item \textbf{Emprego}: também chamada de ``Implantação'', é onde o software (como uma entidade completa ou como um incremento parcialmente efetivado) é entregue ao cliente, que avaliará o produto e fornecerá \textit{feedback}.
	
\end{enumerate}

Segundo \citeonline{pressman:2011}, as atividades descritas acima podem ser aplicadas iterativamente, conforme o projeto se desenvolve, produzindo, a cada iteração, um incremento no software. Assim, cada incremento disponibilizará uma parte dos recursos e funcionalidades, deixando o software cada vez mais completo.


\section{O Método FrameWeb}
\label{sec-frameweb}

``FrameWeb é um método de projeto para construção de sistemas de informação Web (\textit{Web Information Systems} – WISs) baseado em \textit{frameworks}"~\cite[p.~71]{souza:2007}.

De acordo com~\citeonline{souza:2007}, as principais propostas do método são a definição de uma arquitetura lógica baseada no padrão arquitetônico \textit{Service Layer} (Camada de Serviço) (Figura~\ref{fig-padrao-camada-servico}) e a utilização de uma linguagem de modelagem específica para a produção de quatro tipos de modelos de projeto:

\begin{itemize}
	\item \textbf{Modelo de Entidades:} inicialmente chamado de ``Modelo de Domínio'', o Modelo de Entidades ``é um diagrama de classes da UML que representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados relacional''~\cite[p.~78]{souza:2007}. Ele ajusta o modelo de classes (construído na etapa anterior do processo) para a plataforma de implementação escolhida, indicando os tipos de dados de cada atributo e a navegabilidade das associações. Além disso, utiliza mecanismos leves de extensão da UML (como estereótipos e restrições) para adicionar meta-dados às classes de domínio (mapeamentos de persistência) que permitam aos \textit{frameworks} ORM (Seção \ref{sec-mapeamento-objeto-relacional}) transformar objetos que estão na memória em linhas de tabelas no banco de dados relacional e vice-versa. ``Apesar de tais configurações serem relacionadas mais à persistência do que ao domínio, elas são representadas no Modelo de Domínio [Entidades] porque as classes que são mapeadas e seus atributos são exibidas neste diagrama''~\cite{souza:2007}. O Modelo de Entidades serve de base para a implementação das classes residentes no pacote ``Domínio'' do padrão arquitetônico sugerido pelo FrameWeb (Figura~\ref{fig-padrao-camada-servico}).
	
	\item \textbf{Modelo de Persistência:} ``o Modelo de Persistência é um diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela persistência das instâncias das classes de domínio''~\cite[p.~81]{souza:2007}. Ele apresenta uma interface e uma classe concreta (que implementa a interface) para cada classe de domínio que precisa de lógica de acesso a dados. A interface define os métodos de persistência básicos e as operações de consultas específicas necessárias para atender aos casos de uso do sistema. Pode-se criar uma interface e uma classe concreta base (com métodos comuns à todas as outras) para que o modelo não fique ``poluído'' com métodos repetidos. ``Automaticamente, todas as interfaces DAO de todos os diagramas herdam as definições da interface base, ocorrendo o mesmo com as implementações concretas de cada tecnologia de persistência, sem que isso precise estar explícito no diagrama''~\cite{souza:2007}. Como pôde ser observado, o método FrameWeb indica a utilização do padrão de projeto DAO para a construção da camada de acesso a dados.
	
	\item \textbf{Modelo de Navegação:} ``o Modelo de Navegação é um diagrama de classes da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e classes de ação do \textit{framework Front Controller}''~\cite[p.~84]{souza:2007}. A classe de ação é o principal componente do modelo e suas associações de dependência regem o fluxo da aplicação, permitindo identificar como os dados são submetidos e quais resultados são esperados a partir de uma certa entrada de dados. O projetista pode criar uma classe de ação para cada caso de uso ou agrupar casos de uso em uma mesma classe de ação. Também fica a cargo do projetista definir se deve representar várias ações em um mesmo diagrama ou se deve ter um diagrama para cada ação \cite{souza:2007}.
	
	\item \textbf{Modelo de Aplicação:} ``o Modelo de Aplicação é um diagrama de classes da UML que representa as classes de serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências''~\cite[p.~89]{souza:2007}. A granularidade das classes de serviço deve ser definida pelo projetista com base nos casos de uso modelados na fase anterior do processo. Assim como no Modelo de Persistência, deve-se usar as regras de programação por interfaces: cada classe de serviço deve ter uma interface (com a definição dos métodos que executam a lógica de negócio) e uma implementação concreta dessa interface \cite{souza:2007}. Esse diagrama mostra também as associações entre as classes de serviço e os DAOs necessários para a execução dos casos de uso.

\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/fig-padrao-camada-servico.pdf} 
	\caption{Arquitetura padrão proposta pelo FrameWeb. Adaptado de~\cite{souza:2007}.}
	\label{fig-padrao-camada-servico}
\end{figure}

Esses quatro modelos, segundo~\citeonline{souza:2007}, trazem conceitos utilizados pelos \textit{frameworks} para a fase de Projeto por meio da criação de um perfil UML (a linguagem de modelagem define extensões leves ao meta-modelo da UML para representar componentes típicos da plataforma Web e dos \textit{frameworks} utilizados) que faz com que os diagramas fiquem mais próximos da implementação. Dessa forma, ``a fase de codificação é facilitada pelo uso dos \textit{frameworks}, especialmente porque os modelos de projeto exibem componentes relacionados a eles'' \cite[p.~72]{souza:2007}.

Em uma publicação mais recente, \citeonline{souza-celebratingfalbo20} resume os avanços incorporados ao método FrameWeb desde sua proposta inicial, detalhada em~\cite{souza:2007}.

% Acho que este parágrafo não adiciona muito. Já se falou do ORM e do DAO anteriormente e ultimamente temos tentado colocar FrameWeb como método de projeto apenas, desvinculando com o restante do processo.
%Para a aplicação do método, segundo~\citeonline{souza:2007}, é esperado que sejam construídos diagramas de casos de uso e de classes de domínio (ou similares) nas etapas anteriores do processo de software. Além disso, espera-se a utilização de um \textit{framework} de mapeamento objeto/relacional (ORM) por meio do padrão de projeto \textit{Data Access Object} (DAO). Por fim, é sugerido que os princípios de agilidade sejam seguidos, em especial os propostos pela Modelagem Ágil. 


\section{\textit{Frameworks}}
\label{sec-frameworks}

De acordo com \citeonline[p.~42]{souza:2007}, ``um \textit{framework} é visto como um artefato de código que provê componentes prontos que podem ser reutilizados mediante configuração, composição ou herança''. \citeonline{souza:2007} ainda afirma que o uso combinado de alguns desses \textit{frameworks} permite a construção de sistemas Web de grande porte sem muito esforço de codificação.

Segundo \citeonline{gabrieli-et-al:2007}, \textit{frameworks} são infra-estruturas de aplicações projetadas para serem reutilizadas. Portanto, um \textit{framework} é uma arquitetura semi-pronta de aplicação e representa o nível mais alto de reutilização~\cite{cunha-et-al:2003}. A redução da manutenção e a maximização do reúso são dois dos ganhos conseguidos com a utilização dos \textit{frameworks}~\cite{gamma-et-al:2000}.

``As \textbf{ferramentas} da engenharia de software fornecem suporte automatizado ou semiautomatizado para o processo e para os métodos'' \cite[p.~40]{pressman:2011}. Considerando a metodologia de processo genérica descrita na Seção \ref{sec-processo-software} e as definições anteriores, pode-se dizer que os \textit{frameworks} são \textbf{ferramentas} que auxiliam a atividade de Construção. 


\subsection{\textit{Frameworks} MVC}
\label{sec-frameworks-mvc}

MVC é a abreviatura de Modelo-Visão-Controlador (\textit{Model-View-Controller}). O padrão MVC, desenvolvido no final dos anos 70 para uso na plataforma Smalltalk, ``divide a interação da interface com o usuário em três papéis distintos'' \cite[p.~330, \textit{tradução nossa}]{fowler:2002}.

De acordo com \citeonline{fowler:2002}, o Modelo é um objeto que representa alguma informação sobre o domínio do problema, contendo todos os dados e comportamentos que não estejam relacionados com a interface de usuário. A Visão representa a exibição do Modelo na interface com o usuário, sendo este seu papel específico: exibição de informações; qualquer necessidade de alteração nos dados do Modelo é tratada pelo terceiro membro da tríade MVC: o Controlador.

O funcionamento do padrão MVC é apresentado na Figura \ref{fig-padrao-mvc}. O Controlador recebe a entrada do usuário por meio de um estímulo vindo da Visão, manipula o Modelo e, se necessário, seleciona um elemento de Visão para exibir o resultado. O Modelo notifica à Visão as alterações que ocorreram em sua estrutura. Dessa forma, a Visão fica ciente da necessidade de consultar novamente o estado do Modelo \cite{souza:2007}. Segundo \citeonline{fowler:2002} é provável que existam diversas Visões de um Modelo em uma mesma tela em interfaces de usuário mais complexas ou ``ricas''. Dessa forma, se o usuário fizer uma alteração no Modelo por meio de uma Visão, as outras precisam refletir essa mudança (para manter a consistência das informações apresentadas). Segundo \citeonline{fowler:2002}, para se fazer isso sem criar uma dependência, geralmente é implementado o padrão \textit{Observer}, onde a Visão é o \textit{listener} (ouvinte) dos eventos emitidos pelo Modelo.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/fig-mvc.pdf} 
	\caption{Funcionamento do padrão MVC. Adaptado de~\cite{souza:2007}.}
	\label{fig-padrao-mvc}
\end{figure}

A arquitetura MVC veio ao encontro das necessidades dos aplicativos Web \cite{souza:2007}. Porém, um observador mais rigoroso notará que ela não se encaixa perfeitamente nessa plataforma, pois o Modelo, situado no servidor Web, não consegue notificar a Visão sobre alterações, já que esta última se encontra no navegador do cliente e a navegação sempre é realizada no sentido cliente-servidor \cite{souza:2007}. Dessa forma, o nome correto para esse padrão arquitetônico quando aplicado à Web, é ``Controlador Frontal'' (\textit{Front Controller}) \cite[p.~43 apud ALUR et al., 2003]{souza:2007}.

De acordo com \citeonline[p.~344]{fowler:2002}, Controlador Frontal é ``um controlador que lida com todas as solicitações de um site''. Seu funcionamento é ilustrado na Figura \ref{fig-padrao-front-controller}: o servidor Web entrega toda requisição vinda do navegador do cliente para o Controlador Frontal, o qual analisa a solicitação para decidir que tipo de ação iniciar, cria uma instância de uma classe de ação específica e, em seguida, delega a esse objeto a execução do serviço. Por fim, o Controlador Frontal recebe o resultado do processamento e decide que tipo de resposta enviar para o navegador, que pode ser a construção de uma página, redirecionamento a outra página, montagem e exibição de um relatório, dentre várias possibilidades \cite{souza:2007}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/fig-front-controller.pdf} 
	\caption{Funcionamento do Controlador Frontal. Adaptado de~\cite{fowler:2002}.}
	\label{fig-padrao-front-controller}
\end{figure}

\textit{Frameworks} MVC implementam exatamente essa arquitetura, deixando a cargo do desenvolvedor a tarefa de configurar o \textit{framework}, escrever as classes de ação e as páginas da Web que serão retornadas como resultado. Além disso, esse tipo de \textit{framework} também fornece uma série de outras facilidades, como conversão automática de tipos, validação de dados de entrada e internacionalização de páginas Web \cite{souza:2007}.

\subsection{\textit{Frameworks} ORM}
\label{sec-mapeamento-objeto-relacional}

Segundo \citeonline{souza:2007}, Sistemas de Bancos de Dados Relacionais (SGBDRs) há décadas têm sido o padrão de fato para persistência de dados e não há indícios de que este cenário vá mudar tão cedo. \citeonline{bauer-king:2005} dizem que a tecnologia relacional é profundamente conhecida e isso por si só é motivo suficiente para que muitas organizações a escolham. ``Os bancos de dados relacionais estão tão arraigados não é por acidente, mas porque são uma abordagem incrivelmente flexível e robusta para gerenciamento de dados''~\cite[p.~3]{bauer-king:2005}. Consequentemente, ``mesmo aplicações desenvolvidas no paradigma orientado a objetos (OO) têm utilizado bancos de dados relacionais para persistência de seus objetos'' \cite[p.~46]{souza:2007}.

Porém existe um pequeno problema ao unir o paradigma de programação orientado a objetos com o mundo dos bancos de dados relacionais: as operações SQL (linguagem utilizada para manipular os dados em um banco de dados relacional), como projeção e junção, sempre resultam em uma representação tabular dos dados, o que é bem diferente do grafo de objetos interconectados usado para executar a lógica de negócios em um aplicativo que foi construído usando OO \cite{bauer-king:2005}. ``Esses são modelos fundamentalmente diferentes, não apenas maneiras diferentes de visualizar o mesmo modelo'' \cite[p.~7, \textit{tradução nossa}]{bauer-king:2005}. Existe, portanto, uma incompatibilidade de paradigmas.

Para solucionar esse problema surgiu o ``Mapeamento Objeto/Relacional'', ou ORM. \citeonline{bauer-king:2005} utilizam a barra entre as palavras ``Objeto'' e ``Relacional'' para enfatizar o problema de incompatibilidade que ocorre quando os dois mundos colidem.  Os mesmos autores definem ORM como sendo a persistência automatizada (e transparente) de objetos em um aplicativo (construído usando OO) para as tabelas em um banco de dados relacional, usando metadados que descrevem o mapeamento entre os objetos e o banco de dados. Assim, pode-se dizer que, em essência, o ORM funciona transformando dados de uma representação para outra. 

Os \textit{frameworks} ORM, que são responsáveis por trazer, de forma simplificada, o benefício do mapeamento Objeto/Relacional para dentro do projeto, esperam que o programador informe, por meio de metadados, como deve ser feita a transformação dos objetos e seus atributos em tabelas e colunas do SGBDR. Em contrapartida, disponibilizam métodos simples como salvar, excluir e recuperar, além de uma linguagem de consulta, similar à SQL, porém orientada a objetos, para que consultas mais elaboradas possam ser realizadas com igual facilidade \cite{souza:2007}.

\citeonline{fowler:2002}, no capítulo ``\textit{Data Source Architectural Patterns}'' (Padrões de Arquitetura de Fonte de Dados) explica alguns padrões comumente utilizados por ferramentas de persistência de objetos em ORM. Segundo o autor, em um projeto inicial para um Modelo de Domínio, a escolha principal recai entre dois padrões: o \textit{Active Record} e o \textit{Data Mapper}, os quais serão apresentados a seguir.


\subsubsection{Padrão \textit{Active Record}}

Segundo ~\citeonline{fowler:2002}, no padrão \textit{Active Record} as classes de domínio correspondem muito de perto à estrutura de registro do banco de dados, ou seja, cada classe do domínio representa uma tabela (ou uma \textit{view}) do banco, cujas colunas são representadas por propriedades na classe. Uma instância de uma classe (objeto) representa um único registro em uma tabela. 

Ainda segundo \citeonline{fowler:2002}, esse padrão coloca a lógica de acesso a dados nas classes do domínio (elas podem herdar os métodos de uma classe \textit{Active Record} se for mais conveniente) de forma que cada objeto sabe como ler e gravar seus próprios dados no banco de dados. Além disso, cada classe do domínio é responsável por qualquer lógica de negócio que atue sobre os dados.

A Figura \ref{fig-padrao-active-record} foi adaptada de \citeonline{fowler:2002} e exemplifica o padrão \textit{Active Record}. É possível ver uma classe ``Pessoa'' com três atributos persistentes (nome, sobrenome e numeroDeDependentes), três métodos de lógica de acesso a dados (inserir, atualizar e excluir) e três métodos da lógica de negócios (recuperarIsencao, estaMarcadoParaAuditoria e recuperarRendimentosTributaveis) em um relacionamento de dependência com o banco de dados. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{figuras/fig-active-record.pdf} 
	\caption{Padrão Active Record. Adaptado de~\cite{fowler:2002}.}
	\label{fig-padrao-active-record}
\end{figure}

A classe \textit{Active Record} normalmente tem métodos que fazem o seguinte \cite[p.~161, \textit{tradução nossa}]{fowler:2002}:

\begin{itemize}
	\item Construir uma instância do \textit{Active Record} a partir de uma linha de conjunto de resultados SQL;
	\item Construir uma nova instância para posterior inserção na tabela;
	\item Métodos de localização estáticos para envolver consultas SQL comumente usadas e retornar objetos \textit{Active Record};
	\item Atualizar o banco de dados e inserir nele os dados do \textit{Active Record};
	\item Obter e definir os campos;
	\item Implementar algumas peças da lógica de negócios.
\end{itemize}

Segundo \citeonline{fowler:2002}, o padrão \textit{Active Record} é uma boa escolha para a lógica de domínio que não seja muito complexa, como um sistema que seja fortemente baseado em CRUD (operações básicas de cadastro, do inglês \textit{Create, Retrieve, Update, Delete}). A principal vantagem é a simplicidade de implementação e compreensão do padrão. 

O principal problema relacionado ao seu uso, segundo \citeonline{fowler:2002}, é que esse padrão funciona bem somente se os objetos do tipo \textit{Active Record} corresponderem diretamente às tabelas no banco de dados em um esquema isomórfico (se há um mapeamento bijetivo entre elas, ou seja, cada propriedade em um objeto tem sua respectiva coluna na tabela e vice versa). 

Outro argumento citado por \citeonline{fowler:2002} contra o uso do \textit{Active Record} é o fato de que ele acopla o projeto dos objetos ao projeto do banco de dados, tornando mais difícil refatorar o sistema ao longo do tempo.



\subsubsection{Padrão \textit{Data Mapper}}

Como já foi mencionado no início da Seção~\ref{sec-frameworks}, objetos e bancos de dados relacionais usam mecanismos diferentes para estruturar os dados, de forma que alguns conceitos do mundo dos objetos (como coleções e herança) não existem nesses bancos. Porém, o uso desses conceitos é muito valioso para organizar os dados em aplicações cuja lógica de negócios é extensa \cite{fowler:2002}. O padrão \textit{Active Record}, explicado anteriormente, funciona bem quando há uma correspondência direta entre os atributos dos objetos e as colunas das tabelas relacionadas, porém quando se quer evoluir o modelo dos objetos de forma independente do esquema do banco de dados (e vice versa), o padrão \textit{Data Mapper} é mais indicado \cite{fowler:2002}.

O padrão \textit{Data Mapper} cria uma camada de ``Mapeadores'' que movem os dados entre os objetos e o banco de dados, mantendo-os independentes um do outro \cite{fowler:2002}. ``Com o \textit{Data Mapper}, os objetos na memória não precisam nem mesmo saber que há um banco de dados presente. Eles não precisam conter comandos SQL e certamente não precisam ter nenhum conhecimento do esquema do banco de dados'' \cite[p.~165, \textit{tradução nossa}]{fowler:2002}. \citeonline{fowler:2002} ainda afirma que isso ajuda na etapa de codificação pois é possível trabalhar com os objetos do domínio sem ter que entender como eles estão armazenados no banco de dados. 

A Figura \ref{fig-padrao-data-mapper} foi adaptada de \citeonline{fowler:2002} e exemplifica o padrão \textit{Data Mapper}. Comparando-a com a Figura \ref{fig-padrao-active-record} pode-se ver que as operações referentes à lógica de acesso a dados foram movidas para uma camada intermediária (Mapeador Pessoa) a qual tem relacionamentos de dependência com o banco de dados e com a classe de domínio em substituição àquele relacionamento direto que existia entre estes dois.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/fig-data-mapper.pdf} 
	\caption{Padrão Data Mapper. Adaptado de~\cite{fowler:2002}.}
	\label{fig-padrao-data-mapper}
\end{figure}

Segundo \citeonline{fowler:2002}, o ``preço'' que se paga pelo uso do \textit{Data Mapper} em relação ao \textit{Active Record} é a implementação dessa camada adicional, de forma que optar por um ou outro padrão vai depender da complexidade da lógica de negócio. ``Se o modelo do domínio for bastante simples e o banco de dados estiver sob o controle do desenvolvedor do modelo do domínio, então é razoável que os objetos do domínio acessem diretamente o banco de dados por meio do \textit{Active Record}'' \cite[p.~170, \textit{tradução nossa}]{fowler:2002}.
