% ==============================================================================
% TCC - Vinícius Berger
% Capítulo 3 - Especificação de Requisitos
% ==============================================================================
\chapter{Especificação de Requisitos e Análise}
\label{sec-requisitos}

Como já foi mencionado na Introdução (Capítulo~\ref{sec-intro}), o objetivo deste trabalho é realizar um novo projeto e uma nova implementação do SCAP, um sistema Web criado para gerenciar o afastamento dos professores do Departamento de Informática (DI) da UFES. \citeonline{duarte-pg14} e \citeonline{prado-pg15} já realizaram todo o levantamento dos requisitos e análise do SCAP, os quais serão apresentados neste capítulo, juntamente com algumas propostas de melhorias. 

\section{Descrição do Escopo}

As solicitações de afastamento gerenciadas pelo SCAP podem ser para participação em eventos (cursos, palestras, \textit{workshops}, seminários, entre outros) realizados no Brasil ou no exterior. De forma resumida, a tramitação das solicitações feitas por professores do DI ocorre da seguinte forma:

\begin{itemize}
	\item Para os eventos realizados \textbf{em solo nacional}, a solicitação de afastamento precisa ser aprovada pelo DI. Assim, a solicitação deve ser enviada a todos os professores do DI via email. Caso ninguém se manifeste contra o afastamento dentro de dez dias, a solicitação é aprovada. Se houver uma manifestação contrária ao afastamento, o caso é discutido em uma reunião do DI, na qual decide-se pelo deferimento ou indeferimento. Dessa forma, o processo ocorre inteiramente dentro do departamento.
	
	\item Para os eventos realizados \textbf{fora do Brasil}, a solicitação de afastamento precisa ser aprovada pelo DI, pelo Centro Tecnológico (CT) e pela Pró-Reitoria de Pesquisa e Pós-Graduação (PRPPG). Neste caso, o Chefe do Departamento, ao ter ciência de uma nova solicitação de afastamento, escolhe um professor (que não tenha parentesco com o solicitante) para ser o relator da solicitação, o qual deve emitir um parecer recomendando a aprovação ou reprovação do afastamento no âmbito do DI. Independentemente da recomendação do relator, o processo deve aguardar dez dias, que é o prazo para que outros professores se manifestem contra ele, exatamente como no caso anterior. Se houver uma manifestação contrária ao afastamento, o caso é discutido em uma reunião do DI, na qual decide-se pelo deferimento ou indeferimento da solicitação no âmbito do DI. Se não houver manifestação contrária, a solicitação é considerada aprovada pelo DI e é então encaminhada ao CT e posteriormente à PRPPG. O afastamento só é deferido se a solicitação for aprovada em todas as instâncias.
\end{itemize}

O SCAP foi idealizado com o objetivo de assistir os professores e secretários do DI na tramitação das solicitações de afastamento, tornando mais simples todo o processo, tanto a criação da solicitação quanto a análise e armazenamento da mesma. O sistema atinge esse objetivo através do envio automático de emails aos envolvidos e da utilização de formulários para inserção e atualização das informações.

O escopo do sistema é limitado aos procedimentos realizados no DI.  Não existe integração com os processos do CT e da PRPPG. Dessa forma, os pareceres do CT e da PRPPG são inseridos manualmente no SCAP.


\section{Modelo de Casos de Uso}

A Tabela~\ref{tbl-atores-scap} lista e descreve os atores identificados por \citeonline{duarte-pg14} no levantamento de requisitos.

\begin{table}[h]
	\caption{Atores do SCAP.}
	\label{tbl-atores-scap}
	\centering\def\tabularxcolumn#1{m{#1}}\def\arraystretch{1.2}
	\begin{tabularx}{0.8\textwidth}{ 
		| >{\hsize=0.7\hsize}X | 
		>{\hsize=1.3\hsize}X |
	}
	\hline
		\textbf{Ator} & 
		\textbf{Descrição} \\
	\hline
		Professor & 
		Professores efetivos do DI \\ 
	\hline 
		Chefe do Departamento & 
		Professores do DI que estão exercendo a função administrativa de ``Chefe do Departamento'' ou ``Subchefe do Departamento''\\ 
	\hline 
		Secretário & 
		Secretários do DI \\ 
	\hline 
	\end{tabularx}
\end{table}

O \textbf{Secretário} lida com a parte administrativa do sistema, sendo sua responsabilidade cadastrar novos professores e manter esses cadastros atualizados, principalmente os relacionamentos de parentesco entre professores e os mandatos daqueles que forem (ou tiverem sido) chefe ou subchefe do departamento. Além disso, é responsável por cadastrar as decisões do CT e da PRPPG (quando for um afastamento internacional) e por arquivar os processos de solicitação de afastamento já finalizados.

O \textbf{Professor} é o ator principal do sistema. Ele realiza solicitações de afastamento e, se julgar necessário e ainda houver tempo hábil, pode se manifestar contra um afastamento de outro professor. Se for escolhido como relator de um afastamento internacional, deve recomendar ao DI aprovar ou reprovar tal afastamento. 

O \textbf{Chefe do Departamento} tem a responsabilidade de escolher um relator para cada afastamento internacional que for cadastrado no sistema. 

A Figura~\ref{fig-casos-uso} apresenta todos os casos de uso do SCAP. É possível ver os três atores citados anteriormente, cada qual relacionado com os casos de uso de sua competência. Em seguida é apresentada uma listagem com uma breve descrição de cada caso de uso. Vale ressaltar que originalmente o SCAP foi dividido em dois subsistemas. Segundo \citeonline{prado-pg15}, tal divisão foi realizada para facilitar a implementação do sistema e não devido a natureza das classes criadas. Neste trabalho, no entanto, optou-se por não utilizar esta subdivisão, pois não foi identificada tal facilidade neste contexto.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/fig-casos-uso.pdf} 
	\caption{Diagrama de Casos de Uso do SCAP.}
	\label{fig-casos-uso}
\end{figure}

\begin{itemize}
	\item \textbf{Solicitar afastamento}: um professor realiza uma solicitação de afastamento no sistema, informando todos os dados necessários para a tramitação da mesma;
	\item \textbf{Cancelar afastamento}: o professor cancela uma solicitação de afastamento da qual seja o solicitante;
	\item \textbf{Encaminhar afastamento}: o chefe do departamento, após analisar um pedido de afastamento internacional recém-cadastrado, escolhe um professor (que não tenha relação de parentesco com o solicitante) como relator do afastamento;
	\item \textbf{\st{Deferir parecer} Registrar parecer relator}: o professor relator de um afastamento internacional cadastra seu parecer sobre o mesmo;
	\item \textbf{Consultar afastamento}: um professor ou um secretário consulta uma lista de afastamentos cadastrados ou fornece características (dados) de um certo afastamento a fim de localizá-lo no sistema;
	\item \textbf{Manifestar-se contra afastamento}: um professor cadastra um parecer desfavorável a um afastamento nacional. Nesse caso, o deferimento ou indeferimento do afastamento é decidido em uma reunião do DI;
	\item \textbf{Cadastrar usuário}: um secretário cadastra um novo professor ou um novo secretário no sistema, informando os dados pessoais necessários;
	\item \textbf{Cadastrar chefe do departamento}: um secretário insere um mandato no cadastro do professor correspondente (que é o chefe do departamento naquele mandato), informando as datas de início e fim do mesmo;
	\item \textbf{Registrar decisão DI}: um secretário registra a decisão do Departamento de Informática sobre um pedido de afastamento, após a realização de uma reunião, nos casos em que um professor se manifesta contra o afastamento de outro;
	\item \textbf{\st{Registrar parecer CT} Registrar decisão CT}: um secretário registra a decisão do Centro Tecnológico sobre um pedido de afastamento internacional;
	\item \textbf{\st{Registrar parecer PRPPG} Registrar decisão PRPPG}: um secretário registra a decisão da Pró-Reitoria de Pesquisa e Pós-Graduação sobre um pedido de afastamento internacional;
	\item \textbf{Arquivar afastamento}: um secretário arquiva uma solicitação de afastamento que já está encerrada, ou seja, já está deferida ou indeferida.
\end{itemize}

O caso de uso ``Deferir parecer'' foi renomeado para ``Registrar parecer relator''. Isso porque o termo ``deferir''  significa, entre outras coisas, ``conceder; atender; despachar (um requerimento) em conformidade com o que nele se pede; ter acatamento'' \cite{fernandes-et-al:1996}. No meio jurídico, quando se está falando de processos, o termo ``deferido'' é usado por certas autoridades nos requerimentos que lhes são dirigidos, quando concedem o que lhes foi pedido \cite{fernandes-et-al:1996}. Considerando que a descrição do caso de uso em \citeonline{duarte-pg14} indica que o parecer, nesse caso, também pode ser desfavorável, usar o termo ``deferir'' pode gerar certa confusão. Assim, para eliminar qualquer ambiguidade, será usada a expressão ``Registrar parecer relator'', mantendo o mesmo padrão de nomes de outros casos de uso e deixando bem claro, inclusive, que esse caso de uso trata exclusivamente de um parecer de um relator.

Os casos de uso ``Registrar parecer CT'' e ``Registrar parecer PRPPG'' foram renomeados respectivamente para ``Registrar decisão CT'' e ``Registrar decisão PRPPG'' para que não haja o falso entendimento de que esses casos de uso geram Pareceres no sistema. O que de fato ocorre é a mudança da situação do Afastamento e a inclusão (\textit{upload}) de um Documento (um arquivo contendo a decisão do CT ou da PRPPG).

Além disso, foi acrescentado um novo caso de uso para o ator ``Secretário'': ``Registrar decisão DI''. De acordo com o Documento de Especificação de Requisitos produzido por \citeonline{duarte-pg14} e complementado \citeonline{prado-pg15}, quando um professor se manifesta contra uma solicitação de afastamento nacional, o deferimento ou indeferimento da mesma depende de uma reunião do DI. Após essa reunião um secretário deve registrar a decisão do DI no sistema. Porém, essa ação do secretário não foi listada como um caso de uso separado, mas foi embutida no caso de uso ``Manifestar-se contra afastamento'', que é de responsabilidade de um professor. Para deixar explícita essa ação do secretário, criou-se o caso de uso ``Registrar decisão DI''. A descrição do caso de uso e suas precondições são exibidas na Tabela~\ref{tbl-caso-uso-registrar-decisao-di}.

\begin{table}[h]
	\caption{Caso de uso ``Registrar decisão DI''.}
	\label{tbl-caso-uso-registrar-decisao-di}
	\centering\def\tabularxcolumn#1{m{#1}}\def\arraystretch{1.5}
	\begin{tabularx}{0.8\textwidth}{ 
			| >{\hsize=1\hsize}X | 
			  >{\hsize=1\hsize}X |
			  >{\hsize=1\hsize}X |
		}
		\hline
		\multicolumn{3}{|>{\hsize=\dimexpr3\hsize+4\tabcolsep+2\arrayrulewidth\relax}X|}
		{
			\textbf{Caso de uso:} Registrar decisão DI.
			
			\textbf{Descrição sucinta:} Um secretário registra a decisão do DI quando houve uma manifestação contrária à um afastamento.
		} 
		\\
		\hline
		\textbf{Nome do fluxo de eventos normal} 
		& 
		\textbf{Precondições} 
		&
		\textbf{Descrição} 
		\\
		\hline
		Registrar decisão DI 
		& 
		1 - Uma solicitação de afastamento nacional deve ter sido criada.
		
		2 - A situação do afastamento deve ser ``Aguardando decisão DI''.
		
		3 - Algum professor deve ter se manifestado contra o afastamento.
		&
		1 - Um secretário, após a reunião do DI, muda a situação do afastamento e faz \textit{upload} do documento contendo a decisão do DI (geralmente um trecho da ata da reunião).
		\\ 
		\hline
		\multicolumn{3}{|>{\hsize=\dimexpr3\hsize+4\tabcolsep+2\arrayrulewidth\relax}X|}
		{
			\textbf{Requisitos relacionados:} RF09.
			
			\textbf{Classes relacionadas:} Secretário, Afastamento, Parecer.
		} 
		\\
		\hline
	\end{tabularx}
\end{table}


\section{Modelo de Classes}
\label{sec-modelo-classes}

O Diagrama de Classes do SCAP é apresentado na Figura~\ref{fig-classes}, onde é possível ver todas as classes identificadas por \citeonline{duarte-pg14} e \citeonline{prado-pg15} a fim de adaptar as entidades do mundo real para o paradigma OO:

\begin{itemize}
	\item A classe \textbf{Afastamento} representa uma solicitação de afastamento feita por algum professor. Seu atributo ``situacao'' indica em qual estágio da tramitação o afastamento se encontra. O tipo enumerado ``Situacao'', exibido na Figura~\ref{fig-tipos-enumerados}, lista os possíveis valores para este atributo. Já o atributo ``tipo\_afastamento'' indica se o afastamento é nacional ou internacional. Seus possíveis valores são indicados no tipo enumerado ``TipoAfastamento'' da Figura~\ref{fig-tipos-enumerados}. 
		
	\item A classe \textbf{Documento} representa os documentos que podem ser anexados pelo solicitante (para comprovar a veracidade das informações) ou por um secretário, ao cadastrar as decisões do DI / CT / PRPPG. 
	
	\item A classe \textbf{Parecer} representa um parecer emitido por um professor que se manifestou contra o afastamento ou pelo professor relator do afastamento. O atributo ``tipo\_parecer'' indica se o parecer é favorável ou desfavorável. Seus valores possíveis estão na Figura~\ref{fig-tipos-enumerados}, no tipo enumerado ``TipoParecer''.
	
	\item As classes \textbf{Professor} e \textbf{Secretário} representam, respectivamente, professores e secretários. Ambas são subclasses da classe \textbf{Pessoa}. Um professor pode ter relações de parentesco com outros professores, as quais são representadas pelo autorrelacionamento na classe ``Professor''.
	
	\item A classe \textbf{Mandato} representa um período no qual um professor é (ou foi) o chefe ou subchefe do departamento. No mundo real, o subchefe do departamento é o substituto imediato do chefe, atuando somente quando este estiver impossibilitado por algum motivo. Porém, como o escopo deste sistema é reduzido, não há diferenciação entre eles: permite-se que o subchefe atue em conjunto com o chefe (há uma restrição de integridade, na Seção~\ref{sec-restricoes-integridade}, para garantir que apenas dois professores atuem ao mesmo tempo). Dessa forma, ambos são tratados como ``chefe do departamento'' e não há a necessidade de um atributo na classe ``Mandato'' para diferenciá-los.
	
\end{itemize}

%Algumas leves alterações foram feitas nas nomenclaturas dos atributos das classes modeladas por \citeonline{duarte-pg14} e \citeonline{prado-pg15} (por exemplo: uso de \textit{snake\_case} ao invés de \textit{camelCase} e sem uso de abreviaturas). Outra modificação (mais relevante) feita neste trabalho em relação aos anteriores é a seguinte:

Uma modificação que foi feita neste trabalho em relação aos anteriores é a seguinte:

\begin{itemize}
	\item \textbf{Remoção do atributo ``parentesco'' da classe ``Parentesco''}: \citeonline{prado-pg15}, quando acrescentou a classe associativa ``Parentesco'' ao modelo de classes do SCAP, definiu um único atributo nomeado ``parentesco'' que poderia assumir os valores ``sanguineo'' ou ``matrimonial''. Porém não há tratamento diferenciado entre esses dois tipos de parentesco: tudo que vale para um vale para o outro. Sendo assim, como esse dado não tem utilidade dentro do escopo do sistema, ele foi removido. Consequentemente, a representação da classe associativa ``Parentesco'' deixou de ser necessária no diagrama de classes.
\end{itemize}

% É importante ressaltar que modelos de classes na fase de requisitos não costumam mostrar a visibilidade e o tipo dos atributos, pois, teoricamente, a plataforma de implementação ainda não foi definida. Porém, no caso deste trabalho, já sabe-se de antemão quais são as tecnologias envolvidas (linguagem de programação, \textit{frameworks} MVC, banco de dados...) pois o objetivo principal é justamente experimentar uma plataforma nova. Assim, a visibilidade e o tipo dos atributos foram inseridos no Diagrama de Classes do SCAP (Figura \ref{fig-classes}) por já serem conhecidos nesta etapa. 
 

\clearpage
\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figuras/fig-classes.pdf} 
	\caption{Diagrama de Classes do SCAP.}
	\label{fig-classes}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{figuras/fig-tipos-enumerados.pdf} 
	\caption{Tipos enumerados do SCAP.}
	\label{fig-tipos-enumerados}
\end{figure}

\section{Restrições de Integridade}
\label{sec-restricoes-integridade}

A seguir são listadas as restrições de integridade observadas por \citeonline{duarte-pg14} e complementadas por \citeonline{prado-pg15}: 

\begin{enumerate}
	\item Um professor não pode ser solicitado a dar um parecer sobre sua própria solicitação de afastamento.
	\item A data de início de um afastamento não pode ser posterior a data de fim do mesmo afastamento.
	\item A data de início de um mandato de professor não pode ser posterior a data de fim do mesmo mandato.
	\item Não pode haver mais de dois professores (chefe e subchefe de departamento) exercendo um mandato ao mesmo tempo.
	\item Um secretário não pode criar uma solicitação de afastamento.
	\item Um professor não pode ser relator de um afastamento solicitado por um parente.
	\item Uma pessoa deve ser ou secretário ou professor (não pode ser ambos).
\end{enumerate}

