% ==============================================================================
% TCC - Vinícius Berger
% Capítulo 1 - Introdução
% ==============================================================================

\chapter{Introdução}
\label{sec-intro}


Após alguns anos desde a liberação da Internet para uso comercial, que ocorreu no final dos anos 80, surgiram as primeiras empresas provedoras de acesso à Internet nos EUA~\cite{silva:2001}. Ao mesmo tempo, o Laboratório Europeu de Física de Partículas (CERN) inventou a \textit{World Wide Web}, que começou a ser utilizada para colocar informações ao alcance de qualquer usuário da Internet~\cite{silva:2001}.

``Nos primórdios da \textit{World Wide Web} (por volta de 1990 a 1995), os sites eram formados por nada mais que um conjunto de arquivos de hipertexto lincados que apresentavam informações usando texto e gráficos limitados''~\cite[p.~37]{pressman:2011}. Em pouco tempo, a Web tomou grandes proporções. Devido ao grande número de usuários envolvidos, investimento em pesquisas e o surgimento de tecnologias auxiliares, grandes sistemas comerciais começaram a ser desenvolvidos e implantados nesse ambiente. A evolução e manutenção desses sistemas, com o tempo, começou  a exigir esforços demasiados dos programadores e as formas tradicionais de desenvolvimento passaram a não suprir as necessidades do mercado~\cite{prado-pg15}. Surgiram então os primeiros \textit{frameworks}, que, segundo \citeonline{gabrieli-et-al:2007}, são infra-estruturas de aplicações projetadas para serem reutilizadas. Portanto, um \textit{framework} é uma arquitetura semi-pronta de aplicação e representa o nível mais alto de reutilização~\cite{cunha-et-al:2003}. A redução da manutenção e a maximização do reúso são dois dos ganhos conseguidos com a utilização dos \textit{frameworks}~\cite{gamma-et-al:2000}, o que resulta em redução do tempo de desenvolvimento.

De acordo com \citeonline{souza:2007}, este contexto foi o que motivou o início das pesquisas que deram origem ao FrameWeb, um método para Engenharia Web que promove a modelagem dos componentes dos \textit{frameworks} nos diagramas de projeto com vistas a fazer com que estes modelos estejam mais próximos de implementação em código. Dessa forma, a definição da arquitetura fica a cargo do projetista do sistema e não mais dos programadores, o que incentiva a utilização de uma arquitetura robusta e o aumento da produtividade no desenvolvimento de sistemas de informação para a Web~\cite{souza:2007}.

\citeonline{duarte-pg14} aplicou o método FrameWeb no processo de desenvolvimento de um sistema nomeado SCAP --- Sistema de Controle de Afastamentos de Professores --- para avaliar sua aplicabilidade e propor melhorias para os modelos que são definidos pelo método. O SCAP é um sistema que foi especificado para gerenciar as solicitações de afastamento de professores que tramitam dentro do Departamento de Informática da UFES~\cite{duarte-pg14}. Essas solicitações podem ser fundamentadas em diversos instrumentos jurídicos. Por exemplo: os afastamentos para participação em eventos e outras atividades que ocorrem em solo brasileiro são disciplinados pelas Portarias MEC nº 204/2020 e UFES nº 90 de 10/02/2020 \cite{site-prppg-nacional}. Já os afastamentos para participação em eventos e outras atividades que ocorrem fora do Brasil são disciplinados pelos Decretos Presidenciais nº 91.800 de 18/10/1985, nº 1.387 de 07/02/1995, bem como pelas Portarias MEC nº 204/2020 e UFES nº 90 de 10/02/2020 \cite{site-prppg-internacional}. Há ainda os afastamentos para capacitação regidos pela Lei 8.112/1990, Decreto nº 9.991/2019, Instrução Normativa nº 201/19-SGDP/ME e Resolução CEPE 31/2012 \cite{site-prppg-capacitacao}.

O SCAP já foi projetado e implementado por vários estudantes de graduação da UFES em seus trabalhos de conclusão de curso, cada um utilizando \textit{frameworks} diferentes, a saber: Java EE 7 por \citeonline{duarte-pg14}, VRaptor 4 por \citeonline{prado-pg15}, Laravel e Vue.js por \citeonline{pinheiro-pg17}, Spring MVC e Vaadin por \citeonline{matos-pg17}, Ninja por \citeonline{avelar-pg18}, Wicket e Tapestry por \citeonline{ferreira-pg18}, Play por \citeonline{guterres-pg19}, Code Igniter e Node.js por \citeonline{meirelles-pg19}. Vale ressaltar que os requisitos do SCAP, levantados e analisados por \citeonline{duarte-pg14}, foram complementados e materializados oficialmente em ``Documento de Requisitos'' e ``Documento de Especificação de Requisitos'' por \citeonline{prado-pg15}. Os demais trabalhos utilizaram estes documentos como base para gerar, cada um, seu próprio ``Documento de Projeto'' e sua própria implementação do SCAP. Não houve nenhuma atualização no ``Documento de Requisitos'' nem no ``Documento de Especificação de Requisitos'' desde \citeonline{prado-pg15}, apesar de \citeonline{guterres-pg19} ter feito alterações muito interessantes, no âmbito de seu trabalho, que poderiam ser incorporadas oficialmente em um trabalho futuro.

De forma semelhante, duas novas implementações desse sistema são realizadas neste trabalho, uma utilizando o \textit{framework} Yii e outra utilizando o \textit{framework} Symfony, seguindo o método FrameWeb proposto por \citeonline{souza:2007}. Espera-se, dessa forma, contribuir com a validação do método FrameWeb em um cenário novo e eventualmente sugerir adaptações. 



\section{Objetivos}
\label{sec-objetivos}

O presente trabalho tem por objetivo geral realizar duas novas implementações do SCAP, uma utilizando o \textit{framework} Yii e outra utilizando o \textit{framework} Symfony, ambas utilizando o método FrameWeb proposto por \citeonline{souza:2007} na etapa de Projeto. Em seguida, realizar uma análise da aplicação do método FrameWeb em ambos os projetos.

Os objetivos específicos a seguir serão a base para o alcance do objetivo geral:

\begin{itemize}
	\item Compreensão dos requisitos do SCAP;
	
	\item Uso do método FrameWeb proposto por \citeonline{souza:2007} para os projetos arquiteturais das duas novas implementações do sistema;
	
	\item Capacitação nos \textit{frameworks} Yii e Symfony, selecionados para o desenvolvimento;
	
	\item Desenvolvimento das duas novas implementações do sistema, incluindo documentação das arquiteturas e projetos.
\end{itemize}



\section{Método}
\label{sec-metodo}

Para atingir o objetivo geral apresentado na seção anterior, os seguintes passos serão realizados:

\begin{enumerate}
	
	\item Revisão bibliográfica: leitura de materiais que forneçam uma revisão dos conceitos já estudados e materiais que apresentem conteúdos novos e necessários para o desenvolvimento do sistema, sendo o método FrameWeb proposto por \citeonline{souza:2007} o principal deles;
	
	\item Análise do escopo do sistema e dos requisitos levantados: leitura dos trabalhos anteriores, com ênfase nos trabalhos realizados por \citeonline{duarte-pg14} e \citeonline{prado-pg15}, onde consta todo o levantamento e análise de requisitos do sistema a ser implementado;
	
	\item Estudo das tecnologias: leitura da documentação oficial dos dois \textit{frameworks} MVC utilizados para desenvolver o sistema, a saber: Yii e Symfony. Além disso, uma pequena revisão sobre as tecnologias relacionadas ao desenvolvimento Web (HTML, CSS, JavaScript e PHP) e banco de dados relacional;
		
	\item Elaboração das arquiteturas das duas novas implementações do sistema: com base no método FrameWeb e considerando as tecnologias estudadas, elaborar as duas arquiteturas do sistema gerando os Documentos de Projeto (um para a implementação feita com o Yii e outra para a implementação feita com o Symfony);
	
	\item Implementação do sistema: codificação das duas novas implementações do sistema utilizando as tecnologias definidas;
	
\end{enumerate}


\section{Organização do trabalho}
\label{sec-organizacao}

Este trabalho está dividido em cinco capítulos, incluindo a Introdução. O conteúdo dos próximos capítulos é descrito a seguir. 

No Capitulo~\ref{sec-referencial} --- Referencial Teórico --- são reunidos conceitos e explicações de outros autores a respeito dos assuntos relacionados a este trabalho, formando assim uma base de conhecimento que servirá de alicerce para a argumentação ao longo dos demais capítulos.

No Capítulo~\ref{sec-requisitos} --- Especificação de Requisitos e Análise --- são apresentados o levantamento e análise dos requisitos do SCAP (Sistema de Controle de Afastamentos de Professores) realizados em  \citeonline{duarte-pg14} e \citeonline{prado-pg15}.

No Capítulo~\ref{sec-projeto} --- Projeto e Implementação --- é apresentada a aplicação do método FrameWeb na construção das arquiteturas do SCAP. Para as implementações do sistema foram escolhidos dois \textit{frameworks} MVC (Yii e Symfony) ambos desenvolvidos em PHP. Ao longo do capítulo serão apresentados trechos dos códigos referente às duas implementações do SCAP.

No Capítulo~\ref{sec-conclusoes} --- Considerações Finais --- são apresentados os resultados obtidos da aplicação do método FrameWeb em termos de compatibilidade com os frameworks Yii e Symfony.
