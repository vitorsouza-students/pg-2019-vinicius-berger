<?php
/**
 * @Route("/{id}/registrar-parecer-relator", name="[...]", methods={"POST"})
 * @IsGranted("ROLE_PROFESSOR")
 */
public function registrarParecerRelator(Request $request, Afastamento $afastamento)
{
    $professorLogadoId = $this->getUser()->getProfessor()->getId();

    // CONDIÇÕES
    // - Afastamento é internacional
    // - Situação é Aguardando parecer relator
    // - O usuário logado deve ser um professor (já garantido)
    // - O usuário logado deve ser o relator
    if (
        $afastamento->getTipo() !== Afastamento::TIPO_INTERNACIONAL ||
        $afastamento->getSituacao() !== Afastamento::SIT_AG_PARECER_RELATOR ||
        $afastamento->getProfessorRelatorId() !== $professorLogadoId
    ) {
        throw new \Exception('Ação não permitida');
    }

    // executa caso de uso ...
}
