<?php
/**
 * @param $id
 * @return Response
 * @throws NotFoundHttpException
 * @throws Exception
 */
public function actionRegistrarParecerRelator($id)
{
    $model = $this->findModel($id);

    // CONDIÇÕES
    // - Afastamento é internacional
    // - Situação é Aguardando parecer relator
    // - O usuário logado deve ser um professor
    // - O usuário logado deve ser o relator
    if (
        $model->tipo !== Afastamento::TIPO_INTERNACIONAL ||
        $model->situacao !== Afastamento::SITUACAO_AGUARDANDO_PARECER_RELATOR ||
        empty(Yii::$app->user->identity->professor) ||
        $model->professorRelator->id !== Yii::$app->user->identity->professor->id
    ) {
        throw new Exception('Ação não permitida');
    }

    // executa caso de uso ...
}
