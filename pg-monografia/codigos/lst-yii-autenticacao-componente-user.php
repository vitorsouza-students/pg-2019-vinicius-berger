<?php
$config = [
    'components' => [
        'user' => [
            'identityClass' => 'app\models\Usuario',
        ],
        // outros componentes ...
    ],
    // outras configurações ...
];
